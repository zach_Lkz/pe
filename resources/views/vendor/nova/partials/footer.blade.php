<p class="mt-8 text-center text-xs text-80">
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} Promise Education Inc - By Aurora Technology Inc.
    <span class="px-1">&middot;</span>
    v1.0
</p>
