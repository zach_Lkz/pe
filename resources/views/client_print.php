<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
    text-align: center; 
    border:2px solid black; 
    border-collapse: collapse;
    padding: 0.5em 0 0.5em 0;
}
.dot {
    height: 1em;
    width: 1em;
    background-color: #fff;
    border: 2px solid black;
    border-radius: 50%;
    display: inline-block;
    top: 0.2em;
    position: relative;
}
</style>
</head>
<body onload="window.print()">
    <div width="100%">
        <span><img src="<?php URL::to('/') ?>/images/logo.png" style="height:3em;" /></span>
        <span style="float:right;">
            <div>20 Amber Street, Unit 201</div>
            <div>Markham ON</div>
            <div>L3R 5P4</div>
        </span>
    </div>
    <div style="text-align:center; width=100%;"><H1 style="margin-top: 1em; margin-bottom: 2em;">加诺咨询 - 加拿大境内客户信息收集表</H1></div>
    <table style="width:100%;">
        <tr>
            <td colspan="6">
                <span>基本信息</span>
            </td>
        </tr>
        <tr>
            <td width="17%">姓名</td>
            <td width="17%"><?php echo $client->name ?></td>
            <td width="17%">曾用名</td>
            <td width="17%"><?php echo $client->usedName ?></td>
            <td width="17%">性别</td>
            <td width="17%"><?php echo $client->gender ?></td>
        </tr>
        <tr>
            <td>出生日期</td>
            <td colspan="2"><?php echo $client->birthdate ?></td>
            <td>出生地</td>
            <td colspan="2"><?php echo ($birth_region?$birth_region->name:'') . " " . ($birth_province?$birth_province->name:'') . " " . ($birth_city?$birth_city->name:'') ?></td>
        </tr>
        <tr>
            <td>电话</td>
            <td colspan="2"><?php echo $client->phone ?></td>
            <td>邮箱</td>
            <td colspan="2"><?php echo $client->contact_email ?></td>
        </tr>
        <tr>
            <td>国籍</td>
            <td colspan="2"><?php echo $client->citizenship ?></td>
            <td>中国身份证号码</td>
            <td colspan="2"><?php echo $client->chinese_id_number ?></td>
        </tr>
        <tr>
            <td>在加身份</td>
            <td><span class="dot"></span> 学签</td>
            <td><span class="dot"></span> 工签</td>
            <td><span class="dot"></span> 访问签</td>
            <td><span class="dot"></span> PR</td>
            <td><span class="dot"></span> 公民</td>
        </tr>
        <tr>
            <td>护照号码</td>
            <td colspan="2"><?php echo $client->passport ?></td>
            <td>护照到期日</td>
            <td colspan="2"><?php echo substr($client->passport_expire, 0, 10) ?></td>
        </tr>
        <tr>
            <td>签证到期日</td>
            <td colspan="2"><?php echo substr($client->visa_expire, 0, 10) ?></td>
            <td>加拿大UCI号码</td>
            <td colspan="2"><?php echo $client->uci_number ?></td>
        </tr>
        <tr>
            <td>中国现居地址</td>
            <td colspan="5"><?php echo $original_province->name . " " . $original_city->name ?></td>
        </tr>
        <tr>
            <td>加拿大现居地址</td>
            <td colspan="5"><?php echo $client->address . " " . $client->city . " " .  $client->province  . " " .  $client->postal_code ?></td>
        </tr>
    </table>
    <table style="width:100%; margin-top:3em;">
        <tr>
            <td width="17%">客户</td>
            <td width="33%"><?php echo $client->name ?></td>
            <td width="17%">信息收集人</td>
            <td width="33%"><?php echo ($user ? $user->name : '') ?></td>
        </tr>
        <tr>
            <td width="17%" style="height: 4em;">客户签字</td>
            <td width="33%" style="vertical-align: bottom; font-size:0.8em; color:#ccc;"><div>请与护照签名一致</div></td>
            <td width="17%">日期</td>
            <td width="33%" style="vertical-align: bottom; font-size:0.8em; color:#ccc;"><div>YYYY-MM-DD</div></td>
        </tr>
        <tr>
            <td width="17%"  style="height: 4em;">信息收集人签字</td>
            <td width="33%" style="vertical-align: bottom; font-size:0.8em; color:#ccc;"></td>
            <td width="17%">日期</td>
            <td width="33%" style="vertical-align: bottom; font-size:0.8em; color:#ccc;">YYYY-MM-DD</td>
        </tr>
    </table>
</body>
</html>