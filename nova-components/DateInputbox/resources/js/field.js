Nova.booting((Vue, router, store) => {
    Vue.component('index-DateInputBox', require('./components/IndexField'))
    Vue.component('detail-DateInputBox', require('./components/DetailField'))
    Vue.component('form-DateInputBox', require('./components/FormField'))
})
