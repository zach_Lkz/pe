<?php

namespace Acme\DateInputBox;

use Laravel\Nova\Fields\Field;

class DateInputBox extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'DateInputBox';
}
