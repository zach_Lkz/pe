Nova.booting((Vue, router, store) => {
    Vue.component('index-currency-input-box', require('./components/IndexField'))
    Vue.component('detail-currency-input-box', require('./components/DetailField'))
    Vue.component('form-currency-input-box', require('./components/FormField'))
})
