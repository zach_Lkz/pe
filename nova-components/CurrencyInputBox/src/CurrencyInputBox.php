<?php

namespace Acme\CurrencyInputBox;

use Laravel\Nova\Fields\Field;

class CurrencyInputBox extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'currency-input-box';
}
