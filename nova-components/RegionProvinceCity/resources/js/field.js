Nova.booting((Vue, router, store) => {
    Vue.component(
        "index-region-province-city",
        require("./components/IndexField")
    );
    Vue.component(
        "detail-region-province-city",
        require("./components/DetailField")
    );
    Vue.component(
        "form-region-province-city",
        require("./components/FormField")
    );
});
