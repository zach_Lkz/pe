Nova.booting((Vue, router, store) => {
    Vue.component("index-department", require("./components/IndexField"));
    Vue.component("detail-department", require("./components/DetailField"));
    Vue.component("form-department", require("./components/FormField"));
});
