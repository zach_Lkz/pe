Nova.booting((Vue, router, store) => {
    Vue.component("index-region-province", require("./components/IndexField"));
    Vue.component(
        "detail-region-province",
        require("./components/DetailField")
    );
    Vue.component("form-region-province", require("./components/FormField"));
});
