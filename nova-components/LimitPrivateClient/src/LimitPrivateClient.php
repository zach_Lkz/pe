<?php

namespace Acme\LimitPrivateClient;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;
use Auth;

class LimitPrivateClient extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'limit-private-client';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        $user = Auth::user();

        if ($user){
            return $query->where("user_id", $user->id);
        } else {
            return $query;
        }
       
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        return ['私人客户'=>'0'];
    }
}
