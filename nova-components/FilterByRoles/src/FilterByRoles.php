<?php

namespace Acme\FilterByRoles;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;
use App\Role;
class FilterByRoles extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'filter-by-roles';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where("role_id", $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $roles = Role::all();
        //print_r($departments);
        $result = array();
        foreach($roles as $item) {
            $result[] = array(
                "name" => $item->name,
                "value" => $item->id
            );
        }
        return $result;
    }
}
