Nova.booting((Vue, router, store) => {
    Vue.component('index-department-position', require('./components/IndexField'))
    Vue.component('detail-department-position', require('./components/DetailField'))
    Vue.component('form-department-position', require('./components/FormField'))
})
