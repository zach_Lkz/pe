Nova.booting((Vue, router, store) => {
    Vue.component("index-region", require("./components/IndexField"));
    Vue.component("detail-region", require("./components/DetailField"));
    Vue.component("form-region", require("./components/FormField"));
});
