Nova.booting((Vue, router, store) => {
    Vue.component('index-TelephoneInputBox', require('./components/IndexField'))
    Vue.component('detail-TelephoneInputBox', require('./components/DetailField'))
    Vue.component('form-TelephoneInputBox', require('./components/FormField'))
})