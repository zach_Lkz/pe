<?php

namespace Acme\TelephoneInputBox;

use Laravel\Nova\Fields\Field;

class TelephoneInputBox extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'TelephoneInputBox';
}
