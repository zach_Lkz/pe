<?php

namespace Acme\FilterByDepartment;

use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;
use App\Department;

class FilterByDepartment extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'filter-by-department';

    /**
     * Apply the filter to the given query.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $value
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->where("department_id", $value);
    }

    /**
     * Get the filter's available options.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function options(Request $request)
    {
        $departments = Department::all();
        //print_r($departments);
        $result = array();
        foreach($departments as $item) {
            $result[] = array(
                "name" => $item->name,
                "value" => $item->id
            );
        }
        return $result;
    }
}
