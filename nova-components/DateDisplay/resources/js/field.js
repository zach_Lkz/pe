Nova.booting((Vue, router, store) => {
    Vue.component('index-date-display', require('./components/IndexField'))
    Vue.component('detail-date-display', require('./components/DetailField'))
    Vue.component('form-date-display', require('./components/FormField'))
})
