<?php

namespace Acme\DateDisplay;

use Laravel\Nova\Fields\Field;

class DateDisplay extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'date-display';
}
