Nova.booting((Vue, router, store) => {
    Vue.component('index-sin_input_box', require('./components/IndexField'))
    Vue.component('detail-sin_input_box', require('./components/DetailField'))
    Vue.component('form-sin_input_box', require('./components/FormField'))
})
