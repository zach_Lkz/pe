<?php

namespace Acme\SinInputBox;

use Laravel\Nova\Fields\Field;

class SinInputBox extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'sin_input_box';
}
