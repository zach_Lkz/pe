<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//use app\User;
//use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. They are protected
| by your tool's "Authorize" middleware by default. Now, go build!
|
*/

// Route::get('/endpoint', function (Request $request) {
//     //
// });
Route::get('/routes', function (Request $request) {
    /*
    $routes = collect(Route::getRoutes())->map(function ($route) {
        return [
            'uri' => $route->uri,
            'as' => $route->action['as'] ?? '',
            'methods' => $route->methods,
            'action' => $route->action['uses'] ?? '',
            'middleware' => $route->action['middleware'] ?? [],
        ];
    });

    return response()->json($routes);
    */
    $name = $request->input('name');
    $birthdate = $request->input('birthdate');
    $passport = $request->input('passport');
    $user = [];

    $query = DB::table('clients');
    $doquery = 0;
    if (!empty($name)) {
        $query->where('name', $name);
        $doquery++;
    }
    if (!empty($birthdate)) {
        $query->where('birthdate', $birthdate);
        $doquery++;
    }
    if (!empty($passport)) {
        $query->where('passport', $passport);
        $doquery++;
    }
    if ($doquery > 1) {
        $user = $query->get();
    }

    return response()->json($user);
});

Route::get('/routes/create', function (Request $request) {
    $type = $request->input('type');
    $name = $request->input('name');
    $birthdate = $request->input('birthdate');
    $passport = $request->input('passport');
    $user = [];

    if (!empty($name)) {
        $user['name'] = $name;
    } else {
        $user['name'] = '';
    }
    if (!empty($birthdate)) {
        $user['birthdate'] = $birthdate;
    }
    if (!empty($passport)) {
        $user['passport'] = $passport;
    }
    $id = DB::table('clients')->insertGetId($user);

    if ($id) {
        return redirect('/nova/resources/' . $type . '/' . $id . '/edit');    
    }
    return redirect('/nova/route-viewer');
});

Route::get('/routes/{id}/{type}', function (Request $request, $id=0, $type='') {    
    //if ($id && (strtolower($ver) == md5('V'.$id))) {     
    if ($id) {
        DB::table('clients')->where('id', $id)->update(['user_id' => auth()->user()->id]);        
        return redirect('/nova/resources/' . $type . '/' . $id . '/edit?viaResource=&amp;viaResourceId=&amp;viaRelationship=');    
    }
    return redirect('/nova/route-viewer');
});