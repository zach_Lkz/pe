<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::redirect('/','/nova');

Route::redirect('/nova/dashboards/main','/nova/resources/private-clients');

Route::get('/print/company/{id}','PrintController@company');
Route::get('/print/client/{id}','PrintController@company');
