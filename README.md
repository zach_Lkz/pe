#

# 涉及到Nova的自定义行为
例如：创建客户的时候需要路由到特殊的页面进行客户搜索，找不到在进行创建
步骤：
1. 修改/Nova里面的相关功能
2. 测试没问题后，需要进行nova编译

```
cd nova
npm run prod
php artisan nova:publish
php artisan view:clear
```