<?php

namespace App\Policies;

use App\User;
use App\JobLevel;
use Illuminate\Auth\Access\HandlesAuthorization;

class JobLevelPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function viewAny(User $user)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function view(User $user, JobLevel $JobLevel)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function create(User $user)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function update(User $user, JobLevel $JobLevel)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function delete(User $user, JobLevel $JobLevel)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function restore(User $user, JobLevel $JobLevel)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function forceDelete(User $user, JobLevel $JobLevel)
    {
        //
        return $user->checkUserPermission(2);
    }



}
