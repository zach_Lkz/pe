<?php

namespace App\Policies;

use App\User;
use App\Client;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny(User $user)
    {
        //
        //return true;
        return  $user->checkUserPermission(3);
    }

    public function view(User $user, Client $Client)
    {
        //
        return $user->checkUserPermission(5);
    }

    public function create(User $user)
    {
        //
        return $user->checkUserPermission(5);
    }

    public function update(User $user, Client $Client)
    {
        //
        return $user->checkUserPermission(7);
    }

    public function delete(User $user, Client $Client)
    {
        //
        return $user->checkUserPermission(6);
    }

    public function restore(User $user, Client $Client)
    {
        //
        return $user->checkUserPermission(6);
    }

    public function forceDelete(User $user, Client $Client)
    {
        //
        return $user->checkUserPermission(6);
    }



}
