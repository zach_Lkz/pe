<?php

namespace App\Policies;

use App\User;
use App\StatusInCanada;
use Illuminate\Auth\Access\HandlesAuthorization;

class StatusInCanadaPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function viewAny(User $user)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function view(User $user, StatusInCanada $statusInCanada)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function create(User $user)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function update(User $user, StatusInCanada $statusInCanada)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function delete(User $user, StatusInCanada $statusInCanada)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function restore(User $user, StatusInCanada $statusInCanada)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function forceDelete(User $user, StatusInCanada $statusInCanada)
    {
        //
        return $user->checkUserPermission(2);
    }



}
