<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    
    public function viewAny(User $user)
    {
        //
        //return $user->checkUserPermission(2);
        return true;
 
    }

    public function view(User $user, User $user2)
    {
        //
        // return $user->checkUserPermission(2);
        if($user->id == $user2->id){
            return true;
        }else{
            return $user->checkUserPermission(2);
        }
        
    }

    public function create(User $user)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function update(User $user, User $user2)
    {
        //
        // return $user->checkUserPermission(2);
        if($user->id == $user2->id){
            return true;
        }else{
            return $user->checkUserPermission(2);
        }
        
    }

    public function delete(User $user, User $user2)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function restore(User $user, User $user2)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function forceDelete(User $user, User $user2)
    {
        //
        return $user->checkUserPermission(2);
    }

}
