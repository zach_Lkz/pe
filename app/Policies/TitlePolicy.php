<?php

namespace App\Policies;

use App\User;
use App\Title;
use Illuminate\Auth\Access\HandlesAuthorization;

class TitlePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    
    public function viewAny(User $user)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function view(User $user, Title $title)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function create(User $user)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function update(User $user, Title $title)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function delete(User $user, Title $title)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function restore(User $user, Title $title)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function forceDelete(User $user, Title $title)
    {
        //
        return $user->checkUserPermission(2);
    }



}
