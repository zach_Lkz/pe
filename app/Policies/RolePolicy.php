<?php

namespace App\Policies;

use App\User;
use App\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    
    public function viewAny(User $user)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function view(User $user, Role $role)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function create(User $user)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function update(User $user, Role $role)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function delete(User $user, Role $role)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function restore(User $user, Role $role)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function forceDelete(User $user, Role $role)
    {
        //
        return $user->checkUserPermission(2);
    }



}
