<?php

namespace App\Policies;

use App\User;
use App\Permission;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function viewAny(User $user)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function view(User $user, Permission $Permission)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function create(User $user)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function update(User $user, Permission $Permission)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function delete(User $user, Permission $Permission)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function restore(User $user, Permission $Permission)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function forceDelete(User $user, Permission $Permission)
    {
        //
        return $user->checkUserPermission(2);
    }



}
