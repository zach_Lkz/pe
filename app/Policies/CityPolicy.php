<?php

namespace App\Policies;

use App\User;
use App\City;
use Illuminate\Auth\Access\HandlesAuthorization;

class CityPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    
    public function viewAny(User $user)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function view(User $user, City $City)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function create(User $user)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function update(User $user, City $City)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function delete(User $user, City $City)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function restore(User $user, City $City)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function forceDelete(User $user, City $City)
    {
        //
        return $user->checkUserPermission(2);
    }



}
