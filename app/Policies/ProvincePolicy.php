<?php

namespace App\Policies;

use App\User;
use App\Province;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProvincePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function viewAny(User $user)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function view(User $user, Province $Province)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function create(User $user)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function update(User $user, Province $Province)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function delete(User $user, Province $Province)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function restore(User $user, Province $Province)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function forceDelete(User $user, Province $Province)
    {
        //
        return $user->checkUserPermission(2);
    }



}
