<?php

namespace App\Policies;

use App\User;
use App\Department;
use Illuminate\Auth\Access\HandlesAuthorization;

class DepartmentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function viewAny(User $user)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function view(User $user, Department $department)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function create(User $user)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function update(User $user, Department $department)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function delete(User $user, Department $department)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function restore(User $user, Department $department)
    {
        //
        return $user->checkUserPermission(2);
    }

    public function forceDelete(User $user, Department $department)
    {
        //
        return $user->checkUserPermission(2);
    }




}
