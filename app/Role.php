<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }

    public function addPermission($permission_id){
        $this->permissions()->attach($permission_id);
    }

    public function deletePermission($permission_id){
        $this->permissions()->detach($permission_id);
    }
}
