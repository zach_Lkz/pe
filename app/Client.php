<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    //
    use SoftDeletes;
    protected $dates = [
        'birthdate', 'passport_expire', 'visa_expire', 'landing_date'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function original_region()
    {
        return $this->belongsTo('App\Region');
    }

    public function original_province()
    {
        return $this->belongsTo('App\Province');
    }

    public function original_city()
    {
        return $this->belongsTo('App\City');
    }


    public static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            if($post->user_id == null){
                $post->user_id = auth()->user()->id;
            }
            
        });
    }

    public function status_in_canada(){
        return $this->belongsTo('App\StatusInCanada');
    }

    public function test(){
        return "ok";
    }

    public function updateExpireDays(){
        $datetime1 = new \DateTime(NOW());
        $datetime2 = new \DateTime($this->visa_expire);
        $interval = $datetime1->diff($datetime2);

        $days = $interval->format('%R%a');
        
        $this->days_until_expire = $days;
        $this->save();
        return $days; //now do whatever you like with $days
    }

    public function updateCompletionRate(){
        $rate = 0;
        if ($this->name){
            $rate += 5;
        }

        if ($this->usedName){
            $rate += 5;
        }

        if ($this->gender){
            $rate += 5;
        }

        if ($this->birthdate){
            $rate += 5;
        }

        if ($this->contact_email){
            $rate += 5;
        }

        if ($this->phone){
            $rate += 5;
        }
        if ($this->citizenship){
            $rate += 5;
        }

        if ($this->birth_city && $this->birth_province && $this->birth_region){
            $rate += 5;
        }
        if ($this->original_city && $this->original_province && $this->original_region){
            $rate += 5;
        }
        if ($this->address && $this->city && $this->province && $this->postal_code){
            $rate += 5;
        }

        if ($this->status_in_canada_id < 4 && $this->status_in_canada_id > 0){
            if ($this->status_in_canada_id){
                $rate += 5;
            }
            if ($this->chinese_id_number){
                $rate += 5;
            }
            if ($this->passport){
                $rate += 5;
            }
            if ($this->passport_expire){
                $rate += 10;
            }
            if ($this->visa_expire){
                $rate += 10;
            }
            if ($this->uci_number){
                $rate += 5;
            }
            if ($this->landing_location){
                $rate += 5;
            }
            if ($this->landing_date){
                $rate += 5;
            }


        } else if($this->status_in_canada_id < 8){
            if ($this->status_in_canada_id){
                $rate += 10;
            }

            if ($this->chinese_id_number){
                $rate += 10;
            }

            if ($this->passport){
                $rate += 15;
            }

            if ($this->passport_expire){
                $rate += 15;
            }

        }
        $this->completion_rate = $rate;
    }
}
