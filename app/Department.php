<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    //
    public function titles()
    {
        return $this->hasMany('App\Title');
    }


    public function users()
    {
        return $this->hasMany('App\User');
    }
    
}
