<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Panel;

use Laravel\Nova\Http\Requests\NovaRequest;

use Acme\DateInputBox\DateInputBox;
use Acme\TelephoneInputBox\TelephoneInputBox;
use Acme\Department\Department;
use Acme\DepartmentPosition\DepartmentPosition;
use Acme\SinInputBox\SinInputBox;
use Acme\FilterByDepartment\FilterByDepartment;
use Acme\FilterByRoles\FilterByRoles;

class User extends Resource
{
    public static $group = '2.员工管理';
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\User';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';
    public static function label() 
    { 
        return ("展示界面"); 
    }


    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name', 'contact_email', 
        'birthdate', 'phone', "role_id", "department_id"
    ];

    //check login user id, if id equals admin's id, then display this module. Else hide from the navigation bar
    public static function availableForNavigation(Request $request)
    {
        if($request->user()->getAuthIdentifier()!=1){
            return false;
        }else{
            return true;
        }
       
    }
    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()
                ->sortable()->hideFromIndex()->hideFromDetail(),

            Text::make(__('Name'), 'name')
                ->sortable()
                ->rules('required', 'max:255'),

            BelongsTo::make(__('JobLevel'), 'jobLevel', 'App\Nova\JobLevel')
                ->sortable()
                ->nullable()
                ->hideFromDetail()
                ->hideWhenCreating()
                ->hideWhenUpdating(),

            Select::make(__('Gender'), 'gender')->options([
                    'Male' => 'Male',
                    'Female' => 'Female',
                    'Other' => 'Other',
                ])
                ->sortable()
                ->rules('required'),

            DateInputBox::make(__('BirthDate'), 'birthdate')
                ->sortable()
                ->withMeta(['extraAttributes' => [ 'colored' => false]])
                ->rules('required','max:10'),

            TelephoneInputBox::make(__('Phone'), 'phone')
                ->sortable()
                ->rules('required', 'max:20'),

            Text::make(__('ContactEmail'), 'contact_email')
                ->sortable()
                ->rules('required', 'email', 'max:200'),

                //员工身份
            Select::make(__('Identity'), 'identity')->options([
                    'SP' => 'SP',
                    'WP' => 'WP',
                    'VR' => 'VR',
                    'PR' => 'PR',
                    'CC' => 'CC',
                    'CN' => 'CN'
                ])
                ->sortable()
                ->rules('required')
                ->hideFromIndex(),

            DateInputBox::make(__('JoinDate'), 'join_date')
                ->sortable() 
                ->withMeta(['extraAttributes' => [ 'colored' => false]])
                ->rules('required'),

            Department::make(__('Department'), 'department', 'App\Nova\Department')->sortable()->nullable(),
            DepartmentPosition::make(__('Title'), 'title', 'App\Nova\Title')->sortable()->nullable()->hideFromIndex(),
            BelongsTo::make(__('JobLevel'), 'jobLevel', 'App\Nova\JobLevel')->sortable()->nullable()->hideFromIndex(),

            Currency::make(__('salary'), 'salary')->min(0)->step(1)->sortable()->hideFromIndex(),

            Text::make(__('passport'), 'passport')
                ->sortable()->hideFromIndex(),

            SinInputBox::make(__('SIN_number'), 'sin_number')
                ->sortable()->hideFromIndex(),
            

            Text::make(__('LoginEmail'), 'email')
                ->sortable()
                ->rules('required', 'email', 'max:254')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),

            Password::make(__('LoginPassword'), 'Password')
                ->onlyOnForms()
                ->creationRules('required', 'string', 'min:8')
                ->updateRules('nullable', 'string', 'min:8')
                ->hideFromIndex(),

       
            BelongsTo::make(__('Role'), 'role', 'App\Nova\Role')->hideFromIndex()->nullable(),

            new Panel(__("AddressDetail"), $this->addressField()),
        ];

    }

    public function addressField(){
        return [
            Text::make(__('Address'), 'address')->hideFromIndex(),
            Text::make(__('City'), 'city')->hideFromIndex(),
            Text::make(__('Province'), 'province')->hideFromIndex(),
            Text::make(__('postal_code'), 'postal_code')->hideFromIndex()
        
        ];
    }
    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new FilterByDepartment,
            new FilterByRoles
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
