<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;

use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Acme\DateInputBox\DateInputBox;
use Acme\DateDisplay\DateDisplay;
use Acme\TelephoneInputBox\TelephoneInputBox;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Panel;

class PersonalInformation extends Resource
{

    public static $group = '员工管理';
    public static $displayInNavigation = false;


    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\\User';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';
    public static function label() 
    { 
        return __("Profile"); 
    }
    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()
            ->sortable()
            ->withMeta(['extraAttributes' => [
                'readonly' => true
          ]])->hideFromDetail(),
            Text::make(__('Name'), 'name')
            ->sortable()
            ->rules('required', 'max:255')
            ->withMeta(['extraAttributes' => [
                'readonly' => true
          ]]),

        Text::make(__('Gender'), 'gender')
            ->sortable()
            ->rules('required')
            ->withMeta(['extraAttributes' => [
                'readonly' => true
          ]])
            ->hideFromIndex(),

        DateDisplay::make(__('BirthDate'), 'birthdate')
            ->sortable()
            ->rules('required','max:10')
            ->withMeta(['extraAttributes' => [
                'readonly' => true
          ]])
            ->hideFromIndex(),

    

            //员工身份
        Text::make(__('Identity'), 'identity')
            ->withMeta(['extraAttributes' => [
                'readonly' => true]])
            ->sortable()
            ->rules('required'),



        DateDisplay::make(__('JoinDate'), 'join_date')
            ->sortable()
            ->rules('required')->hideFromIndex(),

        BelongsTo::make(__('Department'), 'department', 'App\Nova\Department')->nullable()->withMeta(['extraAttributes' => [
            'readonly' => true]]),
        BelongsTo::make(__('Title'), 'title', 'App\Nova\Title')->nullable()->withMeta(['extraAttributes' => [
            'readonly' => true]]),
        BelongsTo::make(__('JobLevel'), 'jobLevel', 'App\Nova\JobLevel')->nullable()->withMeta(['extraAttributes' => [
            'readonly' => true]]),

        // Number::make(__('salary'), 'salary')->min(0)->step(1)->sortable(),

        Text::make(__('passport'), 'passport')
            ->sortable()
            ->withMeta(['extraAttributes' => [
                'readonly' => true]]),

        Text::make(__('SIN_number'), 'sin_number')
            ->sortable()
            ->withMeta(['extraAttributes' => [
                'readonly' => true]]),
        

        Text::make(__('LoginEmail'), 'email')
            ->sortable()
            ->rules('required', 'email', 'max:254')
            ->creationRules('unique:users,email')
            ->updateRules('unique:users,email,{{resourceId}}')
            ->withMeta(['extraAttributes' => [
                'readonly' => true]]),

      

   
        // BelongsTo::make(__('Role'), 'role', 'App\Nova\Role')->hideFromIndex()->nullable(),

        new Panel("更新信息", $this->changableField()),
   
        //可更改部分
  
        // HasMany::make(__('Clients'), 'clients', 'App\Nova\Client')->hideFromIndex()->nullable(),
        //$this->addressField()

        ];
    }

    public function changableField(){
        return [
            //电话
            TelephoneInputBox::make(__('Phone'), 'phone')
            ->sortable()
            ->rules('required', 'max:20')
            ->hideFromIndex(),

            //邮箱
            Text::make(__('ContactEmail'), 'contact_email')
            ->sortable()
            ->rules('required', 'email', 'max:254')
            ->hideFromIndex(),

            Password::make(__('LoginPassword'), 'Password')
            ->onlyOnForms()
            ->creationRules('required', 'string', 'min:8')
            ->updateRules('nullable', 'string', 'min:8'),

        
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
