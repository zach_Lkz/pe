<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Country;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Avatar;
use NovaButton\Button;
use Laravel\Nova\Panel;
use Acme\DateInputBox\DateInputBox;
use Acme\TelephoneInputBox\TelephoneInputBox;

use Acme\Region\Region;
use Acme\RegionProvince\RegionProvince;
use Acme\RegionProvinceCity\RegionProvinceCity;
use Wemersonrv\InputMask\InputMask;
use Causelabs\ResourceIndexLink\ResourceIndexLink;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Acme\LimitPrivateClient\LimitPrivateClient;

class Client extends Resource
{
    public static $group = '1.客户管理';
    public static function label() 
    { 
        return __('company_client'); 
    }
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Client';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';
    public static function authorizable()
    {
        return true;
    }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name', 
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
                BelongsTo::make(__('规划师'), 'user', 'App\Nova\User')->searchable()->sortable()->nullable()->hideFromIndex(),
                Button::make(__('打印客户资料'))->link(url("/print/company/" . $this->id))->hideFromIndex(),

                new Panel(__("BasicInfo"), [

                    ID::make()->sortable()->hideFromIndex()->hideFromDetail(),
                    
                    Avatar::make(__('Photo'), 'photo')->disk('public')->hideFromIndex(),

                    ResourceIndexLink::make(__('Name'), 'name')
                        ->sortable()
                        ->rules('required', 'max:255'),

                    Text::make(__('UsedName'), 'usedName')
                        ->sortable()
                        ->rules('max:255')->withMeta(['extraAttributes' => [
                            'placeholder' => '无']
                        ])->hideFromIndex(),

                    Select::make( __('Gender'), 'gender')->options([
                            'Male' => 'Male',
                            'Female' => 'Female',
                            'Other' => 'Other',
                        ])
                        ->sortable()
                        ->hideFromIndex(),

                    DateInputBox::make(__('BirthDate'), 'birthdate')
                        ->sortable()->withMeta(['extraAttributes' => [ 'colored' => false]]),

                    TelephoneInputBox::make(__('Phone'), 'phone')
                        ->sortable(),

                    Text::make(__('ContactEmail'), 'contact_email')
                        ->sortable()
                        ->rules('max:200')
                        ->hideFromIndex(),
                    
                    //Country::make(__('Citizenship'), 'citizenship')->hideFromIndex(),
                    Select::make( __('Citizenship'), 'citizenship')->options([
                        '中国' => '中国',
                        '中国香港' => '中国香港',
                        '中国台湾' => '中国台湾',
                        '加拿大' => '加拿大',
                        '美国' => '美国',
                        '越南' => '越南',
                        '韩国' => '韩国'
                    ])
                    ->sortable()
                    ->hideFromIndex(),
                    Text::make(__('BirthCity'), 'birth_city')->hideFromIndex(),
                    Text::make(__('BirthProvince'), 'birth_province')->hideFromIndex(),
                    Text::make(__('BirthRegion'), 'birth_region')->hideFromIndex(),
                    
                ]),

                new Panel(__("AddressDetail"), $this->locations()),
                new Panel(__("IDInformation"), $this->importantInfoamtion()),

                new Panel(__("OtherInformation"), [
                    Date::make(__('landing_date'), 'landing_date')->sortable()->hideFromIndex(),
                    Select::make( __('landing_location'), 'landing_location')->options([
                        '多伦多' => '多伦多',
                        '温哥华' => '温哥华',
                        '蒙特利尔' => '蒙特利尔',
                        '渥太华' => '渥太华',
                        '卡尔加里' => '卡尔加里',
                        '埃德蒙顿' => '埃德蒙顿',
                        '温尼伯' => '温尼伯',
                        '萨斯卡通' => '萨斯卡通'
                    ])
                    ->sortable()
                    ->hideFromIndex(),
                    //Text::make(__('completion_rate'), 'completion_rate')->sortable()->hideWhenCreating(),
                    Text::make(__('completion_rate'),'completion_rate', function () {

                            $this->updateCompletionRate();
                            $rate = (int)$this->completion_rate;
                            $color = 'green';
                            $text = "".$rate."%";
                            if ($rate <= 80){
                                $color = 'red';
                            }
                            return '<span style="font-weight:bold; color:'.$color.';">'.$text. '</span>';
                       
                    })->asHtml()->sortable()->onlyOnIndex(),
                
                ]),
                BelongsTo::make(__('规划师'), 'user', 'App\Nova\User')->searchable()->sortable()->nullable()->onlyOnIndex(),
                // Button::make(__('打印'))->link(url("/print/company/" . $this->id))->hideFromDetail()->onlyOnIndex(),
                Button::make(__('Print'))->link(url("/print/client/" . $this->id))->hideFromDetail()->onlyOnIndex(),


        ];
    }

    protected function locations()
    {
        return [

            RegionProvinceCity::make(__('OriginalCity'), 'original_city', 'App\Nova\City')->searchable()->hideFromIndex(),
            RegionProvince::make(__('OriginalProvince'), 'original_province', 'App\Nova\Province')->hideFromIndex()->readonly(),
            Region::make(__('OriginalRegion'), 'original_region', 'App\Nova\Region')->hideFromIndex()->readonly(),

            Text::make(__('Address'), 'address')->hideFromIndex(),
            // Text::make(__('City'), 'city')->hideFromIndex(),
            // Text::make(__('Province'), 'province')->hideFromIndex(),
            Text::make(__('postal_code'), 'postal_code')->hideFromIndex()
        
        ];
    }


    protected function importantInfoamtion()
    {
        return [
            
            BelongsTo::make(__('status_in_canada'), 'status_in_canada', 'App\Nova\StatusInCanada')->sortable()->nullable()->viewable(false),
            Text::make(__('passport'), 'passport')->sortable()->hideFromIndex(),
            DateInputBox::make(__('passport_expire'), 'passport_expire')->withMeta(['extraAttributes' => [ 'colored' => true, 'isVisa' => false]])->sortable(),
            // Text::make(__('passport_expire'), 'passport_expire', function () {
            //     if ($this->passport_expire && $this->passport_expire != "1900-01-01 00:00:00"){
            //         $this->updateExpireDays();
            //         $days = (int)$this->days_until_expire;
            //         $text = $passport_expire;
            //         $color = 'green';
            //         if ($days < 90 && $days > 0){
            //             $color = 'blue';
            //         } else if ($days > -89 && $days < 1){
            //             $color = 'orange';
            //         } else if ($days < -90){
            //             $color = 'red';
            //         }
            //         return '<span style="font-weight:bold; color:'.$color.';">'.$text. '</span>';
            //     } else {
            //         return "<span></span>";
            //     }
            // })->asHtml()->sortable(),
            InputMask::make(__('chinese_id_number'), 'chinese_id_number')
            ->mask('###-###-####-####-###X')
            ->sortable()
            ->hideFromIndex(),
            DateInputBox::make(__('visa_expire'), 'visa_expire')->withMeta(['extraAttributes' => [ 'colored' => true, 'isVisa' => true]])->sortable(),
            
            Text::make(__('uci_number'), 'uci_number')->hideFromIndex(),
        ];
    }


    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new LimitPrivateClient
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [new Actions\UpdateVisaExpireDays];
    }
}
