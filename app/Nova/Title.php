<?php

namespace App\Nova;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

class Title extends Resource
{
    public static $group = '3.设置';
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Title';
    // public static $displayInNavigation = false;


    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';
    public static function label() 
    { 
        return __("Title"); 
    }
    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->hideFromIndex(),
            Text::make('Name')
            ->sortable()
            ->rules('required', 'max:255'),
            
            BelongsTo::make(__('Department'), 'department', 'App\Nova\Department')->nullable()->sortable(),
            //BelongsTo::make(__('Title'), 'parentTitle', 'App\Nova\Title')->nullable()->sortable(),
            HasMany::make(__('Title'), 'titles', 'App\Nova\Title')->nullable()->sortable(),
            HasMany::make(__('User'), 'users', 'App\Nova\User')->nullable()->sortable(),
            /*
            Text::make('测试用', function () {
                return json_encode($this->getClients());
            })->asHtml()->sortable(),
            */
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
