<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Panel;

use Acme\DateInputBox\DateInputBox;
use Acme\TelephoneInputBox\TelephoneInputBox;
use Acme\CurrencyInputBox\CurrencyInputBox;
use Acme\Department\Department;
use Acme\DepartmentPosition\DepartmentPosition;
use Acme\SinInputBox\SinInputBox;
use Acme\FilterByDepartment\FilterByDepartment;
use Acme\FilterByRoles\FilterByRoles;


use Laravel\Nova\Http\Requests\NovaRequest;

class UserSafe extends Resource
{
    public static $group = '2.员工管理';
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\\User';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

        //check login user id, if id equals admin's id, then display this module. Else hide from the navigation bar

    public static function availableForNavigation(Request $request)
    {
        if($request->user()->getAuthIdentifier()!=1){
            return false;
        }else{
            return true;
        }
       
    }
    public static function label() 
    { 
        return __("UserSafe"); 
    }
    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'email', 'name', 'contact_email',
        'birthdate', 'phone'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable()->hideFromIndex()->hideFromDetail(),

            Text::make(__('Name'), 'name')
                ->sortable()
                ->rules('required', 'max:255'),

            BelongsTo::make(__('Role'), 'role', 'App\Nova\Role')->hideFromDetail()->hideWhenCreating()->hideWhenUpdating()->sortable()->nullable(),
            DepartmentPosition::make(__('Title'), 'title', 'App\Nova\Title')->sortable()->nullable()->hideFromDetail()->hideWhenCreating()->hideWhenUpdating(),

            Select::make(__('Gender'), 'gender')->options([
                    'Male' => 'Male',
                    'Female' => 'Female',
                    'Other' => 'Other',
                ])
                ->sortable()
                ->rules('required')
                ->hideFromIndex(),

            DateInputBox::make(__('BirthDate'), 'birthdate')
                ->sortable()
                ->rules('required','max:10')
                ->withMeta(['extraAttributes' => [ 'colored' => false]])
                ->hideFromIndex(),

            TelephoneInputBox::make(__('Phone'), 'phone')
                ->sortable()
                ->rules('required', 'max:20')
                ->hideFromIndex(),

                //邮箱
            Text::make(__('ContactEmail'), 'contact_email')
                ->sortable()
                ->rules('required', 'email', 'max:254')->hideFromIndex(),

                //员工身份
            Select::make(__('Identity'), 'identity')->options([
                    'SP' => 'SP',
                    'WP' => 'WP',
                    'VR' => 'VR',
                    'PR' => 'PR',
                    'CC' => 'CC',
                    'CN' => 'CN'
                ])
                ->sortable()
                ->rules('required')
                ->hideFromIndex(),

            DateInputBox::make(__('JoinDate'), 'join_date')
                ->sortable()
                ->withMeta(['extraAttributes' => [ 'colored' => false]])
                ->rules('required')->hideFromIndex(),

            Department::make(__('Department'), 'department', 'App\Nova\Department')->sortable()->nullable()->hideFromIndex(),
            DepartmentPosition::make(__('Title'), 'title', 'App\Nova\Title')->sortable()->nullable()->hideFromIndex(),
            BelongsTo::make(__('JobLevel'), 'jobLevel', 'App\Nova\JobLevel')->nullable()->sortable()->hideFromIndex(),
    
            CurrencyInputBox::make(__('salary'), 'salary')->sortable(),

                //员工身份
            Select::make(__('Identity'), 'identity')->options([
                    'SP' => 'SP',
                    'WP' => 'WP',
                    'VR' => 'VR',
                    'PR' => 'PR',
                    'CC' => 'CC',
                    'CN' => 'CN'
                ])
                ->sortable()
                ->rules('required')
                ->hideFromDetail()->hideWhenCreating()->hideWhenUpdating(),


                //员工身份
            SinInputBox::make(__('SIN_number'), 'sin_number')
                ->sortable()
                ->hideFromDetail()->hideWhenCreating()->hideWhenUpdating(),

            Text::make(__('passport'), 'passport')
                ->sortable(),
    
            SinInputBox::make(__('SIN_number'), 'sin_number')
                ->sortable()
                ->hideFromIndex(),
            

            Text::make(__('LoginEmail'), 'email')
                ->sortable()
                ->rules('required', 'email', 'max:254')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}')->hideFromIndex(),

            Password::make(__('LoginPassword'), 'Password')
                ->onlyOnForms()
                ->creationRules('required', 'string', 'min:8')
                ->updateRules('nullable', 'string', 'min:8'),

       
            BelongsTo::make(__('Role'), 'role', 'App\Nova\Role')->sortable()->nullable()->hideFromIndex(),

            new Panel(__("AddressDetail"), $this->addressField()),
            

            HasMany::make(__('Clients'), 'clients', 'App\Nova\Client')->hideFromIndex()->nullable(),
            //$this->addressField()
        ];
    }

    public function addressField(){
        return [
            Text::make(__('Address'), 'address')->hideFromIndex(),
            Text::make(__('City'), 'city')->hideFromIndex(),
            Text::make(__('Province'), 'province')->hideFromIndex(),
            Text::make(__('postal_code'), 'postal_code')->hideFromIndex()
        
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            new FilterByDepartment,
            new FilterByRoles
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
