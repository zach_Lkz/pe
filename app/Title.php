<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Title extends Model
{
    //
    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function titles()
    {
        return $this->hasMany('App\Title', "parent_title_id");
    }

    public function parentTitle()
    {
        return $this->belongsTo('App\Title', "parent_title_id");
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }
    
    //test required
    public function getClients(){
        $result = [];
        if ($this->titles && $this->titles->count() > 0) {
            
            foreach($this->titles as $low_title){
                $client_array = $low_title->getClients();
                if ($client_array){
                     $result = array_merge($result, $client_array);
                }
            }
        } 

        foreach($this->users as $user){
            if ($user->clients){
                Log::info(json_encode($user->clients));
                $client_array = $user->clients->map(function ($item, $key) {
                    return $item->id;
                });
                $client_array =  $client_array->toArray();
                //Log::info(json_encode($client_array));
                if ($client_array){
                    $result = array_merge($result, $client_array);
                }
            }
        }
        return $result;
        
    }


}
