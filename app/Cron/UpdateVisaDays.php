<?php

namespace App\Cron;

use App\Client;

class UpdateVisaDays
{
    public function __invoke()
    {
        $clients = Client::all();

        foreach($clients as $client){
            $client->updateExpireDays();
        }
    }
}
?>