<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    //
    public function province()
    {
        return $this->hasMany('App\Province');
    }
    public function cities()
    {
        return $this->hasMany('App\City');
    }

}
