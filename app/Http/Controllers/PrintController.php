<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Client;
use App\City;
use App\Province;
use App\Region;
use App\User;

class PrintController extends Controller
{
    /**
     * Print page for company client
     */
    public function company(Request $request, $id=0) {
        $client = Client::where("id", "=", $id)->first();
        $original_city = City::where("id", "=", $client->original_city_id)->first();
        $original_province = Province::where("id", "=", $client->original_province_id)->first();
        $original_region = Region::where("id", "=", $client->original_region_id)->first();
        $birth_city = City::where("id", "=", $client->birth_city_id)->first();
        $birth_province = Province::where("id", "=", $client->birth_province_id)->first();
        $birth_region = Region::where("id", "=", $client->birth_region_id)->first();
        $user = User::where("id", "=", $client->user_id)->first();
        return view('client_print',[
            "client" => $client, 
            "original_city" => $original_city, 
            "original_province" => $original_province, 
            "original_region" => $original_region, 
            "birth_city" => $birth_city, 
            "birth_province" => $birth_province, 
            "birth_region" => $birth_region, 
            "user" => $user]);
    }
}
