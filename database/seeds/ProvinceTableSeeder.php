<?php

use Illuminate\Database\Seeder;

class ProvinceTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('provinces')->delete();
        
        \DB::table('provinces')->insert(array (
            0 => 
            array (
                'id' => 11,
                'name' => '北京市',
                'region_id' => 1,
            ),
            1 => 
            array (
                'id' => 12,
                'name' => '天津市',
                'region_id' => 1,
            ),
            2 => 
            array (
                'id' => 13,
                'name' => '河北省',
                'region_id' => 1,
            ),
            3 => 
            array (
                'id' => 14,
                'name' => '山西省',
                'region_id' => 1,
            ),
            4 => 
            array (
                'id' => 15,
                'name' => '内蒙古自治区',
                'region_id' => 1,
            ),
            5 => 
            array (
                'id' => 21,
                'name' => '辽宁省',
                'region_id' => 2,
            ),
            6 => 
            array (
                'id' => 22,
                'name' => '吉林省',
                'region_id' => 2,
            ),
            7 => 
            array (
                'id' => 23,
                'name' => '黑龙江省',
                'region_id' => 2,
            ),
            8 => 
            array (
                'id' => 31,
                'name' => '上海市',
                'region_id' => 3,
            ),
            9 => 
            array (
                'id' => 32,
                'name' => '江苏省',
                'region_id' => 3,
            ),
            10 => 
            array (
                'id' => 33,
                'name' => '浙江省',
                'region_id' => 3,
            ),
            11 => 
            array (
                'id' => 34,
                'name' => '安徽省',
                'region_id' => 3,
            ),
            12 => 
            array (
                'id' => 35,
                'name' => '福建省',
                'region_id' => 3,
            ),
            13 => 
            array (
                'id' => 36,
                'name' => '江西省',
                'region_id' => 3,
            ),
            14 => 
            array (
                'id' => 37,
                'name' => '山东省',
                'region_id' => 3,
            ),
            15 => 
            array (
                'id' => 41,
                'name' => '河南省',
                'region_id' => 4,
            ),
            16 => 
            array (
                'id' => 42,
                'name' => '湖北省',
                'region_id' => 4,
            ),
            17 => 
            array (
                'id' => 43,
                'name' => '湖南省',
                'region_id' => 4,
            ),
            18 => 
            array (
                'id' => 44,
                'name' => '广东省',
                'region_id' => 4,
            ),
            19 => 
            array (
                'id' => 45,
                'name' => '广西壮族自治区',
                'region_id' => 4,
            ),
            20 => 
            array (
                'id' => 46,
                'name' => '海南省',
                'region_id' => 4,
            ),
            21 => 
            array (
                'id' => 50,
                'name' => '重庆市',
                'region_id' => 5,
            ),
            22 => 
            array (
                'id' => 51,
                'name' => '四川省',
                'region_id' => 5,
            ),
            23 => 
            array (
                'id' => 52,
                'name' => '贵州省',
                'region_id' => 5,
            ),
            24 => 
            array (
                'id' => 53,
                'name' => '云南省',
                'region_id' => 5,
            ),
            25 => 
            array (
                'id' => 54,
                'name' => '西藏自治区',
                'region_id' => 5,
            ),
            26 => 
            array (
                'id' => 61,
                'name' => '陕西省',
                'region_id' => 6,
            ),
            27 => 
            array (
                'id' => 62,
                'name' => '甘肃省',
                'region_id' => 6,
            ),
            28 => 
            array (
                'id' => 63,
                'name' => '青海省',
                'region_id' => 6,
            ),
            29 => 
            array (
                'id' => 64,
                'name' => '宁夏回族自治区',
                'region_id' => 6,
            ),
            30 => 
            array (
                'id' => 65,
                'name' => '新疆维吾尔自治区',
                'region_id' => 6,
            ),
        ));
        
        
    }
}