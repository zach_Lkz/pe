<?php

use Illuminate\Database\Seeder;

class RegionTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('regions')->delete();
        
        \DB::table('regions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => '华北地区',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => '东北地区',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => '华东地区',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => '中南地区',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => '西南地区',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => '西北地区',
            ),
        ));
        
        
    }
}