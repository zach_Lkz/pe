<?php

use Illuminate\Database\Seeder;

class CityTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cities')->delete();
        
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 1101,
                'name' => '北京市',
                'province_id' => 11,
                'region_id' => 1,
            ),
            1 => 
            array (
                'id' => 1201,
                'name' => '天津市',
                'province_id' => 12,
                'region_id' => 1,
            ),
            2 => 
            array (
                'id' => 1301,
                'name' => '石家庄市',
                'province_id' => 13,
                'region_id' => 1,
            ),
            3 => 
            array (
                'id' => 1302,
                'name' => '唐山市',
                'province_id' => 13,
                'region_id' => 1,
            ),
            4 => 
            array (
                'id' => 1303,
                'name' => '秦皇岛市',
                'province_id' => 13,
                'region_id' => 1,
            ),
            5 => 
            array (
                'id' => 1304,
                'name' => '邯郸市',
                'province_id' => 13,
                'region_id' => 1,
            ),
            6 => 
            array (
                'id' => 1305,
                'name' => '邢台市',
                'province_id' => 13,
                'region_id' => 1,
            ),
            7 => 
            array (
                'id' => 1306,
                'name' => '保定市',
                'province_id' => 13,
                'region_id' => 1,
            ),
            8 => 
            array (
                'id' => 1307,
                'name' => '张家口市',
                'province_id' => 13,
                'region_id' => 1,
            ),
            9 => 
            array (
                'id' => 1308,
                'name' => '承德市',
                'province_id' => 13,
                'region_id' => 1,
            ),
            10 => 
            array (
                'id' => 1309,
                'name' => '沧州市',
                'province_id' => 13,
                'region_id' => 1,
            ),
            11 => 
            array (
                'id' => 1310,
                'name' => '廊坊市',
                'province_id' => 13,
                'region_id' => 1,
            ),
            12 => 
            array (
                'id' => 1311,
                'name' => '衡水市',
                'province_id' => 13,
                'region_id' => 1,
            ),
            13 => 
            array (
                'id' => 1401,
                'name' => '太原市',
                'province_id' => 14,
                'region_id' => 1,
            ),
            14 => 
            array (
                'id' => 1402,
                'name' => '大同市',
                'province_id' => 14,
                'region_id' => 1,
            ),
            15 => 
            array (
                'id' => 1403,
                'name' => '阳泉市',
                'province_id' => 14,
                'region_id' => 1,
            ),
            16 => 
            array (
                'id' => 1404,
                'name' => '长治市',
                'province_id' => 14,
                'region_id' => 1,
            ),
            17 => 
            array (
                'id' => 1405,
                'name' => '晋城市',
                'province_id' => 14,
                'region_id' => 1,
            ),
            18 => 
            array (
                'id' => 1406,
                'name' => '朔州市',
                'province_id' => 14,
                'region_id' => 1,
            ),
            19 => 
            array (
                'id' => 1407,
                'name' => '晋中市',
                'province_id' => 14,
                'region_id' => 1,
            ),
            20 => 
            array (
                'id' => 1408,
                'name' => '运城市',
                'province_id' => 14,
                'region_id' => 1,
            ),
            21 => 
            array (
                'id' => 1409,
                'name' => '忻州市',
                'province_id' => 14,
                'region_id' => 1,
            ),
            22 => 
            array (
                'id' => 1410,
                'name' => '临汾市',
                'province_id' => 14,
                'region_id' => 1,
            ),
            23 => 
            array (
                'id' => 1411,
                'name' => '吕梁市',
                'province_id' => 14,
                'region_id' => 1,
            ),
            24 => 
            array (
                'id' => 1501,
                'name' => '呼和浩特市',
                'province_id' => 15,
                'region_id' => 1,
            ),
            25 => 
            array (
                'id' => 1502,
                'name' => '包头市',
                'province_id' => 15,
                'region_id' => 1,
            ),
            26 => 
            array (
                'id' => 1503,
                'name' => '乌海市',
                'province_id' => 15,
                'region_id' => 1,
            ),
            27 => 
            array (
                'id' => 1504,
                'name' => '赤峰市',
                'province_id' => 15,
                'region_id' => 1,
            ),
            28 => 
            array (
                'id' => 1505,
                'name' => '通辽市',
                'province_id' => 15,
                'region_id' => 1,
            ),
            29 => 
            array (
                'id' => 1506,
                'name' => '鄂尔多斯市',
                'province_id' => 15,
                'region_id' => 1,
            ),
            30 => 
            array (
                'id' => 1507,
                'name' => '呼伦贝尔市',
                'province_id' => 15,
                'region_id' => 1,
            ),
            31 => 
            array (
                'id' => 1508,
                'name' => '巴彦淖尔市',
                'province_id' => 15,
                'region_id' => 1,
            ),
            32 => 
            array (
                'id' => 1509,
                'name' => '乌兰察布市',
                'province_id' => 15,
                'region_id' => 1,
            ),
            33 => 
            array (
                'id' => 1522,
                'name' => '兴安盟',
                'province_id' => 15,
                'region_id' => 1,
            ),
            34 => 
            array (
                'id' => 1525,
                'name' => '锡林郭勒盟',
                'province_id' => 15,
                'region_id' => 1,
            ),
            35 => 
            array (
                'id' => 1529,
                'name' => '阿拉善盟',
                'province_id' => 15,
                'region_id' => 1,
            ),
            36 => 
            array (
                'id' => 2101,
                'name' => '沈阳市',
                'province_id' => 21,
                'region_id' => 2,
            ),
            37 => 
            array (
                'id' => 2102,
                'name' => '大连市',
                'province_id' => 21,
                'region_id' => 2,
            ),
            38 => 
            array (
                'id' => 2103,
                'name' => '鞍山市',
                'province_id' => 21,
                'region_id' => 2,
            ),
            39 => 
            array (
                'id' => 2104,
                'name' => '抚顺市',
                'province_id' => 21,
                'region_id' => 2,
            ),
            40 => 
            array (
                'id' => 2105,
                'name' => '本溪市',
                'province_id' => 21,
                'region_id' => 2,
            ),
            41 => 
            array (
                'id' => 2106,
                'name' => '丹东市',
                'province_id' => 21,
                'region_id' => 2,
            ),
            42 => 
            array (
                'id' => 2107,
                'name' => '锦州市',
                'province_id' => 21,
                'region_id' => 2,
            ),
            43 => 
            array (
                'id' => 2108,
                'name' => '营口市',
                'province_id' => 21,
                'region_id' => 2,
            ),
            44 => 
            array (
                'id' => 2109,
                'name' => '阜新市',
                'province_id' => 21,
                'region_id' => 2,
            ),
            45 => 
            array (
                'id' => 2110,
                'name' => '辽阳市',
                'province_id' => 21,
                'region_id' => 2,
            ),
            46 => 
            array (
                'id' => 2111,
                'name' => '盘锦市',
                'province_id' => 21,
                'region_id' => 2,
            ),
            47 => 
            array (
                'id' => 2112,
                'name' => '铁岭市',
                'province_id' => 21,
                'region_id' => 2,
            ),
            48 => 
            array (
                'id' => 2113,
                'name' => '朝阳市',
                'province_id' => 21,
                'region_id' => 2,
            ),
            49 => 
            array (
                'id' => 2114,
                'name' => '葫芦岛市',
                'province_id' => 21,
                'region_id' => 2,
            ),
            50 => 
            array (
                'id' => 2201,
                'name' => '长春市',
                'province_id' => 22,
                'region_id' => 2,
            ),
            51 => 
            array (
                'id' => 2202,
                'name' => '吉林市',
                'province_id' => 22,
                'region_id' => 2,
            ),
            52 => 
            array (
                'id' => 2203,
                'name' => '四平市',
                'province_id' => 22,
                'region_id' => 2,
            ),
            53 => 
            array (
                'id' => 2204,
                'name' => '辽源市',
                'province_id' => 22,
                'region_id' => 2,
            ),
            54 => 
            array (
                'id' => 2205,
                'name' => '通化市',
                'province_id' => 22,
                'region_id' => 2,
            ),
            55 => 
            array (
                'id' => 2206,
                'name' => '白山市',
                'province_id' => 22,
                'region_id' => 2,
            ),
            56 => 
            array (
                'id' => 2207,
                'name' => '松原市',
                'province_id' => 22,
                'region_id' => 2,
            ),
            57 => 
            array (
                'id' => 2208,
                'name' => '白城市',
                'province_id' => 22,
                'region_id' => 2,
            ),
            58 => 
            array (
                'id' => 2224,
                'name' => '延边朝鲜族自治州',
                'province_id' => 22,
                'region_id' => 2,
            ),
            59 => 
            array (
                'id' => 2301,
                'name' => '哈尔滨市',
                'province_id' => 23,
                'region_id' => 2,
            ),
            60 => 
            array (
                'id' => 2302,
                'name' => '齐齐哈尔市',
                'province_id' => 23,
                'region_id' => 2,
            ),
            61 => 
            array (
                'id' => 2303,
                'name' => '鸡西市',
                'province_id' => 23,
                'region_id' => 2,
            ),
            62 => 
            array (
                'id' => 2304,
                'name' => '鹤岗市',
                'province_id' => 23,
                'region_id' => 2,
            ),
            63 => 
            array (
                'id' => 2305,
                'name' => '双鸭山市',
                'province_id' => 23,
                'region_id' => 2,
            ),
            64 => 
            array (
                'id' => 2306,
                'name' => '大庆市',
                'province_id' => 23,
                'region_id' => 2,
            ),
            65 => 
            array (
                'id' => 2307,
                'name' => '伊春市',
                'province_id' => 23,
                'region_id' => 2,
            ),
            66 => 
            array (
                'id' => 2308,
                'name' => '佳木斯市',
                'province_id' => 23,
                'region_id' => 2,
            ),
            67 => 
            array (
                'id' => 2309,
                'name' => '七台河市',
                'province_id' => 23,
                'region_id' => 2,
            ),
            68 => 
            array (
                'id' => 2310,
                'name' => '牡丹江市',
                'province_id' => 23,
                'region_id' => 2,
            ),
            69 => 
            array (
                'id' => 2311,
                'name' => '黑河市',
                'province_id' => 23,
                'region_id' => 2,
            ),
            70 => 
            array (
                'id' => 2312,
                'name' => '绥化市',
                'province_id' => 23,
                'region_id' => 2,
            ),
            71 => 
            array (
                'id' => 2327,
                'name' => '大兴安岭地区',
                'province_id' => 23,
                'region_id' => 2,
            ),
            72 => 
            array (
                'id' => 3101,
                'name' => '上海市',
                'province_id' => 31,
                'region_id' => 3,
            ),
            73 => 
            array (
                'id' => 3201,
                'name' => '南京市',
                'province_id' => 32,
                'region_id' => 3,
            ),
            74 => 
            array (
                'id' => 3202,
                'name' => '无锡市',
                'province_id' => 32,
                'region_id' => 3,
            ),
            75 => 
            array (
                'id' => 3203,
                'name' => '徐州市',
                'province_id' => 32,
                'region_id' => 3,
            ),
            76 => 
            array (
                'id' => 3204,
                'name' => '常州市',
                'province_id' => 32,
                'region_id' => 3,
            ),
            77 => 
            array (
                'id' => 3205,
                'name' => '苏州市',
                'province_id' => 32,
                'region_id' => 3,
            ),
            78 => 
            array (
                'id' => 3206,
                'name' => '南通市',
                'province_id' => 32,
                'region_id' => 3,
            ),
            79 => 
            array (
                'id' => 3207,
                'name' => '连云港市',
                'province_id' => 32,
                'region_id' => 3,
            ),
            80 => 
            array (
                'id' => 3208,
                'name' => '淮安市',
                'province_id' => 32,
                'region_id' => 3,
            ),
            81 => 
            array (
                'id' => 3209,
                'name' => '盐城市',
                'province_id' => 32,
                'region_id' => 3,
            ),
            82 => 
            array (
                'id' => 3210,
                'name' => '扬州市',
                'province_id' => 32,
                'region_id' => 3,
            ),
            83 => 
            array (
                'id' => 3211,
                'name' => '镇江市',
                'province_id' => 32,
                'region_id' => 3,
            ),
            84 => 
            array (
                'id' => 3212,
                'name' => '泰州市',
                'province_id' => 32,
                'region_id' => 3,
            ),
            85 => 
            array (
                'id' => 3213,
                'name' => '宿迁市',
                'province_id' => 32,
                'region_id' => 3,
            ),
            86 => 
            array (
                'id' => 3301,
                'name' => '杭州市',
                'province_id' => 33,
                'region_id' => 3,
            ),
            87 => 
            array (
                'id' => 3302,
                'name' => '宁波市',
                'province_id' => 33,
                'region_id' => 3,
            ),
            88 => 
            array (
                'id' => 3303,
                'name' => '温州市',
                'province_id' => 33,
                'region_id' => 3,
            ),
            89 => 
            array (
                'id' => 3304,
                'name' => '嘉兴市',
                'province_id' => 33,
                'region_id' => 3,
            ),
            90 => 
            array (
                'id' => 3305,
                'name' => '湖州市',
                'province_id' => 33,
                'region_id' => 3,
            ),
            91 => 
            array (
                'id' => 3306,
                'name' => '绍兴市',
                'province_id' => 33,
                'region_id' => 3,
            ),
            92 => 
            array (
                'id' => 3307,
                'name' => '金华市',
                'province_id' => 33,
                'region_id' => 3,
            ),
            93 => 
            array (
                'id' => 3308,
                'name' => '衢州市',
                'province_id' => 33,
                'region_id' => 3,
            ),
            94 => 
            array (
                'id' => 3309,
                'name' => '舟山市',
                'province_id' => 33,
                'region_id' => 3,
            ),
            95 => 
            array (
                'id' => 3310,
                'name' => '台州市',
                'province_id' => 33,
                'region_id' => 3,
            ),
            96 => 
            array (
                'id' => 3311,
                'name' => '丽水市',
                'province_id' => 33,
                'region_id' => 3,
            ),
            97 => 
            array (
                'id' => 3401,
                'name' => '合肥市',
                'province_id' => 34,
                'region_id' => 3,
            ),
            98 => 
            array (
                'id' => 3402,
                'name' => '芜湖市',
                'province_id' => 34,
                'region_id' => 3,
            ),
            99 => 
            array (
                'id' => 3403,
                'name' => '蚌埠市',
                'province_id' => 34,
                'region_id' => 3,
            ),
            100 => 
            array (
                'id' => 3404,
                'name' => '淮南市',
                'province_id' => 34,
                'region_id' => 3,
            ),
            101 => 
            array (
                'id' => 3405,
                'name' => '马鞍山市',
                'province_id' => 34,
                'region_id' => 3,
            ),
            102 => 
            array (
                'id' => 3406,
                'name' => '淮北市',
                'province_id' => 34,
                'region_id' => 3,
            ),
            103 => 
            array (
                'id' => 3407,
                'name' => '铜陵市',
                'province_id' => 34,
                'region_id' => 3,
            ),
            104 => 
            array (
                'id' => 3408,
                'name' => '安庆市',
                'province_id' => 34,
                'region_id' => 3,
            ),
            105 => 
            array (
                'id' => 3410,
                'name' => '黄山市',
                'province_id' => 34,
                'region_id' => 3,
            ),
            106 => 
            array (
                'id' => 3411,
                'name' => '滁州市',
                'province_id' => 34,
                'region_id' => 3,
            ),
            107 => 
            array (
                'id' => 3412,
                'name' => '阜阳市',
                'province_id' => 34,
                'region_id' => 3,
            ),
            108 => 
            array (
                'id' => 3413,
                'name' => '宿州市',
                'province_id' => 34,
                'region_id' => 3,
            ),
            109 => 
            array (
                'id' => 3415,
                'name' => '六安市',
                'province_id' => 34,
                'region_id' => 3,
            ),
            110 => 
            array (
                'id' => 3416,
                'name' => '亳州市',
                'province_id' => 34,
                'region_id' => 3,
            ),
            111 => 
            array (
                'id' => 3417,
                'name' => '池州市',
                'province_id' => 34,
                'region_id' => 3,
            ),
            112 => 
            array (
                'id' => 3418,
                'name' => '宣城市',
                'province_id' => 34,
                'region_id' => 3,
            ),
            113 => 
            array (
                'id' => 3501,
                'name' => '福州市',
                'province_id' => 35,
                'region_id' => 3,
            ),
            114 => 
            array (
                'id' => 3502,
                'name' => '厦门市',
                'province_id' => 35,
                'region_id' => 3,
            ),
            115 => 
            array (
                'id' => 3503,
                'name' => '莆田市',
                'province_id' => 35,
                'region_id' => 3,
            ),
            116 => 
            array (
                'id' => 3504,
                'name' => '三明市',
                'province_id' => 35,
                'region_id' => 3,
            ),
            117 => 
            array (
                'id' => 3505,
                'name' => '泉州市',
                'province_id' => 35,
                'region_id' => 3,
            ),
            118 => 
            array (
                'id' => 3506,
                'name' => '漳州市',
                'province_id' => 35,
                'region_id' => 3,
            ),
            119 => 
            array (
                'id' => 3507,
                'name' => '南平市',
                'province_id' => 35,
                'region_id' => 3,
            ),
            120 => 
            array (
                'id' => 3508,
                'name' => '龙岩市',
                'province_id' => 35,
                'region_id' => 3,
            ),
            121 => 
            array (
                'id' => 3509,
                'name' => '宁德市',
                'province_id' => 35,
                'region_id' => 3,
            ),
            122 => 
            array (
                'id' => 3601,
                'name' => '南昌市',
                'province_id' => 36,
                'region_id' => 3,
            ),
            123 => 
            array (
                'id' => 3602,
                'name' => '景德镇市',
                'province_id' => 36,
                'region_id' => 3,
            ),
            124 => 
            array (
                'id' => 3603,
                'name' => '萍乡市',
                'province_id' => 36,
                'region_id' => 3,
            ),
            125 => 
            array (
                'id' => 3604,
                'name' => '九江市',
                'province_id' => 36,
                'region_id' => 3,
            ),
            126 => 
            array (
                'id' => 3605,
                'name' => '新余市',
                'province_id' => 36,
                'region_id' => 3,
            ),
            127 => 
            array (
                'id' => 3606,
                'name' => '鹰潭市',
                'province_id' => 36,
                'region_id' => 3,
            ),
            128 => 
            array (
                'id' => 3607,
                'name' => '赣州市',
                'province_id' => 36,
                'region_id' => 3,
            ),
            129 => 
            array (
                'id' => 3608,
                'name' => '吉安市',
                'province_id' => 36,
                'region_id' => 3,
            ),
            130 => 
            array (
                'id' => 3609,
                'name' => '宜春市',
                'province_id' => 36,
                'region_id' => 3,
            ),
            131 => 
            array (
                'id' => 3610,
                'name' => '抚州市',
                'province_id' => 36,
                'region_id' => 3,
            ),
            132 => 
            array (
                'id' => 3611,
                'name' => '上饶市',
                'province_id' => 36,
                'region_id' => 3,
            ),
            133 => 
            array (
                'id' => 3701,
                'name' => '济南市',
                'province_id' => 37,
                'region_id' => 3,
            ),
            134 => 
            array (
                'id' => 3702,
                'name' => '青岛市',
                'province_id' => 37,
                'region_id' => 3,
            ),
            135 => 
            array (
                'id' => 3703,
                'name' => '淄博市',
                'province_id' => 37,
                'region_id' => 3,
            ),
            136 => 
            array (
                'id' => 3704,
                'name' => '枣庄市',
                'province_id' => 37,
                'region_id' => 3,
            ),
            137 => 
            array (
                'id' => 3705,
                'name' => '东营市',
                'province_id' => 37,
                'region_id' => 3,
            ),
            138 => 
            array (
                'id' => 3706,
                'name' => '烟台市',
                'province_id' => 37,
                'region_id' => 3,
            ),
            139 => 
            array (
                'id' => 3707,
                'name' => '潍坊市',
                'province_id' => 37,
                'region_id' => 3,
            ),
            140 => 
            array (
                'id' => 3708,
                'name' => '济宁市',
                'province_id' => 37,
                'region_id' => 3,
            ),
            141 => 
            array (
                'id' => 3709,
                'name' => '泰安市',
                'province_id' => 37,
                'region_id' => 3,
            ),
            142 => 
            array (
                'id' => 3710,
                'name' => '威海市',
                'province_id' => 37,
                'region_id' => 3,
            ),
            143 => 
            array (
                'id' => 3711,
                'name' => '日照市',
                'province_id' => 37,
                'region_id' => 3,
            ),
            144 => 
            array (
                'id' => 3712,
                'name' => '莱芜市',
                'province_id' => 37,
                'region_id' => 3,
            ),
            145 => 
            array (
                'id' => 3713,
                'name' => '临沂市',
                'province_id' => 37,
                'region_id' => 3,
            ),
            146 => 
            array (
                'id' => 3714,
                'name' => '德州市',
                'province_id' => 37,
                'region_id' => 3,
            ),
            147 => 
            array (
                'id' => 3715,
                'name' => '聊城市',
                'province_id' => 37,
                'region_id' => 3,
            ),
            148 => 
            array (
                'id' => 3716,
                'name' => '滨州市',
                'province_id' => 37,
                'region_id' => 3,
            ),
            149 => 
            array (
                'id' => 3717,
                'name' => '菏泽市',
                'province_id' => 37,
                'region_id' => 3,
            ),
            150 => 
            array (
                'id' => 4101,
                'name' => '郑州市',
                'province_id' => 41,
                'region_id' => 4,
            ),
            151 => 
            array (
                'id' => 4102,
                'name' => '开封市',
                'province_id' => 41,
                'region_id' => 4,
            ),
            152 => 
            array (
                'id' => 4103,
                'name' => '洛阳市',
                'province_id' => 41,
                'region_id' => 4,
            ),
            153 => 
            array (
                'id' => 4104,
                'name' => '平顶山市',
                'province_id' => 41,
                'region_id' => 4,
            ),
            154 => 
            array (
                'id' => 4105,
                'name' => '安阳市',
                'province_id' => 41,
                'region_id' => 4,
            ),
            155 => 
            array (
                'id' => 4106,
                'name' => '鹤壁市',
                'province_id' => 41,
                'region_id' => 4,
            ),
            156 => 
            array (
                'id' => 4107,
                'name' => '新乡市',
                'province_id' => 41,
                'region_id' => 4,
            ),
            157 => 
            array (
                'id' => 4108,
                'name' => '焦作市',
                'province_id' => 41,
                'region_id' => 4,
            ),
            158 => 
            array (
                'id' => 4109,
                'name' => '濮阳市',
                'province_id' => 41,
                'region_id' => 4,
            ),
            159 => 
            array (
                'id' => 4110,
                'name' => '许昌市',
                'province_id' => 41,
                'region_id' => 4,
            ),
            160 => 
            array (
                'id' => 4111,
                'name' => '漯河市',
                'province_id' => 41,
                'region_id' => 4,
            ),
            161 => 
            array (
                'id' => 4112,
                'name' => '三门峡市',
                'province_id' => 41,
                'region_id' => 4,
            ),
            162 => 
            array (
                'id' => 4113,
                'name' => '南阳市',
                'province_id' => 41,
                'region_id' => 4,
            ),
            163 => 
            array (
                'id' => 4114,
                'name' => '商丘市',
                'province_id' => 41,
                'region_id' => 4,
            ),
            164 => 
            array (
                'id' => 4115,
                'name' => '信阳市',
                'province_id' => 41,
                'region_id' => 4,
            ),
            165 => 
            array (
                'id' => 4116,
                'name' => '周口市',
                'province_id' => 41,
                'region_id' => 4,
            ),
            166 => 
            array (
                'id' => 4117,
                'name' => '驻马店市',
                'province_id' => 41,
                'region_id' => 4,
            ),
            167 => 
            array (
                'id' => 4190,
                'name' => '省直辖县级行政区划',
                'province_id' => 41,
                'region_id' => 4,
            ),
            168 => 
            array (
                'id' => 4201,
                'name' => '武汉市',
                'province_id' => 42,
                'region_id' => 4,
            ),
            169 => 
            array (
                'id' => 4202,
                'name' => '黄石市',
                'province_id' => 42,
                'region_id' => 4,
            ),
            170 => 
            array (
                'id' => 4203,
                'name' => '十堰市',
                'province_id' => 42,
                'region_id' => 4,
            ),
            171 => 
            array (
                'id' => 4205,
                'name' => '宜昌市',
                'province_id' => 42,
                'region_id' => 4,
            ),
            172 => 
            array (
                'id' => 4206,
                'name' => '襄阳市',
                'province_id' => 42,
                'region_id' => 4,
            ),
            173 => 
            array (
                'id' => 4207,
                'name' => '鄂州市',
                'province_id' => 42,
                'region_id' => 4,
            ),
            174 => 
            array (
                'id' => 4208,
                'name' => '荆门市',
                'province_id' => 42,
                'region_id' => 4,
            ),
            175 => 
            array (
                'id' => 4209,
                'name' => '孝感市',
                'province_id' => 42,
                'region_id' => 4,
            ),
            176 => 
            array (
                'id' => 4210,
                'name' => '荆州市',
                'province_id' => 42,
                'region_id' => 4,
            ),
            177 => 
            array (
                'id' => 4211,
                'name' => '黄冈市',
                'province_id' => 42,
                'region_id' => 4,
            ),
            178 => 
            array (
                'id' => 4212,
                'name' => '咸宁市',
                'province_id' => 42,
                'region_id' => 4,
            ),
            179 => 
            array (
                'id' => 4213,
                'name' => '随州市',
                'province_id' => 42,
                'region_id' => 4,
            ),
            180 => 
            array (
                'id' => 4228,
                'name' => '恩施土家族苗族自治州',
                'province_id' => 42,
                'region_id' => 4,
            ),
            181 => 
            array (
                'id' => 4290,
                'name' => '省直辖县级行政区划',
                'province_id' => 42,
                'region_id' => 4,
            ),
            182 => 
            array (
                'id' => 4301,
                'name' => '长沙市',
                'province_id' => 43,
                'region_id' => 4,
            ),
            183 => 
            array (
                'id' => 4302,
                'name' => '株洲市',
                'province_id' => 43,
                'region_id' => 4,
            ),
            184 => 
            array (
                'id' => 4303,
                'name' => '湘潭市',
                'province_id' => 43,
                'region_id' => 4,
            ),
            185 => 
            array (
                'id' => 4304,
                'name' => '衡阳市',
                'province_id' => 43,
                'region_id' => 4,
            ),
            186 => 
            array (
                'id' => 4305,
                'name' => '邵阳市',
                'province_id' => 43,
                'region_id' => 4,
            ),
            187 => 
            array (
                'id' => 4306,
                'name' => '岳阳市',
                'province_id' => 43,
                'region_id' => 4,
            ),
            188 => 
            array (
                'id' => 4307,
                'name' => '常德市',
                'province_id' => 43,
                'region_id' => 4,
            ),
            189 => 
            array (
                'id' => 4308,
                'name' => '张家界市',
                'province_id' => 43,
                'region_id' => 4,
            ),
            190 => 
            array (
                'id' => 4309,
                'name' => '益阳市',
                'province_id' => 43,
                'region_id' => 4,
            ),
            191 => 
            array (
                'id' => 4310,
                'name' => '郴州市',
                'province_id' => 43,
                'region_id' => 4,
            ),
            192 => 
            array (
                'id' => 4311,
                'name' => '永州市',
                'province_id' => 43,
                'region_id' => 4,
            ),
            193 => 
            array (
                'id' => 4312,
                'name' => '怀化市',
                'province_id' => 43,
                'region_id' => 4,
            ),
            194 => 
            array (
                'id' => 4313,
                'name' => '娄底市',
                'province_id' => 43,
                'region_id' => 4,
            ),
            195 => 
            array (
                'id' => 4331,
                'name' => '湘西土家族苗族自治州',
                'province_id' => 43,
                'region_id' => 4,
            ),
            196 => 
            array (
                'id' => 4401,
                'name' => '广州市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            197 => 
            array (
                'id' => 4402,
                'name' => '韶关市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            198 => 
            array (
                'id' => 4403,
                'name' => '深圳市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            199 => 
            array (
                'id' => 4404,
                'name' => '珠海市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            200 => 
            array (
                'id' => 4405,
                'name' => '汕头市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            201 => 
            array (
                'id' => 4406,
                'name' => '佛山市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            202 => 
            array (
                'id' => 4407,
                'name' => '江门市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            203 => 
            array (
                'id' => 4408,
                'name' => '湛江市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            204 => 
            array (
                'id' => 4409,
                'name' => '茂名市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            205 => 
            array (
                'id' => 4412,
                'name' => '肇庆市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            206 => 
            array (
                'id' => 4413,
                'name' => '惠州市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            207 => 
            array (
                'id' => 4414,
                'name' => '梅州市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            208 => 
            array (
                'id' => 4415,
                'name' => '汕尾市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            209 => 
            array (
                'id' => 4416,
                'name' => '河源市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            210 => 
            array (
                'id' => 4417,
                'name' => '阳江市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            211 => 
            array (
                'id' => 4418,
                'name' => '清远市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            212 => 
            array (
                'id' => 4419,
                'name' => '东莞市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            213 => 
            array (
                'id' => 4420,
                'name' => '中山市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            214 => 
            array (
                'id' => 4451,
                'name' => '潮州市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            215 => 
            array (
                'id' => 4452,
                'name' => '揭阳市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            216 => 
            array (
                'id' => 4453,
                'name' => '云浮市',
                'province_id' => 44,
                'region_id' => 4,
            ),
            217 => 
            array (
                'id' => 4501,
                'name' => '南宁市',
                'province_id' => 45,
                'region_id' => 4,
            ),
            218 => 
            array (
                'id' => 4502,
                'name' => '柳州市',
                'province_id' => 45,
                'region_id' => 4,
            ),
            219 => 
            array (
                'id' => 4503,
                'name' => '桂林市',
                'province_id' => 45,
                'region_id' => 4,
            ),
            220 => 
            array (
                'id' => 4504,
                'name' => '梧州市',
                'province_id' => 45,
                'region_id' => 4,
            ),
            221 => 
            array (
                'id' => 4505,
                'name' => '北海市',
                'province_id' => 45,
                'region_id' => 4,
            ),
            222 => 
            array (
                'id' => 4506,
                'name' => '防城港市',
                'province_id' => 45,
                'region_id' => 4,
            ),
            223 => 
            array (
                'id' => 4507,
                'name' => '钦州市',
                'province_id' => 45,
                'region_id' => 4,
            ),
            224 => 
            array (
                'id' => 4508,
                'name' => '贵港市',
                'province_id' => 45,
                'region_id' => 4,
            ),
            225 => 
            array (
                'id' => 4509,
                'name' => '玉林市',
                'province_id' => 45,
                'region_id' => 4,
            ),
            226 => 
            array (
                'id' => 4510,
                'name' => '百色市',
                'province_id' => 45,
                'region_id' => 4,
            ),
            227 => 
            array (
                'id' => 4511,
                'name' => '贺州市',
                'province_id' => 45,
                'region_id' => 4,
            ),
            228 => 
            array (
                'id' => 4512,
                'name' => '河池市',
                'province_id' => 45,
                'region_id' => 4,
            ),
            229 => 
            array (
                'id' => 4513,
                'name' => '来宾市',
                'province_id' => 45,
                'region_id' => 4,
            ),
            230 => 
            array (
                'id' => 4514,
                'name' => '崇左市',
                'province_id' => 45,
                'region_id' => 4,
            ),
            231 => 
            array (
                'id' => 4601,
                'name' => '海口市',
                'province_id' => 46,
                'region_id' => 4,
            ),
            232 => 
            array (
                'id' => 4602,
                'name' => '三亚市',
                'province_id' => 46,
                'region_id' => 4,
            ),
            233 => 
            array (
                'id' => 4603,
                'name' => '三沙市',
                'province_id' => 46,
                'region_id' => 4,
            ),
            234 => 
            array (
                'id' => 4604,
                'name' => '儋州市',
                'province_id' => 46,
                'region_id' => 4,
            ),
            235 => 
            array (
                'id' => 4690,
                'name' => '省直辖县级行政区划',
                'province_id' => 46,
                'region_id' => 4,
            ),
            236 => 
            array (
                'id' => 5001,
                'name' => '重庆市',
                'province_id' => 50,
                'region_id' => 5,
            ),
            237 => 
            array (
                'id' => 5002,
                'name' => '县',
                'province_id' => 50,
                'region_id' => 5,
            ),
            238 => 
            array (
                'id' => 5101,
                'name' => '成都市',
                'province_id' => 51,
                'region_id' => 5,
            ),
            239 => 
            array (
                'id' => 5103,
                'name' => '自贡市',
                'province_id' => 51,
                'region_id' => 5,
            ),
            240 => 
            array (
                'id' => 5104,
                'name' => '攀枝花市',
                'province_id' => 51,
                'region_id' => 5,
            ),
            241 => 
            array (
                'id' => 5105,
                'name' => '泸州市',
                'province_id' => 51,
                'region_id' => 5,
            ),
            242 => 
            array (
                'id' => 5106,
                'name' => '德阳市',
                'province_id' => 51,
                'region_id' => 5,
            ),
            243 => 
            array (
                'id' => 5107,
                'name' => '绵阳市',
                'province_id' => 51,
                'region_id' => 5,
            ),
            244 => 
            array (
                'id' => 5108,
                'name' => '广元市',
                'province_id' => 51,
                'region_id' => 5,
            ),
            245 => 
            array (
                'id' => 5109,
                'name' => '遂宁市',
                'province_id' => 51,
                'region_id' => 5,
            ),
            246 => 
            array (
                'id' => 5110,
                'name' => '内江市',
                'province_id' => 51,
                'region_id' => 5,
            ),
            247 => 
            array (
                'id' => 5111,
                'name' => '乐山市',
                'province_id' => 51,
                'region_id' => 5,
            ),
            248 => 
            array (
                'id' => 5113,
                'name' => '南充市',
                'province_id' => 51,
                'region_id' => 5,
            ),
            249 => 
            array (
                'id' => 5114,
                'name' => '眉山市',
                'province_id' => 51,
                'region_id' => 5,
            ),
            250 => 
            array (
                'id' => 5115,
                'name' => '宜宾市',
                'province_id' => 51,
                'region_id' => 5,
            ),
            251 => 
            array (
                'id' => 5116,
                'name' => '广安市',
                'province_id' => 51,
                'region_id' => 5,
            ),
            252 => 
            array (
                'id' => 5117,
                'name' => '达州市',
                'province_id' => 51,
                'region_id' => 5,
            ),
            253 => 
            array (
                'id' => 5118,
                'name' => '雅安市',
                'province_id' => 51,
                'region_id' => 5,
            ),
            254 => 
            array (
                'id' => 5119,
                'name' => '巴中市',
                'province_id' => 51,
                'region_id' => 5,
            ),
            255 => 
            array (
                'id' => 5120,
                'name' => '资阳市',
                'province_id' => 51,
                'region_id' => 5,
            ),
            256 => 
            array (
                'id' => 5132,
                'name' => '阿坝藏族羌族自治州',
                'province_id' => 51,
                'region_id' => 5,
            ),
            257 => 
            array (
                'id' => 5133,
                'name' => '甘孜藏族自治州',
                'province_id' => 51,
                'region_id' => 5,
            ),
            258 => 
            array (
                'id' => 5134,
                'name' => '凉山彝族自治州',
                'province_id' => 51,
                'region_id' => 5,
            ),
            259 => 
            array (
                'id' => 5201,
                'name' => '贵阳市',
                'province_id' => 52,
                'region_id' => 5,
            ),
            260 => 
            array (
                'id' => 5202,
                'name' => '六盘水市',
                'province_id' => 52,
                'region_id' => 5,
            ),
            261 => 
            array (
                'id' => 5203,
                'name' => '遵义市',
                'province_id' => 52,
                'region_id' => 5,
            ),
            262 => 
            array (
                'id' => 5204,
                'name' => '安顺市',
                'province_id' => 52,
                'region_id' => 5,
            ),
            263 => 
            array (
                'id' => 5205,
                'name' => '毕节市',
                'province_id' => 52,
                'region_id' => 5,
            ),
            264 => 
            array (
                'id' => 5206,
                'name' => '铜仁市',
                'province_id' => 52,
                'region_id' => 5,
            ),
            265 => 
            array (
                'id' => 5223,
                'name' => '黔西南布依族苗族自治州',
                'province_id' => 52,
                'region_id' => 5,
            ),
            266 => 
            array (
                'id' => 5226,
                'name' => '黔东南苗族侗族自治州',
                'province_id' => 52,
                'region_id' => 5,
            ),
            267 => 
            array (
                'id' => 5227,
                'name' => '黔南布依族苗族自治州',
                'province_id' => 52,
                'region_id' => 5,
            ),
            268 => 
            array (
                'id' => 5301,
                'name' => '昆明市',
                'province_id' => 53,
                'region_id' => 5,
            ),
            269 => 
            array (
                'id' => 5303,
                'name' => '曲靖市',
                'province_id' => 53,
                'region_id' => 5,
            ),
            270 => 
            array (
                'id' => 5304,
                'name' => '玉溪市',
                'province_id' => 53,
                'region_id' => 5,
            ),
            271 => 
            array (
                'id' => 5305,
                'name' => '保山市',
                'province_id' => 53,
                'region_id' => 5,
            ),
            272 => 
            array (
                'id' => 5306,
                'name' => '昭通市',
                'province_id' => 53,
                'region_id' => 5,
            ),
            273 => 
            array (
                'id' => 5307,
                'name' => '丽江市',
                'province_id' => 53,
                'region_id' => 5,
            ),
            274 => 
            array (
                'id' => 5308,
                'name' => '普洱市',
                'province_id' => 53,
                'region_id' => 5,
            ),
            275 => 
            array (
                'id' => 5309,
                'name' => '临沧市',
                'province_id' => 53,
                'region_id' => 5,
            ),
            276 => 
            array (
                'id' => 5323,
                'name' => '楚雄彝族自治州',
                'province_id' => 53,
                'region_id' => 5,
            ),
            277 => 
            array (
                'id' => 5325,
                'name' => '红河哈尼族彝族自治州',
                'province_id' => 53,
                'region_id' => 5,
            ),
            278 => 
            array (
                'id' => 5326,
                'name' => '文山壮族苗族自治州',
                'province_id' => 53,
                'region_id' => 5,
            ),
            279 => 
            array (
                'id' => 5328,
                'name' => '西双版纳傣族自治州',
                'province_id' => 53,
                'region_id' => 5,
            ),
            280 => 
            array (
                'id' => 5329,
                'name' => '大理白族自治州',
                'province_id' => 53,
                'region_id' => 5,
            ),
            281 => 
            array (
                'id' => 5331,
                'name' => '德宏傣族景颇族自治州',
                'province_id' => 53,
                'region_id' => 5,
            ),
            282 => 
            array (
                'id' => 5333,
                'name' => '怒江傈僳族自治州',
                'province_id' => 53,
                'region_id' => 5,
            ),
            283 => 
            array (
                'id' => 5334,
                'name' => '迪庆藏族自治州',
                'province_id' => 53,
                'region_id' => 5,
            ),
            284 => 
            array (
                'id' => 5401,
                'name' => '拉萨市',
                'province_id' => 54,
                'region_id' => 5,
            ),
            285 => 
            array (
                'id' => 5402,
                'name' => '日喀则市',
                'province_id' => 54,
                'region_id' => 5,
            ),
            286 => 
            array (
                'id' => 5403,
                'name' => '昌都市',
                'province_id' => 54,
                'region_id' => 5,
            ),
            287 => 
            array (
                'id' => 5404,
                'name' => '林芝市',
                'province_id' => 54,
                'region_id' => 5,
            ),
            288 => 
            array (
                'id' => 5405,
                'name' => '山南市',
                'province_id' => 54,
                'region_id' => 5,
            ),
            289 => 
            array (
                'id' => 5424,
                'name' => '那曲地区',
                'province_id' => 54,
                'region_id' => 5,
            ),
            290 => 
            array (
                'id' => 5425,
                'name' => '阿里地区',
                'province_id' => 54,
                'region_id' => 5,
            ),
            291 => 
            array (
                'id' => 6101,
                'name' => '西安市',
                'province_id' => 61,
                'region_id' => 6,
            ),
            292 => 
            array (
                'id' => 6102,
                'name' => '铜川市',
                'province_id' => 61,
                'region_id' => 6,
            ),
            293 => 
            array (
                'id' => 6103,
                'name' => '宝鸡市',
                'province_id' => 61,
                'region_id' => 6,
            ),
            294 => 
            array (
                'id' => 6104,
                'name' => '咸阳市',
                'province_id' => 61,
                'region_id' => 6,
            ),
            295 => 
            array (
                'id' => 6105,
                'name' => '渭南市',
                'province_id' => 61,
                'region_id' => 6,
            ),
            296 => 
            array (
                'id' => 6106,
                'name' => '延安市',
                'province_id' => 61,
                'region_id' => 6,
            ),
            297 => 
            array (
                'id' => 6107,
                'name' => '汉中市',
                'province_id' => 61,
                'region_id' => 6,
            ),
            298 => 
            array (
                'id' => 6108,
                'name' => '榆林市',
                'province_id' => 61,
                'region_id' => 6,
            ),
            299 => 
            array (
                'id' => 6109,
                'name' => '安康市',
                'province_id' => 61,
                'region_id' => 6,
            ),
            300 => 
            array (
                'id' => 6110,
                'name' => '商洛市',
                'province_id' => 61,
                'region_id' => 6,
            ),
            301 => 
            array (
                'id' => 6201,
                'name' => '兰州市',
                'province_id' => 62,
                'region_id' => 6,
            ),
            302 => 
            array (
                'id' => 6202,
                'name' => '嘉峪关市',
                'province_id' => 62,
                'region_id' => 6,
            ),
            303 => 
            array (
                'id' => 6203,
                'name' => '金昌市',
                'province_id' => 62,
                'region_id' => 6,
            ),
            304 => 
            array (
                'id' => 6204,
                'name' => '白银市',
                'province_id' => 62,
                'region_id' => 6,
            ),
            305 => 
            array (
                'id' => 6205,
                'name' => '天水市',
                'province_id' => 62,
                'region_id' => 6,
            ),
            306 => 
            array (
                'id' => 6206,
                'name' => '武威市',
                'province_id' => 62,
                'region_id' => 6,
            ),
            307 => 
            array (
                'id' => 6207,
                'name' => '张掖市',
                'province_id' => 62,
                'region_id' => 6,
            ),
            308 => 
            array (
                'id' => 6208,
                'name' => '平凉市',
                'province_id' => 62,
                'region_id' => 6,
            ),
            309 => 
            array (
                'id' => 6209,
                'name' => '酒泉市',
                'province_id' => 62,
                'region_id' => 6,
            ),
            310 => 
            array (
                'id' => 6210,
                'name' => '庆阳市',
                'province_id' => 62,
                'region_id' => 6,
            ),
            311 => 
            array (
                'id' => 6211,
                'name' => '定西市',
                'province_id' => 62,
                'region_id' => 6,
            ),
            312 => 
            array (
                'id' => 6212,
                'name' => '陇南市',
                'province_id' => 62,
                'region_id' => 6,
            ),
            313 => 
            array (
                'id' => 6229,
                'name' => '临夏回族自治州',
                'province_id' => 62,
                'region_id' => 6,
            ),
            314 => 
            array (
                'id' => 6230,
                'name' => '甘南藏族自治州',
                'province_id' => 62,
                'region_id' => 6,
            ),
            315 => 
            array (
                'id' => 6301,
                'name' => '西宁市',
                'province_id' => 63,
                'region_id' => 6,
            ),
            316 => 
            array (
                'id' => 6302,
                'name' => '海东市',
                'province_id' => 63,
                'region_id' => 6,
            ),
            317 => 
            array (
                'id' => 6322,
                'name' => '海北藏族自治州',
                'province_id' => 63,
                'region_id' => 6,
            ),
            318 => 
            array (
                'id' => 6323,
                'name' => '黄南藏族自治州',
                'province_id' => 63,
                'region_id' => 6,
            ),
            319 => 
            array (
                'id' => 6325,
                'name' => '海南藏族自治州',
                'province_id' => 63,
                'region_id' => 6,
            ),
            320 => 
            array (
                'id' => 6326,
                'name' => '果洛藏族自治州',
                'province_id' => 63,
                'region_id' => 6,
            ),
            321 => 
            array (
                'id' => 6327,
                'name' => '玉树藏族自治州',
                'province_id' => 63,
                'region_id' => 6,
            ),
            322 => 
            array (
                'id' => 6328,
                'name' => '海西蒙古族藏族自治州',
                'province_id' => 63,
                'region_id' => 6,
            ),
            323 => 
            array (
                'id' => 6401,
                'name' => '银川市',
                'province_id' => 64,
                'region_id' => 6,
            ),
            324 => 
            array (
                'id' => 6402,
                'name' => '石嘴山市',
                'province_id' => 64,
                'region_id' => 6,
            ),
            325 => 
            array (
                'id' => 6403,
                'name' => '吴忠市',
                'province_id' => 64,
                'region_id' => 6,
            ),
            326 => 
            array (
                'id' => 6404,
                'name' => '固原市',
                'province_id' => 64,
                'region_id' => 6,
            ),
            327 => 
            array (
                'id' => 6405,
                'name' => '中卫市',
                'province_id' => 64,
                'region_id' => 6,
            ),
            328 => 
            array (
                'id' => 6501,
                'name' => '乌鲁木齐市',
                'province_id' => 65,
                'region_id' => 6,
            ),
            329 => 
            array (
                'id' => 6502,
                'name' => '克拉玛依市',
                'province_id' => 65,
                'region_id' => 6,
            ),
            330 => 
            array (
                'id' => 6504,
                'name' => '吐鲁番市',
                'province_id' => 65,
                'region_id' => 6,
            ),
            331 => 
            array (
                'id' => 6505,
                'name' => '哈密市',
                'province_id' => 65,
                'region_id' => 6,
            ),
            332 => 
            array (
                'id' => 6523,
                'name' => '昌吉回族自治州',
                'province_id' => 65,
                'region_id' => 6,
            ),
            333 => 
            array (
                'id' => 6527,
                'name' => '博尔塔拉蒙古自治州',
                'province_id' => 65,
                'region_id' => 6,
            ),
            334 => 
            array (
                'id' => 6528,
                'name' => '巴音郭楞蒙古自治州',
                'province_id' => 65,
                'region_id' => 6,
            ),
            335 => 
            array (
                'id' => 6529,
                'name' => '阿克苏地区',
                'province_id' => 65,
                'region_id' => 6,
            ),
            336 => 
            array (
                'id' => 6530,
                'name' => '克孜勒苏柯尔克孜自治州',
                'province_id' => 65,
                'region_id' => 6,
            ),
            337 => 
            array (
                'id' => 6531,
                'name' => '喀什地区',
                'province_id' => 65,
                'region_id' => 6,
            ),
            338 => 
            array (
                'id' => 6532,
                'name' => '和田地区',
                'province_id' => 65,
                'region_id' => 6,
            ),
            339 => 
            array (
                'id' => 6540,
                'name' => '伊犁哈萨克自治州',
                'province_id' => 65,
                'region_id' => 6,
            ),
            340 => 
            array (
                'id' => 6542,
                'name' => '塔城地区',
                'province_id' => 65,
                'region_id' => 6,
            ),
            341 => 
            array (
                'id' => 6543,
                'name' => '阿勒泰地区',
                'province_id' => 65,
                'region_id' => 6,
            ),
            342 => 
            array (
                'id' => 6590,
                'name' => '自治区直辖县级行政区划',
                'province_id' => 65,
                'region_id' => 6,
            ),
        ));
        
        
    }
}