<?php

use App\Client;
use App\Department;
use App\JobLevel;
use App\Permission;
use App\Role;
use App\StatusInCanada;
use App\Title;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public static $role_array = [];
    public static $title_array = [];
    public static $job_lv_array = [];
    public static $department_array = [];
    public static $status_array = [];
    public static $permission_array = [];

    public static $employee_number = 10;
    public static $client_number = 200;

    public function run()
    {
        //ini_set('memory_limit','1024M');

        $this->prepare_db();

        $user = new User;
        $user->email = "admin@admin.com";
        $user->email_verified_at = now();
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
        $user->remember_token = Str::random(10);
        $user->save();

        $employee = factory(App\Employee::class)->create();
        $employee->name = "Admin";

        $role = UsersTableSeeder::$role_array[0];
        $role->employees()->save($employee);

        $title = UsersTableSeeder::$title_array[rand(3, 22)];
        $title->employees()->save($employee);

        $job_lv = UsersTableSeeder::$job_lv_array[array_rand(UsersTableSeeder::$job_lv_array)];
        $job_lv->employees()->save($employee);

        $department = UsersTableSeeder::$department_array[array_rand(UsersTableSeeder::$department_array)];
        $department->employees()->save($employee);

        $user->employee()->save($employee);

        factory(App\User::class, UsersTableSeeder::$employee_number)->create()->each(function ($user) {
            $employee = factory(App\Employee::class)->create();

            $role = UsersTableSeeder::$role_array[1];
            $role->employees()->save($employee);

            $title = UsersTableSeeder::$title_array[array_rand(UsersTableSeeder::$title_array)];
            $title->employees()->save($employee);

            $job_lv = UsersTableSeeder::$job_lv_array[array_rand(UsersTableSeeder::$job_lv_array)];
            $job_lv->employees()->save($employee);

            $department = UsersTableSeeder::$department_array[array_rand(UsersTableSeeder::$department_array)];
            $department->employees()->save($employee);

            $user->employee()->save($employee);
        });

        factory(Client::class, UsersTableSeeder::$client_number)->create();

    }

    public function prepare_db()
    {
        $this->create_department();
        $this->create_permission();
        $this->create_role();
        $this->create_title();
        $this->create_status();
        $this->create_joblevel();
    }

    public function create_department()
    {
        /*
        经理部
        行政部
        市场部
        业务部
        文案部
         */
        $department = new Department;
        $department->name = "经理部";
        $department->save();
        UsersTableSeeder::$department_array[] = $department;

        $department = new Department;
        $department->name = "行政部";
        $department->save();
        UsersTableSeeder::$department_array[] = $department;

        $department = new Department;
        $department->name = "市场部";
        $department->save();
        UsersTableSeeder::$department_array[] = $department;

        $department = new Department;
        $department->name = "业务部";
        $department->save();
        UsersTableSeeder::$department_array[] = $department;

        $department = new Department;
        $department->name = "文案部";
        $department->save();
        UsersTableSeeder::$department_array[] = $department;
    }
    public function create_permission()
    {
        $permissions = ["用户权限管理", //1
            "信息管理", //2
            "客户列表", //3
            "安全界面", //4
            "添加新客户", //5
            "删除客户", //6
            "修改客户信息", //7
            "地址修改", //8
        ];

        foreach ($permissions as $permission) {
            $permission1 = new Permission;
            $permission1->name = $permission;
            $permission1->save();
            UsersTableSeeder::$permission_array[] = $permission1;
        }

    }
    public function create_role()
    {
        $role = new Role;
        $role->name = "管理员";
        $role->save();

        foreach (UsersTableSeeder::$permission_array as $permission) {
            $role->addPermission($permission->id);
        }

        UsersTableSeeder::$role_array[] = $role;


        $role = new Role;
        $role->name = "实习生";
        $role->save();
        //$role->addPermission(3);
        $role->addPermission(5);
        $role->addPermission(7);

        UsersTableSeeder::$role_array[] = $role;
    }

    public function create_title()
    {
        $title = new Title;
        $title->name = "总经理 ";
        $title->parent_title_id = "0";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "Markham/经理 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "North ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "业务部/部长 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划一队/主任 ";
        $title->parent_title_id = "4";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划二队/.主任 ";
        $title->parent_title_id = "4";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划三队/主任 ";
        $title->parent_title_id = "4";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划一队/一组/主管 ";
        $title->parent_title_id = "5";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划一队/二组/主管 ";
        $title->parent_title_id = "5";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划一队/三组/主管 ";
        $title->parent_title_id = "5";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划二队/一组/主管 ";
        $title->parent_title_id = "6";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划二队/二组/主管 ";
        $title->parent_title_id = "6";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划二队/三组/主管 ";
        $title->parent_title_id = "6";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划三队/一组/主管 ";
        $title->parent_title_id = "7";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划三队/二组/主管 ";
        $title->parent_title_id = "7";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划三队/三组/主管 ";
        $title->parent_title_id = "7";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划一队/一组/组员 ";
        $title->parent_title_id = "8";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划一队/二组/组员 ";
        $title->parent_title_id = "9";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划一队/三组/组员 ";
        $title->parent_title_id = "10";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划二队/一组/组员 ";
        $title->parent_title_id = "11";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划二队/二组/组员 ";
        $title->parent_title_id = "12";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划二队/三组/组员 ";
        $title->parent_title_id = "13";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划三队/一组/组员 ";
        $title->parent_title_id = "14";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划三队/二组/组员 ";
        $title->parent_title_id = "15";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "规划三队/三组/组员 ";
        $title->parent_title_id = "16";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "留学顾问/主任 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "培训讲师/主管 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "留学顾问/主管 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "实习生 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "留学顾问 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "移民/讲师 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "留学/讲师 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "行政部/部长 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "外联/主任 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "人力资源/主任 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "外联/助理 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "招聘/主管 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "前台 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "市场部/部长 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "新媒体运营/主任 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "IT/主任 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "设计/主任 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "视频剪辑师 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "文字编辑师 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "网络工程师 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "平面设计师 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "文案部/部长 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "签证中心/主任 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "申请中心/主任 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;
        $title = new Title;
        $title->name = "文案 ";
        $title->parent_title_id = "1";
        $title->save();
        UsersTableSeeder::$title_array[] = $title;

    }
    public function create_status()
    {
        $statuses = ['学习签证', '工作签证', '旅游签证', "居民", "公民", "难民", "第一次签证"];

        foreach ($statuses as $status) {
            $statusInCanada = new StatusInCanada;
            $statusInCanada->name = $status;
            $statusInCanada->save();
            UsersTableSeeder::$status_array[] = $statusInCanada;
        }
    }

    public function create_joblevel()
    {

        $job_lv = new JobLevel;
        $job_lv->name = "见习留学顾问";
        $job_lv->save();
        UsersTableSeeder::$job_lv_array[] = $job_lv;

        $job_lv = new JobLevel;
        $job_lv->name = "留学顾问";
        $job_lv->save();
        UsersTableSeeder::$job_lv_array[] = $job_lv;

        $job_lv = new JobLevel;
        $job_lv->name = "见习规划师";
        $job_lv->save();
        UsersTableSeeder::$job_lv_array[] = $job_lv;

        $job_lv = new JobLevel;
        $job_lv->name = "留学规划师";
        $job_lv->save();
        UsersTableSeeder::$job_lv_array[] = $job_lv;

        $job_lv = new JobLevel;
        $job_lv->name = "移民规划师";
        $job_lv->save();
        UsersTableSeeder::$job_lv_array[] = $job_lv;

        $job_lv = new JobLevel;
        $job_lv->name = "资深规划师";
        $job_lv->save();
        UsersTableSeeder::$job_lv_array[] = $job_lv;

        $job_lv = new JobLevel;
        $job_lv->name = "暂无";
        $job_lv->save();
        UsersTableSeeder::$job_lv_array[] = $job_lv;
    }

}
