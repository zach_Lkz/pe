<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use App\Client;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});


$factory->define(Client::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'usedName' => '',
        'phone' => $faker->phoneNumber,

        'contact_email' => $faker->unique()->safeEmail,

        'citizenship' => "中国",

        'birth_region' => $faker->streetAddress,
        'birth_city' => $faker->city,
        'birth_province' => $faker->state,

        'original_city' => $faker->city,
        'original_province' => $faker->state,
        'original_region' => $faker->country,

        'address' => $faker->streetAddress,
        'city' => $faker->city,
        'province' => $faker->state,
        'postal_code' => $faker->postcode,

        'contact_email' => $faker->unique()->safeEmail,
        'gender' => 'Male',


        'passport' => "CN" . rand(1111111, 9999999),
        'chinese_id_number' => rand(111111111, 999999999),
        'passport_expire' => '2020-'. mt_rand(10,12) .'-' . mt_rand(1,30),
        'visa_expire' => '2020-05-' . mt_rand(1,30),
        'uci_number' => rand(111111111, 999999999),
        'landing_location' => 'Toronto, ON',
        'landing_date' => '2015-05-' . mt_rand(1,30),
        'completion_rate' => mt_rand(0,90),

        'user_id' => mt_rand(1,20)
    ];
});


