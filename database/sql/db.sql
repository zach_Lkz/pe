# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.26)
# Database: promise_education
# Generation Time: 2019-10-11 15:36:57 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table action_events
# ------------------------------------------------------------

DROP TABLE IF EXISTS `action_events`;

CREATE TABLE `action_events` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `batch_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actionable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actionable_id` int(10) unsigned NOT NULL,
  `target_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` int(10) unsigned DEFAULT NULL,
  `fields` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'running',
  `exception` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `original` text COLLATE utf8mb4_unicode_ci,
  `changes` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `action_events_actionable_type_actionable_id_index` (`actionable_type`,`actionable_id`),
  KEY `action_events_batch_id_model_type_model_id_index` (`batch_id`,`model_type`,`model_id`),
  KEY `action_events_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `action_events` WRITE;
/*!40000 ALTER TABLE `action_events` DISABLE KEYS */;

INSERT INTO `action_events` (`id`, `batch_id`, `user_id`, `name`, `actionable_type`, `actionable_id`, `target_type`, `target_id`, `model_type`, `model_id`, `fields`, `status`, `exception`, `created_at`, `updated_at`, `original`, `changes`)
VALUES
	(1,'8ec90921-98d9-449e-a1dc-2a3cd42f4985',1,'Update','App\\City',6590,'App\\City',6590,'App\\City',6590,'','finished','','2019-10-01 14:42:07','2019-10-01 14:42:07','[]','[]'),
	(2,'8ec90956-8437-4628-84de-a8d1b5c7bca7',1,'Update','App\\Client',167,'App\\Client',167,'App\\Client',167,'','finished','','2019-10-01 14:42:42','2019-10-01 14:42:42','{\"usedName\":\"\",\"citizenship\":\"\\u4e2d\\u56fd\"}','{\"usedName\":null,\"citizenship\":\"AW\"}'),
	(7,'8ecbadd9-ce15-46bb-be7d-a25a13fd4248',1,'Update','App\\User',1,'App\\User',1,'App\\User',1,'','finished','','2019-10-02 22:14:22','2019-10-02 22:14:22','{\"job_level_id\":null,\"address\":\"\",\"city\":\"\",\"province\":\"\",\"postal_code\":\"\",\"gender\":\"Other\",\"contact_email\":\"\",\"department_id\":null,\"title_id\":null,\"identity\":null,\"name\":\"\",\"phone\":\"\",\"join_date\":\"1900-01-01\"}','{\"job_level_id\":5,\"address\":null,\"city\":null,\"province\":null,\"postal_code\":null,\"gender\":\"Male\",\"contact_email\":\"admin@admin.com\",\"department_id\":4,\"title_id\":4,\"identity\":\"VR\",\"name\":\"admin\",\"phone\":\"+1\",\"join_date\":\"2000-01-01 00:00:00\"}'),
	(8,'8ecbaef4-5f12-48ec-931b-e1c76f0881fb',1,'Update','App\\User',2,'App\\User',2,'App\\User',2,'','finished','','2019-10-02 22:17:27','2019-10-02 22:17:27','{\"email\":\"kaley97@example.net\",\"job_level_id\":null,\"address\":\"\",\"city\":\"\",\"province\":\"\",\"postal_code\":\"\",\"gender\":\"Other\",\"contact_email\":\"\",\"department_id\":null,\"title_id\":null,\"identity\":null,\"name\":\"\",\"phone\":\"\"}','{\"email\":\"test@test.com\",\"job_level_id\":7,\"address\":\"110 steels\",\"city\":\"Markham\",\"province\":\"ON\",\"postal_code\":\"L4D 9D8\",\"gender\":\"Female\",\"contact_email\":\"test@test.com\",\"department_id\":4,\"title_id\":3,\"identity\":\"SP\",\"name\":\"USER\",\"phone\":\"+1(905)-111-0111\",\"Password\":\"$2y$10$p4Z\\/\\/itvZkPd\\/mhHHRmuVuoQTo66uz.GcQBn3wRsPRLxBYPp98XHS\"}'),
	(9,'8ecbb8ef-1b95-4a61-b5eb-c55a3a80a896',2,'Update','App\\Role',2,'App\\Role',2,'App\\Role',2,'','finished','','2019-10-02 22:45:21','2019-10-02 22:45:21','[]','[]'),
	(10,'8ecd4134-1a49-4f78-9efa-b90eec722f83',1,'Delete','App\\User',3,'App\\User',3,'App\\User',3,'','finished','','2019-10-03 13:02:13','2019-10-03 13:02:13',NULL,NULL),
	(11,'8ecd4134-2dda-4627-b088-e6c2e2eecb3b',1,'Delete','App\\User',4,'App\\User',4,'App\\User',4,'','finished','','2019-10-03 13:02:13','2019-10-03 13:02:13',NULL,NULL),
	(12,'8ecd4134-3b6b-45e5-b94e-75d51e45ae85',1,'Delete','App\\User',5,'App\\User',5,'App\\User',5,'','finished','','2019-10-03 13:02:13','2019-10-03 13:02:13',NULL,NULL),
	(13,'8ecd4134-4670-423c-b1b4-0a93d63c374e',1,'Delete','App\\User',6,'App\\User',6,'App\\User',6,'','finished','','2019-10-03 13:02:13','2019-10-03 13:02:13',NULL,NULL),
	(14,'8ecd4134-5108-4f8f-b291-cc5eed730431',1,'Delete','App\\User',7,'App\\User',7,'App\\User',7,'','finished','','2019-10-03 13:02:13','2019-10-03 13:02:13',NULL,NULL),
	(15,'8ecd4134-5bb1-4f39-816b-c4c0e3b22bfc',1,'Delete','App\\User',8,'App\\User',8,'App\\User',8,'','finished','','2019-10-03 13:02:13','2019-10-03 13:02:13',NULL,NULL),
	(16,'8ecd4134-6669-445d-af43-e74596267340',1,'Delete','App\\User',9,'App\\User',9,'App\\User',9,'','finished','','2019-10-03 13:02:13','2019-10-03 13:02:13',NULL,NULL),
	(17,'8ecd4134-7108-4ec2-bb68-d0f32902e4a8',1,'Delete','App\\User',10,'App\\User',10,'App\\User',10,'','finished','','2019-10-03 13:02:13','2019-10-03 13:02:13',NULL,NULL),
	(18,'8ecd4134-7bb1-41fd-9f30-f962eb932b2e',1,'Delete','App\\User',11,'App\\User',11,'App\\User',11,'','finished','','2019-10-03 13:02:13','2019-10-03 13:02:13',NULL,NULL),
	(19,'8ecd42ea-4bf4-4ab6-b2a4-6028f7833fc3',1,'Update','App\\User',2,'App\\User',2,'App\\User',2,'','finished','','2019-10-03 13:07:00','2019-10-03 13:07:00','{\"title_id\":3,\"phone\":\"+1(905)-111-0111\"}','{\"title_id\":44,\"phone\":\"+1+1(905)-111-01\"}'),
	(20,'8ecd5a7a-894e-4fbb-a2de-9f6410ffdea5',1,'Update','App\\StatusInCanada',1,'App\\StatusInCanada',1,'App\\StatusInCanada',1,'','finished','','2019-10-03 14:12:54','2019-10-03 14:12:54','{\"name\":\"\\u5b66\\u4e60\\u7b7e\\u8bc1\"}','{\"name\":\"SP\"}'),
	(21,'8ecd5aad-0109-471e-8394-09306813f156',1,'Update','App\\StatusInCanada',2,'App\\StatusInCanada',2,'App\\StatusInCanada',2,'','finished','','2019-10-03 14:13:27','2019-10-03 14:13:27','{\"name\":\"\\u5de5\\u4f5c\\u7b7e\\u8bc1\"}','{\"name\":\"WP\"}'),
	(22,'8ecd5ad2-71d1-4fa4-9bb9-9667473eec86',1,'Update','App\\StatusInCanada',3,'App\\StatusInCanada',3,'App\\StatusInCanada',3,'','finished','','2019-10-03 14:13:51','2019-10-03 14:13:51','{\"name\":\"\\u65c5\\u6e38\\u7b7e\\u8bc1\"}','{\"name\":\"VV\"}'),
	(23,'8ecd5b07-73db-44b0-902a-d46508eb5f80',1,'Create','App\\StatusInCanada',8,'App\\StatusInCanada',8,'App\\StatusInCanada',8,'','finished','','2019-10-03 14:14:26','2019-10-03 14:14:26',NULL,'{\"name\":\"10\",\"updated_at\":\"2019-10-03 18:14:26\",\"created_at\":\"2019-10-03 18:14:26\",\"id\":8}'),
	(24,'8ecd5b0d-d8f6-4dfc-a4dd-d98a1f18d4f3',1,'Create','App\\StatusInCanada',9,'App\\StatusInCanada',9,'App\\StatusInCanada',9,'','finished','','2019-10-03 14:14:30','2019-10-03 14:14:30',NULL,'{\"name\":\"15\",\"updated_at\":\"2019-10-03 18:14:30\",\"created_at\":\"2019-10-03 18:14:30\",\"id\":9}'),
	(25,'8ecd5b13-10cb-41e4-9cfc-87e7f21de2d2',1,'Create','App\\StatusInCanada',10,'App\\StatusInCanada',10,'App\\StatusInCanada',10,'','finished','','2019-10-03 14:14:34','2019-10-03 14:14:34',NULL,'{\"name\":\"20\",\"updated_at\":\"2019-10-03 18:14:34\",\"created_at\":\"2019-10-03 18:14:34\",\"id\":10}'),
	(26,'8ecd5b26-c657-4479-9e51-9a988cd616fa',1,'Delete','App\\StatusInCanada',9,'App\\StatusInCanada',9,'App\\StatusInCanada',9,'','finished','','2019-10-03 14:14:47','2019-10-03 14:14:47',NULL,NULL),
	(27,'8ecd5b3c-3872-45a7-807c-37533708f6ec',1,'Create','App\\StatusInCanada',11,'App\\StatusInCanada',11,'App\\StatusInCanada',11,'','finished','','2019-10-03 14:15:01','2019-10-03 14:15:01',NULL,'{\"name\":\"15\",\"updated_at\":\"2019-10-03 18:15:01\",\"created_at\":\"2019-10-03 18:15:01\",\"id\":11}'),
	(28,'8ecd5dd5-6376-4a30-8ae2-ed10c4a8bbc8',1,'Create','App\\User',12,'App\\User',12,'App\\User',12,'','finished','','2019-10-03 14:22:17','2019-10-03 14:22:17',NULL,'{\"name\":\"\\u9ad8\\u59ff\",\"gender\":\"Female\",\"birthdate\":\"1990-11-26 00:00:00\",\"phone\":\"+1(416)-721-9686\",\"contact_email\":\"cynthiapink1013@gmail.com\",\"identity\":\"PR\",\"join_date\":\"2018-10-01 00:00:00\",\"department_id\":4,\"title_id\":8,\"job_level_id\":6,\"salary\":\"0\",\"passport\":null,\"sin_number\":\"592056527\",\"email\":\"cynthiapink1013@gmail.com\",\"Password\":\"$2y$10$UTKv3P6flaYgAcwaviQdfunj6kz29FwQUYue\\/7gnUSXfhGmQhFNU.\",\"role_id\":2,\"address\":null,\"city\":null,\"province\":null,\"postal_code\":null,\"updated_at\":\"2019-10-03 18:22:17\",\"created_at\":\"2019-10-03 18:22:17\",\"id\":12}'),
	(29,'8ecd5df2-7aca-4dc5-abdc-be7ba777b47b',1,'Delete','App\\Client',182,'App\\Client',182,'App\\Client',182,'','finished','','2019-10-03 14:22:36','2019-10-03 14:22:36',NULL,NULL),
	(30,'8ecd5df8-166a-4834-89de-b00177288f0a',1,'Delete','App\\Client',163,'App\\Client',163,'App\\Client',163,'','finished','','2019-10-03 14:22:39','2019-10-03 14:22:39',NULL,NULL),
	(31,'8ecd5dfd-5c46-4e64-8d47-be270c887c78',1,'Delete','App\\Client',162,'App\\Client',162,'App\\Client',162,'','finished','','2019-10-03 14:22:43','2019-10-03 14:22:43',NULL,NULL),
	(32,'8ecd5e01-c957-4699-bbde-0693f72439b1',1,'Delete','App\\Client',152,'App\\Client',152,'App\\Client',152,'','finished','','2019-10-03 14:22:46','2019-10-03 14:22:46',NULL,NULL),
	(33,'8ecd5e05-e495-4cc4-ba76-0afee31676ca',1,'Delete','App\\Client',116,'App\\Client',116,'App\\Client',116,'','finished','','2019-10-03 14:22:48','2019-10-03 14:22:48',NULL,NULL),
	(34,'8ecd5e0a-e5d6-401c-9f6a-9069835616f1',1,'Delete','App\\Client',16,'App\\Client',16,'App\\Client',16,'','finished','','2019-10-03 14:22:52','2019-10-03 14:22:52',NULL,NULL),
	(35,'8ecd5e0e-e22e-4454-9939-bbe2681e42bf',1,'Delete','App\\Client',1,'App\\Client',1,'App\\Client',1,'','finished','','2019-10-03 14:22:54','2019-10-03 14:22:54',NULL,NULL),
	(36,'8ecd5e7b-173d-4723-9404-cf9c493e88e8',1,'Create','App\\User',13,'App\\User',13,'App\\User',13,'','finished','','2019-10-03 14:24:05','2019-10-03 14:24:05',NULL,'{\"name\":\"test\",\"gender\":\"Male\",\"birthdate\":\"1995-12-12 00:00:00\",\"phone\":\"+1(123)-456-7890\",\"contact_email\":\"test@gmail.com\",\"identity\":\"PR\",\"join_date\":\"2019-10-10 00:00:00\",\"department_id\":4,\"title_id\":1,\"job_level_id\":3,\"salary\":\"12\",\"passport\":\"12313\",\"sin_number\":\"12344567\",\"email\":\"test@gmail.com\",\"Password\":\"$2y$10$XjTBSRz5iurngyFwlND3K.jVvcnHCgti85lp\\/0oSWGaO8ciTkguS6\",\"role_id\":2,\"address\":\"12 PARSELL ST\",\"city\":\"RICHMOND HILL\",\"province\":\"ON\",\"postal_code\":\"L4E0C7\",\"updated_at\":\"2019-10-03 18:24:05\",\"created_at\":\"2019-10-03 18:24:05\",\"id\":13}'),
	(37,'8ecd5eb3-f094-4925-b265-7b8a1b7a9c1a',1,'Update','App\\User',12,'App\\User',12,'App\\User',12,'','finished','','2019-10-03 14:24:42','2019-10-03 14:24:42','{\"address\":null,\"city\":null,\"province\":null,\"postal_code\":null,\"phone\":\"+1(416)-721-9686\"}','{\"address\":\"TH8-23 Sheppard Ave\\uff0cEast\",\"city\":\"North York\",\"province\":\"ON\",\"postal_code\":\"M2N 0C8\",\"phone\":\"+1+1(416)-721-96\"}'),
	(38,'8ecd61d3-ca9b-434a-aae3-82367358df6b',1,'Create','App\\User',14,'App\\User',14,'App\\User',14,'','finished','','2019-10-03 14:33:27','2019-10-03 14:33:27',NULL,'{\"name\":\"\\u5b59\\u6d69\\u68e0\",\"gender\":\"Male\",\"birthdate\":\"1995-06-13 00:00:00\",\"phone\":\"+1(514)-566-3722\",\"contact_email\":\"Timsun@lpfirm.ca\",\"identity\":\"CC\",\"join_date\":\"2018-01-01 00:00:00\",\"department_id\":4,\"title_id\":4,\"job_level_id\":6,\"salary\":\"0\",\"passport\":null,\"sin_number\":\"565176708\",\"email\":\"Timsun@lpfirm.ca\",\"Password\":\"$2y$10$pyLiBsMLihVizdnzkcNhSuW5QuXLygpc22WXXC0NRorkyTY2bkgIe\",\"role_id\":2,\"address\":\"413-17 Kenaston Gdns\",\"city\":\"North York\",\"province\":\"ON\",\"postal_code\":\"M2K 0B9\",\"updated_at\":\"2019-10-03 18:33:27\",\"created_at\":\"2019-10-03 18:33:27\",\"id\":14}'),
	(39,'8ecd65ce-f39d-4531-8bf9-eee15c0594b2',1,'Update','App\\User',12,'App\\User',12,'App\\User',12,'','finished','','2019-10-03 14:44:35','2019-10-03 14:44:35','{\"passport\":null,\"phone\":\"+1+1(416)-721-96\"}','{\"passport\":\"EI5543074\",\"phone\":\"+1+1+1(416)-721\"}'),
	(40,'8ecd6b64-f2e1-4144-9ea4-abbb71b51163',1,'Create','App\\User',15,'App\\User',15,'App\\User',15,'','finished','','2019-10-03 15:00:12','2019-10-03 15:00:12',NULL,'{\"name\":\"\\u674e\\u5efa\\u83b9\",\"gender\":\"Female\",\"birthdate\":\"1993-08-09 00:00:00\",\"phone\":\"+1(647)-895-5856\",\"contact_email\":\"mumulee555@gmail.com\",\"identity\":\"WP\",\"join_date\":\"2018-05-01 00:00:00\",\"department_id\":2,\"title_id\":37,\"job_level_id\":7,\"salary\":\"2125\",\"passport\":null,\"sin_number\":\"930518295\",\"email\":\"mumulee555@gmail.com\",\"Password\":\"$2y$10$GOnVCQMt0cQsKRMQ8\\/X3FectLdTE5pWpo28oNREl0ZzCVahkRVsey\",\"role_id\":2,\"address\":\"B612-99 South Town Ctr Blvd\",\"city\":\"Markham\",\"province\":\"ON\",\"postal_code\":\"L6G 0E9\",\"updated_at\":\"2019-10-03 19:00:12\",\"created_at\":\"2019-10-03 19:00:12\",\"id\":15}'),
	(41,'8ecd6d52-daa1-4e24-af3d-9deb6a561547',1,'Create','App\\User',16,'App\\User',16,'App\\User',16,'','finished','','2019-10-03 15:05:35','2019-10-03 15:05:35',NULL,'{\"name\":\"\\u8bb8\\u8bfa\",\"gender\":\"Male\",\"birthdate\":\"1996-08-05 00:00:00\",\"phone\":\"+1(647)-870-2666\",\"contact_email\":\"hank.promise@gmail.com\",\"identity\":\"CC\",\"join_date\":\"2018-05-01 00:00:00\",\"department_id\":1,\"title_id\":2,\"job_level_id\":6,\"salary\":\"4000\",\"passport\":\"EB548575\",\"sin_number\":\"564852369\",\"email\":\"hank.promise@gmail.com\",\"Password\":\"$2y$10$UeAmIt\\/j5yFxvg22LCbOEuSHgP8HeU\\/l.AQ779ZtKfr1kn0LC1T9W\",\"role_id\":2,\"address\":null,\"city\":null,\"province\":null,\"postal_code\":null,\"updated_at\":\"2019-10-03 19:05:35\",\"created_at\":\"2019-10-03 19:05:35\",\"id\":16}'),
	(42,'8ecd6dae-65a7-4381-8d63-2366313a1d58',16,'Create','App\\Client',201,'App\\Client',201,'App\\Client',201,'','finished','','2019-10-03 15:06:35','2019-10-03 15:06:35',NULL,'{\"name\":\"\\u8bb8\\u8bfa\",\"usedName\":null,\"gender\":null,\"birthdate\":null,\"phone\":null,\"contact_email\":null,\"citizenship\":null,\"birth_city\":null,\"birth_province\":null,\"birth_region\":null,\"original_city\":null,\"original_province\":null,\"original_region\":null,\"address\":null,\"city\":null,\"province\":null,\"postal_code\":null,\"chinese_id_number\":null,\"passport\":null,\"passport_expire\":null,\"visa_expire\":null,\"uci_number\":null,\"landing_location\":null,\"landing_date\":null,\"updated_at\":\"2019-10-03 19:06:35\",\"created_at\":\"2019-10-03 19:06:35\",\"id\":201}'),
	(43,'8ecdcc93-229a-4214-a748-3800979f192f',1,'Create','App\\Role',3,'App\\Role',3,'App\\Role',3,'','finished','','2019-10-03 19:31:56','2019-10-03 19:31:56',NULL,'{\"name\":\"\\u89c4\\u5212\\u5e08\",\"updated_at\":\"2019-10-03 23:31:56\",\"created_at\":\"2019-10-03 23:31:56\",\"id\":3}'),
	(44,'8ecdcca0-1774-421c-a0a4-c01330f69810',1,'Create','App\\Role',4,'App\\Role',4,'App\\Role',4,'','finished','','2019-10-03 19:32:04','2019-10-03 19:32:04',NULL,'{\"name\":\"\\u6587\\u6848\\u5e08\",\"updated_at\":\"2019-10-03 23:32:04\",\"created_at\":\"2019-10-03 23:32:04\",\"id\":4}'),
	(45,'8ecdccad-a004-463e-b421-05a3d14b8d40',1,'Create','App\\Role',5,'App\\Role',5,'App\\Role',5,'','finished','','2019-10-03 19:32:13','2019-10-03 19:32:13',NULL,'{\"name\":\"\\u6536\\u6b3e\\u5458\",\"updated_at\":\"2019-10-03 23:32:13\",\"created_at\":\"2019-10-03 23:32:13\",\"id\":5}'),
	(46,'8ecdccb6-967d-4aec-bf8b-f19c0fb1fb0e',1,'Create','App\\Role',6,'App\\Role',6,'App\\Role',6,'','finished','','2019-10-03 19:32:19','2019-10-03 19:32:19',NULL,'{\"name\":\"\\u884c\\u653f\\u5e08\",\"updated_at\":\"2019-10-03 23:32:19\",\"created_at\":\"2019-10-03 23:32:19\",\"id\":6}'),
	(47,'8ecdcccc-8d64-4051-8724-9aa7416fcbda',1,'Delete','App\\Role',6,'App\\Role',6,'App\\Role',6,'','finished','','2019-10-03 19:32:33','2019-10-03 19:32:33',NULL,NULL),
	(48,'8ecdcd01-346b-4995-95fb-a7f9994369f5',1,'Update','App\\User',16,'App\\User',16,'App\\User',16,'','finished','','2019-10-03 19:33:08','2019-10-03 19:33:08','{\"role_id\":2,\"phone\":\"+1(647)-870-2666\"}','{\"role_id\":1,\"phone\":\"+1+1(647)-870-26\"}'),
	(49,'8ecdcd86-da91-487b-9f5a-4111bb7d3c7f',16,'Delete','App\\User',14,'App\\User',14,'App\\User',14,'','finished','','2019-10-03 19:34:36','2019-10-03 19:34:36',NULL,NULL),
	(50,'8ecdcd8e-c64b-4774-85a6-5ef9b843dcc1',16,'Delete','App\\User',15,'App\\User',15,'App\\User',15,'','finished','','2019-10-03 19:34:41','2019-10-03 19:34:41',NULL,NULL),
	(51,'8ecdcd94-67e9-43dc-ba37-73a55453403a',16,'Delete','App\\User',12,'App\\User',12,'App\\User',12,'','finished','','2019-10-03 19:34:44','2019-10-03 19:34:44',NULL,NULL),
	(52,'8ecdcda6-c265-4c03-bea7-354b45edf520',16,'Update','App\\User',16,'App\\User',16,'App\\User',16,'','finished','','2019-10-03 19:34:56','2019-10-03 19:34:56','{\"phone\":\"+1+1(647)-870-26\"}','{\"phone\":\"+1(647)-870\"}'),
	(53,'8ecdcdc9-4048-4c8d-b9fd-dd30138bae72',16,'Update','App\\User',16,'App\\User',16,'App\\User',16,'','finished','','2019-10-03 19:35:19','2019-10-03 19:35:19','{\"address\":null,\"phone\":\"+1(647)-870\"}','{\"address\":\"Unit20 Amber St\",\"phone\":\"+1(647)-870-2666\"}'),
	(54,'8ecdce76-f113-4620-89d4-51200c575c72',16,'Update','App\\User',16,'App\\User',16,'App\\User',16,'','finished','','2019-10-03 19:37:13','2019-10-03 19:37:13','{\"passport\":\"EB548575\",\"sin_number\":564852369,\"address\":\"Unit20 Amber St\",\"city\":null,\"province\":null,\"postal_code\":null,\"birthdate\":\"1996-08-05\",\"phone\":\"+1(647)-870-2666\",\"join_date\":\"2018-05-01\"}','{\"passport\":null,\"sin_number\":null,\"address\":\"Unit 201\\uff0c20 Amber St\",\"city\":\"Markham\",\"province\":\"ON\",\"postal_code\":\"L3R 5P4\",\"birthdate\":\"1991-09-19 00:00:00\",\"phone\":\"+1+1(647)-870-26\",\"join_date\":\"2018-01-01 00:00:00\",\"Password\":\"$2y$10$71sZd4slmfghTwyeGbRNd.EAfCid.kKX5PklGcSf0W0Su586XuV1m\"}'),
	(55,'8ecdcf82-8689-4698-a053-2c14a11b62b1',16,'Create','App\\User',17,'App\\User',17,'App\\User',17,'','finished','','2019-10-03 19:40:08','2019-10-03 19:40:08',NULL,'{\"name\":\"\\u5f20\\u7fca\\u4eba\",\"gender\":\"Male\",\"birthdate\":\"1987-01-12 00:00:00\",\"phone\":\"+1(647)-808-8458\",\"contact_email\":\"Zero870112@hotmail.com\",\"identity\":\"PR\",\"join_date\":\"2018-01-01 00:00:00\",\"department_id\":5,\"title_id\":47,\"job_level_id\":7,\"salary\":null,\"passport\":null,\"sin_number\":\"578385973\",\"email\":\"Zero870112@hotmail.com\",\"Password\":\"$2y$10$o11z1HcP9I87Gi37BQZSduipRNiFqXJqcOQn53k8iOhmiZch4u4Da\",\"role_id\":4,\"address\":\"506-8110  Brichmount Road\",\"city\":\"Markham\",\"province\":\"ON\",\"postal_code\":\"L6G 0E3\",\"updated_at\":\"2019-10-03 23:40:08\",\"created_at\":\"2019-10-03 23:40:08\",\"id\":17}'),
	(56,'8ecdd03e-a10c-4817-9dff-46dadf3dde4f',16,'Update','App\\User',16,'App\\User',16,'App\\User',16,'','finished','','2019-10-03 19:42:12','2019-10-03 19:42:12','{\"phone\":\"+1+1(647)-870-26\"}','{\"phone\":\"+1\"}'),
	(57,'8ecdd05c-a646-44af-8a15-51d9b95d3616',16,'Update','App\\User',16,'App\\User',16,'App\\User',16,'','finished','','2019-10-03 19:42:31','2019-10-03 19:42:31','{\"phone\":\"+1\"}','{\"phone\":\"+1(647)-870-2666\"}'),
	(58,'8ecdd135-d24b-4865-a1d4-e55f728c5aa3',16,'Create','App\\User',18,'App\\User',18,'App\\User',18,'','finished','','2019-10-03 19:44:54','2019-10-03 19:44:54',NULL,'{\"name\":\"\\u5b8b\\u6d77\\u57f9\",\"gender\":\"Male\",\"birthdate\":\"1990-10-11 00:00:00\",\"phone\":\"+1(416)-846-2886\",\"contact_email\":\"angle.hq@hotmail.com\",\"identity\":\"PR\",\"join_date\":\"2018-01-01 00:00:00\",\"department_id\":5,\"title_id\":2,\"job_level_id\":7,\"salary\":null,\"passport\":null,\"sin_number\":null,\"email\":\"angle.hq@hotmail.com\",\"Password\":\"$2y$10$nl7K\\/YGXYkNul\\/RU0.w99ubiIo3i1s.u.TCnXVgsAgc0ZXpCxREw2\",\"role_id\":4,\"address\":null,\"city\":null,\"province\":null,\"postal_code\":null,\"updated_at\":\"2019-10-03 23:44:54\",\"created_at\":\"2019-10-03 23:44:54\",\"id\":18}'),
	(59,'8ecdd31f-64e0-409a-beba-76dc59df147f',16,'Create','App\\User',19,'App\\User',19,'App\\User',19,'','finished','','2019-10-03 19:50:14','2019-10-03 19:50:14',NULL,'{\"name\":\"\\u6b27\\u9633\\u4fca\\u6770\",\"gender\":\"Male\",\"birthdate\":\"1989-08-04 00:00:00\",\"phone\":\"+1(416)-875-6689\",\"contact_email\":\"oyworkshop@hotmail.com\",\"identity\":\"CC\",\"join_date\":\"2018-03-15 00:00:00\",\"department_id\":2,\"title_id\":33,\"job_level_id\":7,\"salary\":\"1000\",\"passport\":null,\"sin_number\":\"545256471\",\"email\":\"oyworkshop@hotmail.com\",\"Password\":\"$2y$10$wVFQf\\/OWmyFVXW8evw2w4eVnhk9yC7gBPZrCr95JIp3yxL6p2K9zi\",\"role_id\":null,\"address\":null,\"city\":null,\"province\":null,\"postal_code\":null,\"updated_at\":\"2019-10-03 23:50:14\",\"created_at\":\"2019-10-03 23:50:14\",\"id\":19}'),
	(60,'8ecdd342-17e7-4a0b-b0be-d3c6c4d815d2',16,'Create','App\\Role',7,'App\\Role',7,'App\\Role',7,'','finished','','2019-10-03 19:50:37','2019-10-03 19:50:37',NULL,'{\"name\":\"\\u884c\\u653f\\u5e08\",\"updated_at\":\"2019-10-03 23:50:37\",\"created_at\":\"2019-10-03 23:50:37\",\"id\":7}'),
	(61,'8ecdd35f-596c-45ae-ad06-f24a6a4f9c78',16,'Update','App\\User',19,'App\\User',19,'App\\User',19,'','finished','','2019-10-03 19:50:56','2019-10-03 19:50:56','{\"role_id\":null,\"phone\":\"+1(416)-875-6689\"}','{\"role_id\":7,\"phone\":\"+1+1(416)-875-66\",\"Password\":\"$2y$10$VghCGIgeV3hsfRQpYLATO.kuo7mtjcEmnoSavog9ikQCvEb45sdXG\"}'),
	(62,'8ecdd37f-2d13-491b-9878-0d626d21a937',16,'Update','App\\User',19,'App\\User',19,'App\\User',19,'','finished','','2019-10-03 19:51:17','2019-10-03 19:51:17','{\"phone\":\"+1+1(416)-875-66\"}','{\"phone\":\"+1(416)-875-6689\"}'),
	(63,'8ecdd444-19c1-4099-8366-76676b13f03f',16,'Create','App\\User',20,'App\\User',20,'App\\User',20,'','finished','','2019-10-03 19:53:26','2019-10-03 19:53:26',NULL,'{\"name\":\"\\u5b59\\u6d69\\u68e0\",\"gender\":\"Male\",\"birthdate\":\"1995-06-13 00:00:00\",\"phone\":\"+1(541)-566-3722\",\"contact_email\":\"timsun@lpfirm.ca\",\"identity\":\"CC\",\"join_date\":\"2018-01-01 00:00:00\",\"department_id\":4,\"title_id\":4,\"job_level_id\":6,\"salary\":null,\"passport\":null,\"sin_number\":\"565176708\",\"email\":\"timsun@lpfirm.ca\",\"Password\":\"$2y$10$8FnPDpqZv3gNR2j8OgGP3OOUtg4imWGflI5C\\/Sk00HECPe0QP.W6K\",\"role_id\":3,\"address\":null,\"city\":null,\"province\":null,\"postal_code\":null,\"updated_at\":\"2019-10-03 23:53:26\",\"created_at\":\"2019-10-03 23:53:26\",\"id\":20}'),
	(64,'8ecde0f4-1005-4a5a-9d4d-598013b7d79e',16,'Create','App\\User',21,'App\\User',21,'App\\User',21,'','finished','','2019-10-03 20:28:55','2019-10-03 20:28:55',NULL,'{\"name\":\"\\u9ad8\\u59ff\",\"gender\":\"Female\",\"birthdate\":\"1990-11-26 00:00:00\",\"phone\":\"+1(416)-721-9686\",\"contact_email\":\"cynthiapink1013@gmail.com\",\"identity\":\"PR\",\"join_date\":\"2018-10-01 00:00:00\",\"department_id\":4,\"title_id\":null,\"job_level_id\":null,\"salary\":null,\"passport\":null,\"sin_number\":null,\"email\":\"cynthiapink1013@gmail.com\",\"Password\":\"$2y$10$ESmTOrwwxtWBnWyIZnIaI.oBPWtamrCJrcRKYj65\\/QQwDAMeUhffm\",\"role_id\":null,\"address\":null,\"city\":null,\"province\":null,\"postal_code\":null,\"updated_at\":\"2019-10-04 00:28:55\",\"created_at\":\"2019-10-04 00:28:55\",\"id\":21}'),
	(65,'8ecde105-f001-486e-b414-f74bd9373cd4',16,'Create','App\\Department',6,'App\\Department',6,'App\\Department',6,'','finished','','2019-10-03 20:29:07','2019-10-03 20:29:07',NULL,'{\"name\":\"\\u89c4\\u5212\\u90e8\",\"updated_at\":\"2019-10-04 00:29:07\",\"created_at\":\"2019-10-04 00:29:07\",\"id\":6}'),
	(66,'8ecde128-aa7e-432f-86a3-230662a4cd28',16,'Update','App\\User',21,'App\\User',21,'App\\User',21,'','finished','','2019-10-03 20:29:29','2019-10-03 20:29:29','{\"department_id\":4,\"phone\":\"+1(416)-721-9686\"}','{\"department_id\":6,\"phone\":\"+1+1(416)-721-96\"}'),
	(67,'8ecde164-ff9b-436a-890f-fbd00537cd5e',16,'Update','App\\User',21,'App\\User',21,'App\\User',21,'','finished','','2019-10-03 20:30:09','2019-10-03 20:30:09','{\"job_level_id\":null,\"sin_number\":null,\"title_id\":null,\"phone\":\"+1+1(416)-721-96\"}','{\"job_level_id\":6,\"sin_number\":\"592056527\",\"title_id\":5,\"phone\":\"+1+1+1(416)-721\",\"Password\":\"$2y$10$LPBI3FpO\\/oJPNm.ZvOxAE.eMfb7DDj43VxQGJzbV9byao\\/krQgCua\"}'),
	(68,'8ecde177-5283-4489-9494-d0ae881fdb78',16,'Delete','App\\JobLevel',1,'App\\JobLevel',1,'App\\JobLevel',1,'','finished','','2019-10-03 20:30:21','2019-10-03 20:30:21',NULL,NULL),
	(69,'8ecde255-e606-4819-9176-9bec9479a797',16,'Update','App\\User',21,'App\\User',21,'App\\User',21,'','finished','','2019-10-03 20:32:47','2019-10-03 20:32:47','{\"phone\":\"+1+1+1(416)-721\"}','{\"phone\":\"+1(416)-72\"}'),
	(70,'8ecde2b0-67c2-4678-93d3-1e58c6f6c497',16,'Update','App\\User',21,'App\\User',21,'App\\User',21,'','finished','','2019-10-03 20:33:46','2019-10-03 20:33:46','{\"phone\":\"+1(416)-72\"}','{\"phone\":\"+1(416)-721-9686\"}'),
	(71,'8ecde3ed-f6d3-4b9e-b659-b68236f7c151',16,'Create','App\\User',22,'App\\User',22,'App\\User',22,'','finished','','2019-10-03 20:37:14','2019-10-03 20:37:14',NULL,'{\"name\":\"\\u674e\\u5efa\\u83b9\",\"gender\":\"Female\",\"birthdate\":\"1993-08-09 00:00:00\",\"phone\":\"+1(647)-895-5856\",\"contact_email\":\"mumulee555@gmail.com\",\"identity\":\"WP\",\"join_date\":\"2018-05-01 00:00:00\",\"department_id\":4,\"title_id\":35,\"job_level_id\":7,\"salary\":null,\"passport\":null,\"sin_number\":\"930518295\",\"email\":\"mumulee555@gmail.com\",\"Password\":\"$2y$10$krwEEYZWvr8BwY9Y56fb0.s6v\\/zzsheC.sMlzEFvHrKAbAbgOwU.G\",\"role_id\":3,\"address\":null,\"city\":null,\"province\":null,\"postal_code\":null,\"updated_at\":\"2019-10-04 00:37:14\",\"created_at\":\"2019-10-04 00:37:14\",\"id\":22}'),
	(72,'8ecde4a9-959d-4b02-8289-4c4b2e07ec3b',16,'Create','App\\User',23,'App\\User',23,'App\\User',23,'','finished','','2019-10-03 20:39:17','2019-10-03 20:39:17',NULL,'{\"name\":\"\\u5434\\u4f73\\u4f0a\",\"gender\":\"Female\",\"birthdate\":\"1993-02-02 00:00:00\",\"phone\":\"+1(647)-871-4622\",\"contact_email\":\"wujiayi9322@gmail.com\",\"identity\":\"WP\",\"join_date\":\"2018-05-01 00:00:00\",\"department_id\":5,\"title_id\":null,\"job_level_id\":7,\"salary\":null,\"passport\":null,\"sin_number\":\"945376192\",\"email\":\"wujiayi9322@gmail.com\",\"Password\":\"$2y$10$3hdsXaU2EoenzDArkk21P.NY8eMcTGYPz.2vmk02d4hxO.deJTKVS\",\"role_id\":4,\"address\":null,\"city\":null,\"province\":null,\"postal_code\":null,\"updated_at\":\"2019-10-04 00:39:17\",\"created_at\":\"2019-10-04 00:39:17\",\"id\":23}'),
	(73,'8ece0b20-766d-421c-84cb-dfb2be8d9398',16,'Create','App\\User',24,'App\\User',24,'App\\User',24,'','finished','','2019-10-03 22:26:50','2019-10-03 22:26:50',NULL,'{\"name\":\"\\u8bb8\\u8bfa2\",\"gender\":\"Male\",\"birthdate\":\"1991-09-19 00:00:00\",\"phone\":\"+1(647)-996-0919\",\"contact_email\":\"xu.nuo@lpfeim.ca\",\"identity\":\"PR\",\"join_date\":\"2018-05-05 00:00:00\",\"department_id\":null,\"title_id\":null,\"job_level_id\":null,\"salary\":null,\"passport\":null,\"sin_number\":null,\"email\":\"xu.nuo@lpfeim.ca\",\"Password\":\"$2y$10$ER6Pn71WMj6MYfthBysEiu7duQws2roQbeuQ83WgwdT95\\/8VzE5Fa\",\"role_id\":null,\"address\":null,\"city\":null,\"province\":null,\"postal_code\":null,\"updated_at\":\"2019-10-04 02:26:50\",\"created_at\":\"2019-10-04 02:26:50\",\"id\":24}'),
	(74,'8ece0b44-fca9-47f9-ae08-37b976f3b186',16,'Update','App\\User',24,'App\\User',24,'App\\User',24,'','finished','','2019-10-03 22:27:14','2019-10-03 22:27:14','{\"email\":\"xu.nuo@lpfeim.ca\",\"phone\":\"+1(647)-996-0919\"}','{\"email\":\"xu.nuo@lpfrim.ca\",\"phone\":\"+1+1(647)-996-09\"}'),
	(75,'8ece0b73-dd68-4de7-8539-fd0df8b5f1de',16,'Update','App\\User',24,'App\\User',24,'App\\User',24,'','finished','','2019-10-03 22:27:45','2019-10-03 22:27:45','{\"contact_email\":\"xu.nuo@lpfeim.ca\",\"phone\":\"+1+1(647)-996-09\"}','{\"contact_email\":\"xu.nuo@lpfirm.ca\",\"phone\":\"+1(647)-996\"}'),
	(76,'8ece0b84-3061-4e55-8aca-b901988d64c6',16,'Update','App\\User',24,'App\\User',24,'App\\User',24,'','finished','','2019-10-03 22:27:56','2019-10-03 22:27:56','{\"phone\":\"+1(647)-996\"}','{\"phone\":\"+1(647)-996-0919\"}'),
	(77,'8ece0c11-ac1f-45c4-8fa1-53e013062556',1,'Update','App\\User',24,'App\\User',24,'App\\User',24,'','finished','','2019-10-03 22:29:28','2019-10-03 22:29:28','{\"phone\":\"+1(647)-996-0919\"}','{\"phone\":\"+1+1(647)-996-09\",\"Password\":\"$2y$10$EgwMHHbr1sRC1q86Sqbzu.ocRQ2pPepkoudXNHarYtmBjEXcri1pu\"}'),
	(78,'8ecf3c51-a1be-46cc-963c-c571296f1b76',16,'Create','App\\User',25,'App\\User',25,'App\\User',25,'','finished','','2019-10-04 12:40:13','2019-10-04 12:40:13',NULL,'{\"name\":\"\\u5f20\\u6893\\u82ae\",\"gender\":\"Female\",\"birthdate\":\"1995-04-24 00:00:00\",\"phone\":\"+1(905)-348-8188\",\"contact_email\":\"olivia.zhcc@gmail.com\",\"identity\":\"WP\",\"join_date\":\"2018-05-01 00:00:00\",\"department_id\":6,\"title_id\":null,\"job_level_id\":2,\"salary\":null,\"passport\":null,\"sin_number\":\"935938803\",\"email\":\"olivia.zhcc@gmail.com\",\"Password\":\"$2y$10$I7Z7yvbf5EsagPvgmvXzp.0vsekrbXu4M3JiUNeaEB\\/N.e65680Hu\",\"role_id\":3,\"address\":null,\"city\":null,\"province\":null,\"postal_code\":null,\"updated_at\":\"2019-10-04 16:40:13\",\"created_at\":\"2019-10-04 16:40:13\",\"id\":25}'),
	(79,'8ecf41e9-2f91-4e6b-b261-97ba70a47462',16,'Create','App\\JobLevel',8,'App\\JobLevel',8,'App\\JobLevel',8,'','finished','','2019-10-04 12:55:51','2019-10-04 12:55:51',NULL,'{\"name\":\"\\u884c\\u653f\\u89c4\\u5212\\u5e08\",\"updated_at\":\"2019-10-04 16:55:51\",\"created_at\":\"2019-10-04 16:55:51\",\"id\":8}'),
	(80,'8ecf4304-f441-44ca-ab03-42cdd231dfdf',16,'Delete','App\\User',24,'App\\User',24,'App\\User',24,'','finished','','2019-10-04 12:58:57','2019-10-04 12:58:57',NULL,NULL),
	(81,'8edbee87-8fda-4e48-abf2-73d07c0e4ef0',1,'Update','App\\User',25,'App\\User',25,'App\\User',25,'','finished','','2019-10-11 00:08:28','2019-10-11 00:08:28','{\"department_id\":6,\"title_id\":null,\"phone\":\"+1(905)-348-8188\"}','{\"department_id\":5,\"title_id\":8,\"phone\":\"+1+1(905)-348-81\"}'),
	(82,'8edc047d-6afd-49ca-9b62-1c06f1aa213c',1,'Update','App\\User',25,'App\\User',25,'App\\User',25,'','finished','','2019-10-11 01:09:52','2019-10-11 01:09:52','{\"title_id\":8,\"phone\":\"+1+1(905)-348-81\"}','{\"title_id\":10,\"phone\":\"+1+1+1(905)-348\"}'),
	(83,'8edc04c0-0a68-45a2-9f47-18ead474a857',1,'Update','App\\User',25,'App\\User',25,'App\\User',25,'','finished','','2019-10-11 01:10:36','2019-10-11 01:10:36','{\"title_id\":10,\"phone\":\"+1+1+1(905)-348\"}','{\"title_id\":9,\"phone\":\"+1+1+1+1(905)-34\"}'),
	(84,'8edd2046-dbd3-4fe5-8a88-40aea825cc87',1,'Update','App\\Client',201,'App\\Client',201,'App\\Client',201,'','finished','','2019-10-11 14:23:24','2019-10-11 14:23:24','{\"photo\":\"\"}','{\"photo\":\"bQNlatsZLZYerczUs1zaUdoXeox4XxL8KTho9EJ3.jpeg\"}'),
	(85,'8edd2057-93f3-4503-8335-20da49128291',1,'Update','App\\Client',201,'App\\Client',201,'App\\Client',201,'','finished','','2019-10-11 14:23:35','2019-10-11 14:23:35','[]','[]'),
	(86,'8edd20fd-4b08-4b23-a5ed-cefc80e2dfbd',1,'Update','App\\Client',201,'App\\Client',201,'App\\Client',201,'','finished','','2019-10-11 14:25:23','2019-10-11 14:25:23','{\"photo\":\"bQNlatsZLZYerczUs1zaUdoXeox4XxL8KTho9EJ3.jpeg\"}','{\"photo\":\"W9XOkpiZqtbo8WHsUQxUTY4W2mU6NVt62J1lSKu5.jpeg\"}'),
	(87,'8edd2968-d553-4a87-8800-2ce9d99298f2',1,'Update','App\\User',25,'App\\User',25,'App\\User',25,'','finished','','2019-10-11 14:48:56','2019-10-11 14:48:56','{\"name\":\"\\u5f20\\u6893\\u82ae\",\"phone\":\"+1+1+1+1(905)-34\"}','{\"name\":\"Alvin Zhong\",\"phone\":\"+1(905)-000-0000\"}'),
	(88,'8edd2c5b-04f7-4737-ba6d-a25204b201b7',1,'Update','App\\User',1,'App\\User',1,'App\\User',1,'','finished','','2019-10-11 14:57:10','2019-10-11 14:57:10','{\"phone\":\"+1\"}','{\"phone\":\"+1(905)-000-0000\"}'),
	(89,'8edd2ec8-7a90-4bb7-bbab-59752e9c1676',1,'Update','App\\User',25,'App\\User',25,'App\\User',25,'','finished','','2019-10-11 15:03:58','2019-10-11 15:03:58','{\"salary\":null,\"phone\":\"+1(905)-000-0000\"}','{\"salary\":\"0\",\"phone\":\"+1+1(905)-000-00\"}'),
	(90,'8edd2f22-b961-4bc8-9b52-2b7d541419c6',1,'Update','App\\User',25,'App\\User',25,'App\\User',25,'','finished','','2019-10-11 15:04:57','2019-10-11 15:04:57','{\"department_id\":5,\"title_id\":9,\"phone\":\"+1+1(905)-000-00\"}','{\"department_id\":4,\"title_id\":30,\"phone\":\"+1+1+1(905)-000\"}'),
	(91,'8edd30d2-eaff-4e95-a137-dc7503fdf7a3',1,'Create','App\\User',26,'App\\User',26,'App\\User',26,'','finished','','2019-10-11 15:09:40','2019-10-11 15:09:40',NULL,'{\"name\":\"test\",\"gender\":\"Male\",\"birthdate\":\"2000-01-01 00:00:00\",\"phone\":\"+1(905)-000-0001\",\"contact_email\":\"test@gmail.com\",\"identity\":\"CN\",\"join_date\":\"2019-01-01 00:00:00\",\"department_id\":4,\"title_id\":29,\"job_level_id\":3,\"salary\":\"0\",\"passport\":\"999-999-999\",\"sin_number\":\"999999999\",\"email\":\"test1@gmail.com\",\"Password\":\"$2y$10$Khuy9VVVHnQyQGrI1e5e.uOfTfPcrc6nRVYLbLL5pjSKSHh9xOaf6\",\"role_id\":2,\"address\":\"2-110 cre\",\"city\":\"markham\",\"province\":\"Ontario\",\"postal_code\":\"L3R 1H3\",\"updated_at\":\"2019-10-11 15:09:40\",\"created_at\":\"2019-10-11 15:09:40\",\"id\":26}'),
	(92,'8edd321a-0ff0-4be3-afbf-de6d68d416aa',26,'Update','App\\Client',1,'App\\Client',1,'App\\Client',1,'','finished','','2019-10-11 15:13:14','2019-10-11 15:13:14','{\"usedName\":\"\",\"user_id\":12}','{\"usedName\":null,\"user_id\":null}'),
	(93,'8edd34ca-072a-41b3-a138-609936bcaa74',1,'Update','App\\Client',200,'App\\Client',200,'App\\Client',200,'','finished','','2019-10-11 15:20:45','2019-10-11 15:20:45','{\"usedName\":\"\",\"visa_expire\":\"2020-05-30\",\"user_id\":7}','{\"usedName\":null,\"visa_expire\":\"2019-09-13 00:00:00\",\"user_id\":null}'),
	(94,'8edd35e4-1690-43ec-99a8-efd1fc75f592',1,'Update','App\\Client',200,'App\\Client',200,'App\\Client',200,'','finished','','2019-10-11 15:23:50','2019-10-11 15:23:50','{\"status_in_canada_id\":null}','{\"status_in_canada_id\":1}');

/*!40000 ALTER TABLE `action_events` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cities`;

CREATE TABLE `cities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province_id` bigint(20) unsigned DEFAULT NULL,
  `region_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;

INSERT INTO `cities` (`id`, `name`, `province_id`, `region_id`, `created_at`, `updated_at`)
VALUES
	(1101,'北京市',11,1,NULL,NULL),
	(1201,'天津市',12,1,NULL,NULL),
	(1301,'石家庄市',13,1,NULL,NULL),
	(1302,'唐山市',13,1,NULL,NULL),
	(1303,'秦皇岛市',13,1,NULL,NULL),
	(1304,'邯郸市',13,1,NULL,NULL),
	(1305,'邢台市',13,1,NULL,NULL),
	(1306,'保定市',13,1,NULL,NULL),
	(1307,'张家口市',13,1,NULL,NULL),
	(1308,'承德市',13,1,NULL,NULL),
	(1309,'沧州市',13,1,NULL,NULL),
	(1310,'廊坊市',13,1,NULL,NULL),
	(1311,'衡水市',13,1,NULL,NULL),
	(1401,'太原市',14,1,NULL,NULL),
	(1402,'大同市',14,1,NULL,NULL),
	(1403,'阳泉市',14,1,NULL,NULL),
	(1404,'长治市',14,1,NULL,NULL),
	(1405,'晋城市',14,1,NULL,NULL),
	(1406,'朔州市',14,1,NULL,NULL),
	(1407,'晋中市',14,1,NULL,NULL),
	(1408,'运城市',14,1,NULL,NULL),
	(1409,'忻州市',14,1,NULL,NULL),
	(1410,'临汾市',14,1,NULL,NULL),
	(1411,'吕梁市',14,1,NULL,NULL),
	(1501,'呼和浩特市',15,1,NULL,NULL),
	(1502,'包头市',15,1,NULL,NULL),
	(1503,'乌海市',15,1,NULL,NULL),
	(1504,'赤峰市',15,1,NULL,NULL),
	(1505,'通辽市',15,1,NULL,NULL),
	(1506,'鄂尔多斯市',15,1,NULL,NULL),
	(1507,'呼伦贝尔市',15,1,NULL,NULL),
	(1508,'巴彦淖尔市',15,1,NULL,NULL),
	(1509,'乌兰察布市',15,1,NULL,NULL),
	(1522,'兴安盟',15,1,NULL,NULL),
	(1525,'锡林郭勒盟',15,1,NULL,NULL),
	(1529,'阿拉善盟',15,1,NULL,NULL),
	(2101,'沈阳市',21,2,NULL,NULL),
	(2102,'大连市',21,2,NULL,NULL),
	(2103,'鞍山市',21,2,NULL,NULL),
	(2104,'抚顺市',21,2,NULL,NULL),
	(2105,'本溪市',21,2,NULL,NULL),
	(2106,'丹东市',21,2,NULL,NULL),
	(2107,'锦州市',21,2,NULL,NULL),
	(2108,'营口市',21,2,NULL,NULL),
	(2109,'阜新市',21,2,NULL,NULL),
	(2110,'辽阳市',21,2,NULL,NULL),
	(2111,'盘锦市',21,2,NULL,NULL),
	(2112,'铁岭市',21,2,NULL,NULL),
	(2113,'朝阳市',21,2,NULL,NULL),
	(2114,'葫芦岛市',21,2,NULL,NULL),
	(2201,'长春市',22,2,NULL,NULL),
	(2202,'吉林市',22,2,NULL,NULL),
	(2203,'四平市',22,2,NULL,NULL),
	(2204,'辽源市',22,2,NULL,NULL),
	(2205,'通化市',22,2,NULL,NULL),
	(2206,'白山市',22,2,NULL,NULL),
	(2207,'松原市',22,2,NULL,NULL),
	(2208,'白城市',22,2,NULL,NULL),
	(2224,'延边朝鲜族自治州',22,2,NULL,NULL),
	(2301,'哈尔滨市',23,2,NULL,NULL),
	(2302,'齐齐哈尔市',23,2,NULL,NULL),
	(2303,'鸡西市',23,2,NULL,NULL),
	(2304,'鹤岗市',23,2,NULL,NULL),
	(2305,'双鸭山市',23,2,NULL,NULL),
	(2306,'大庆市',23,2,NULL,NULL),
	(2307,'伊春市',23,2,NULL,NULL),
	(2308,'佳木斯市',23,2,NULL,NULL),
	(2309,'七台河市',23,2,NULL,NULL),
	(2310,'牡丹江市',23,2,NULL,NULL),
	(2311,'黑河市',23,2,NULL,NULL),
	(2312,'绥化市',23,2,NULL,NULL),
	(2327,'大兴安岭地区',23,2,NULL,NULL),
	(3101,'上海市',31,3,NULL,NULL),
	(3201,'南京市',32,3,NULL,NULL),
	(3202,'无锡市',32,3,NULL,NULL),
	(3203,'徐州市',32,3,NULL,NULL),
	(3204,'常州市',32,3,NULL,NULL),
	(3205,'苏州市',32,3,NULL,NULL),
	(3206,'南通市',32,3,NULL,NULL),
	(3207,'连云港市',32,3,NULL,NULL),
	(3208,'淮安市',32,3,NULL,NULL),
	(3209,'盐城市',32,3,NULL,NULL),
	(3210,'扬州市',32,3,NULL,NULL),
	(3211,'镇江市',32,3,NULL,NULL),
	(3212,'泰州市',32,3,NULL,NULL),
	(3213,'宿迁市',32,3,NULL,NULL),
	(3301,'杭州市',33,3,NULL,NULL),
	(3302,'宁波市',33,3,NULL,NULL),
	(3303,'温州市',33,3,NULL,NULL),
	(3304,'嘉兴市',33,3,NULL,NULL),
	(3305,'湖州市',33,3,NULL,NULL),
	(3306,'绍兴市',33,3,NULL,NULL),
	(3307,'金华市',33,3,NULL,NULL),
	(3308,'衢州市',33,3,NULL,NULL),
	(3309,'舟山市',33,3,NULL,NULL),
	(3310,'台州市',33,3,NULL,NULL),
	(3311,'丽水市',33,3,NULL,NULL),
	(3401,'合肥市',34,3,NULL,NULL),
	(3402,'芜湖市',34,3,NULL,NULL),
	(3403,'蚌埠市',34,3,NULL,NULL),
	(3404,'淮南市',34,3,NULL,NULL),
	(3405,'马鞍山市',34,3,NULL,NULL),
	(3406,'淮北市',34,3,NULL,NULL),
	(3407,'铜陵市',34,3,NULL,NULL),
	(3408,'安庆市',34,3,NULL,NULL),
	(3410,'黄山市',34,3,NULL,NULL),
	(3411,'滁州市',34,3,NULL,NULL),
	(3412,'阜阳市',34,3,NULL,NULL),
	(3413,'宿州市',34,3,NULL,NULL),
	(3415,'六安市',34,3,NULL,NULL),
	(3416,'亳州市',34,3,NULL,NULL),
	(3417,'池州市',34,3,NULL,NULL),
	(3418,'宣城市',34,3,NULL,NULL),
	(3501,'福州市',35,3,NULL,NULL),
	(3502,'厦门市',35,3,NULL,NULL),
	(3503,'莆田市',35,3,NULL,NULL),
	(3504,'三明市',35,3,NULL,NULL),
	(3505,'泉州市',35,3,NULL,NULL),
	(3506,'漳州市',35,3,NULL,NULL),
	(3507,'南平市',35,3,NULL,NULL),
	(3508,'龙岩市',35,3,NULL,NULL),
	(3509,'宁德市',35,3,NULL,NULL),
	(3601,'南昌市',36,3,NULL,NULL),
	(3602,'景德镇市',36,3,NULL,NULL),
	(3603,'萍乡市',36,3,NULL,NULL),
	(3604,'九江市',36,3,NULL,NULL),
	(3605,'新余市',36,3,NULL,NULL),
	(3606,'鹰潭市',36,3,NULL,NULL),
	(3607,'赣州市',36,3,NULL,NULL),
	(3608,'吉安市',36,3,NULL,NULL),
	(3609,'宜春市',36,3,NULL,NULL),
	(3610,'抚州市',36,3,NULL,NULL),
	(3611,'上饶市',36,3,NULL,NULL),
	(3701,'济南市',37,3,NULL,NULL),
	(3702,'青岛市',37,3,NULL,NULL),
	(3703,'淄博市',37,3,NULL,NULL),
	(3704,'枣庄市',37,3,NULL,NULL),
	(3705,'东营市',37,3,NULL,NULL),
	(3706,'烟台市',37,3,NULL,NULL),
	(3707,'潍坊市',37,3,NULL,NULL),
	(3708,'济宁市',37,3,NULL,NULL),
	(3709,'泰安市',37,3,NULL,NULL),
	(3710,'威海市',37,3,NULL,NULL),
	(3711,'日照市',37,3,NULL,NULL),
	(3712,'莱芜市',37,3,NULL,NULL),
	(3713,'临沂市',37,3,NULL,NULL),
	(3714,'德州市',37,3,NULL,NULL),
	(3715,'聊城市',37,3,NULL,NULL),
	(3716,'滨州市',37,3,NULL,NULL),
	(3717,'菏泽市',37,3,NULL,NULL),
	(4101,'郑州市',41,4,NULL,NULL),
	(4102,'开封市',41,4,NULL,NULL),
	(4103,'洛阳市',41,4,NULL,NULL),
	(4104,'平顶山市',41,4,NULL,NULL),
	(4105,'安阳市',41,4,NULL,NULL),
	(4106,'鹤壁市',41,4,NULL,NULL),
	(4107,'新乡市',41,4,NULL,NULL),
	(4108,'焦作市',41,4,NULL,NULL),
	(4109,'濮阳市',41,4,NULL,NULL),
	(4110,'许昌市',41,4,NULL,NULL),
	(4111,'漯河市',41,4,NULL,NULL),
	(4112,'三门峡市',41,4,NULL,NULL),
	(4113,'南阳市',41,4,NULL,NULL),
	(4114,'商丘市',41,4,NULL,NULL),
	(4115,'信阳市',41,4,NULL,NULL),
	(4116,'周口市',41,4,NULL,NULL),
	(4117,'驻马店市',41,4,NULL,NULL),
	(4190,'省直辖县级行政区划',41,4,NULL,NULL),
	(4201,'武汉市',42,4,NULL,NULL),
	(4202,'黄石市',42,4,NULL,NULL),
	(4203,'十堰市',42,4,NULL,NULL),
	(4205,'宜昌市',42,4,NULL,NULL),
	(4206,'襄阳市',42,4,NULL,NULL),
	(4207,'鄂州市',42,4,NULL,NULL),
	(4208,'荆门市',42,4,NULL,NULL),
	(4209,'孝感市',42,4,NULL,NULL),
	(4210,'荆州市',42,4,NULL,NULL),
	(4211,'黄冈市',42,4,NULL,NULL),
	(4212,'咸宁市',42,4,NULL,NULL),
	(4213,'随州市',42,4,NULL,NULL),
	(4228,'恩施土家族苗族自治州',42,4,NULL,NULL),
	(4290,'省直辖县级行政区划',42,4,NULL,NULL),
	(4301,'长沙市',43,4,NULL,NULL),
	(4302,'株洲市',43,4,NULL,NULL),
	(4303,'湘潭市',43,4,NULL,NULL),
	(4304,'衡阳市',43,4,NULL,NULL),
	(4305,'邵阳市',43,4,NULL,NULL),
	(4306,'岳阳市',43,4,NULL,NULL),
	(4307,'常德市',43,4,NULL,NULL),
	(4308,'张家界市',43,4,NULL,NULL),
	(4309,'益阳市',43,4,NULL,NULL),
	(4310,'郴州市',43,4,NULL,NULL),
	(4311,'永州市',43,4,NULL,NULL),
	(4312,'怀化市',43,4,NULL,NULL),
	(4313,'娄底市',43,4,NULL,NULL),
	(4331,'湘西土家族苗族自治州',43,4,NULL,NULL),
	(4401,'广州市',44,4,NULL,NULL),
	(4402,'韶关市',44,4,NULL,NULL),
	(4403,'深圳市',44,4,NULL,NULL),
	(4404,'珠海市',44,4,NULL,NULL),
	(4405,'汕头市',44,4,NULL,NULL),
	(4406,'佛山市',44,4,NULL,NULL),
	(4407,'江门市',44,4,NULL,NULL),
	(4408,'湛江市',44,4,NULL,NULL),
	(4409,'茂名市',44,4,NULL,NULL),
	(4412,'肇庆市',44,4,NULL,NULL),
	(4413,'惠州市',44,4,NULL,NULL),
	(4414,'梅州市',44,4,NULL,NULL),
	(4415,'汕尾市',44,4,NULL,NULL),
	(4416,'河源市',44,4,NULL,NULL),
	(4417,'阳江市',44,4,NULL,NULL),
	(4418,'清远市',44,4,NULL,NULL),
	(4419,'东莞市',44,4,NULL,NULL),
	(4420,'中山市',44,4,NULL,NULL),
	(4451,'潮州市',44,4,NULL,NULL),
	(4452,'揭阳市',44,4,NULL,NULL),
	(4453,'云浮市',44,4,NULL,NULL),
	(4501,'南宁市',45,4,NULL,NULL),
	(4502,'柳州市',45,4,NULL,NULL),
	(4503,'桂林市',45,4,NULL,NULL),
	(4504,'梧州市',45,4,NULL,NULL),
	(4505,'北海市',45,4,NULL,NULL),
	(4506,'防城港市',45,4,NULL,NULL),
	(4507,'钦州市',45,4,NULL,NULL),
	(4508,'贵港市',45,4,NULL,NULL),
	(4509,'玉林市',45,4,NULL,NULL),
	(4510,'百色市',45,4,NULL,NULL),
	(4511,'贺州市',45,4,NULL,NULL),
	(4512,'河池市',45,4,NULL,NULL),
	(4513,'来宾市',45,4,NULL,NULL),
	(4514,'崇左市',45,4,NULL,NULL),
	(4601,'海口市',46,4,NULL,NULL),
	(4602,'三亚市',46,4,NULL,NULL),
	(4603,'三沙市',46,4,NULL,NULL),
	(4604,'儋州市',46,4,NULL,NULL),
	(4690,'省直辖县级行政区划',46,4,NULL,NULL),
	(5001,'重庆市',50,5,NULL,NULL),
	(5002,'县',50,5,NULL,NULL),
	(5101,'成都市',51,5,NULL,NULL),
	(5103,'自贡市',51,5,NULL,NULL),
	(5104,'攀枝花市',51,5,NULL,NULL),
	(5105,'泸州市',51,5,NULL,NULL),
	(5106,'德阳市',51,5,NULL,NULL),
	(5107,'绵阳市',51,5,NULL,NULL),
	(5108,'广元市',51,5,NULL,NULL),
	(5109,'遂宁市',51,5,NULL,NULL),
	(5110,'内江市',51,5,NULL,NULL),
	(5111,'乐山市',51,5,NULL,NULL),
	(5113,'南充市',51,5,NULL,NULL),
	(5114,'眉山市',51,5,NULL,NULL),
	(5115,'宜宾市',51,5,NULL,NULL),
	(5116,'广安市',51,5,NULL,NULL),
	(5117,'达州市',51,5,NULL,NULL),
	(5118,'雅安市',51,5,NULL,NULL),
	(5119,'巴中市',51,5,NULL,NULL),
	(5120,'资阳市',51,5,NULL,NULL),
	(5132,'阿坝藏族羌族自治州',51,5,NULL,NULL),
	(5133,'甘孜藏族自治州',51,5,NULL,NULL),
	(5134,'凉山彝族自治州',51,5,NULL,NULL),
	(5201,'贵阳市',52,5,NULL,NULL),
	(5202,'六盘水市',52,5,NULL,NULL),
	(5203,'遵义市',52,5,NULL,NULL),
	(5204,'安顺市',52,5,NULL,NULL),
	(5205,'毕节市',52,5,NULL,NULL),
	(5206,'铜仁市',52,5,NULL,NULL),
	(5223,'黔西南布依族苗族自治州',52,5,NULL,NULL),
	(5226,'黔东南苗族侗族自治州',52,5,NULL,NULL),
	(5227,'黔南布依族苗族自治州',52,5,NULL,NULL),
	(5301,'昆明市',53,5,NULL,NULL),
	(5303,'曲靖市',53,5,NULL,NULL),
	(5304,'玉溪市',53,5,NULL,NULL),
	(5305,'保山市',53,5,NULL,NULL),
	(5306,'昭通市',53,5,NULL,NULL),
	(5307,'丽江市',53,5,NULL,NULL),
	(5308,'普洱市',53,5,NULL,NULL),
	(5309,'临沧市',53,5,NULL,NULL),
	(5323,'楚雄彝族自治州',53,5,NULL,NULL),
	(5325,'红河哈尼族彝族自治州',53,5,NULL,NULL),
	(5326,'文山壮族苗族自治州',53,5,NULL,NULL),
	(5328,'西双版纳傣族自治州',53,5,NULL,NULL),
	(5329,'大理白族自治州',53,5,NULL,NULL),
	(5331,'德宏傣族景颇族自治州',53,5,NULL,NULL),
	(5333,'怒江傈僳族自治州',53,5,NULL,NULL),
	(5334,'迪庆藏族自治州',53,5,NULL,NULL),
	(5401,'拉萨市',54,5,NULL,NULL),
	(5402,'日喀则市',54,5,NULL,NULL),
	(5403,'昌都市',54,5,NULL,NULL),
	(5404,'林芝市',54,5,NULL,NULL),
	(5405,'山南市',54,5,NULL,NULL),
	(5424,'那曲地区',54,5,NULL,NULL),
	(5425,'阿里地区',54,5,NULL,NULL),
	(6101,'西安市',61,6,NULL,NULL),
	(6102,'铜川市',61,6,NULL,NULL),
	(6103,'宝鸡市',61,6,NULL,NULL),
	(6104,'咸阳市',61,6,NULL,NULL),
	(6105,'渭南市',61,6,NULL,NULL),
	(6106,'延安市',61,6,NULL,NULL),
	(6107,'汉中市',61,6,NULL,NULL),
	(6108,'榆林市',61,6,NULL,NULL),
	(6109,'安康市',61,6,NULL,NULL),
	(6110,'商洛市',61,6,NULL,NULL),
	(6201,'兰州市',62,6,NULL,NULL),
	(6202,'嘉峪关市',62,6,NULL,NULL),
	(6203,'金昌市',62,6,NULL,NULL),
	(6204,'白银市',62,6,NULL,NULL),
	(6205,'天水市',62,6,NULL,NULL),
	(6206,'武威市',62,6,NULL,NULL),
	(6207,'张掖市',62,6,NULL,NULL),
	(6208,'平凉市',62,6,NULL,NULL),
	(6209,'酒泉市',62,6,NULL,NULL),
	(6210,'庆阳市',62,6,NULL,NULL),
	(6211,'定西市',62,6,NULL,NULL),
	(6212,'陇南市',62,6,NULL,NULL),
	(6229,'临夏回族自治州',62,6,NULL,NULL),
	(6230,'甘南藏族自治州',62,6,NULL,NULL),
	(6301,'西宁市',63,6,NULL,NULL),
	(6302,'海东市',63,6,NULL,NULL),
	(6322,'海北藏族自治州',63,6,NULL,NULL),
	(6323,'黄南藏族自治州',63,6,NULL,NULL),
	(6325,'海南藏族自治州',63,6,NULL,NULL),
	(6326,'果洛藏族自治州',63,6,NULL,NULL),
	(6327,'玉树藏族自治州',63,6,NULL,NULL),
	(6328,'海西蒙古族藏族自治州',63,6,NULL,NULL),
	(6401,'银川市',64,6,NULL,NULL),
	(6402,'石嘴山市',64,6,NULL,NULL),
	(6403,'吴忠市',64,6,NULL,NULL),
	(6404,'固原市',64,6,NULL,NULL),
	(6405,'中卫市',64,6,NULL,NULL),
	(6501,'乌鲁木齐市',65,6,NULL,NULL),
	(6502,'克拉玛依市',65,6,NULL,NULL),
	(6504,'吐鲁番市',65,6,NULL,NULL),
	(6505,'哈密市',65,6,NULL,NULL),
	(6523,'昌吉回族自治州',65,6,NULL,NULL),
	(6527,'博尔塔拉蒙古自治州',65,6,NULL,NULL),
	(6528,'巴音郭楞蒙古自治州',65,6,NULL,NULL),
	(6529,'阿克苏地区',65,6,NULL,NULL),
	(6530,'克孜勒苏柯尔克孜自治州',65,6,NULL,NULL),
	(6531,'喀什地区',65,6,NULL,NULL),
	(6532,'和田地区',65,6,NULL,NULL),
	(6540,'伊犁哈萨克自治州',65,6,NULL,NULL),
	(6542,'塔城地区',65,6,NULL,NULL),
	(6543,'阿勒泰地区',65,6,NULL,NULL),
	(6590,'自治区直辖县级行政区划',65,6,NULL,NULL);

/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clients`;

CREATE TABLE `clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usedName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `gender` enum('Male','Female','Other') COLLATE utf8mb4_unicode_ci DEFAULT 'Other',
  `birthdate` date DEFAULT '1900-01-01',
  `contact_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `citizenship` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'Canada',
  `birth_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `birth_province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `birth_region` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `original_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `original_province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `original_region` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `postal_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `status_in_canada_id` bigint(20) unsigned DEFAULT NULL,
  `chinese_id_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `passport` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `passport_expire` date DEFAULT '1900-01-01',
  `visa_expire` date DEFAULT '1900-01-01',
  `days_until_expire` int(11) DEFAULT '0',
  `uci_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `landing_location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `landing_date` date DEFAULT '1900-01-01',
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `completion_rate` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;

INSERT INTO `clients` (`id`, `name`, `usedName`, `gender`, `birthdate`, `contact_email`, `phone`, `citizenship`, `birth_city`, `birth_province`, `birth_region`, `original_city`, `original_province`, `original_region`, `address`, `city`, `province`, `postal_code`, `status_in_canada_id`, `chinese_id_number`, `passport`, `passport_expire`, `visa_expire`, `days_until_expire`, `uci_number`, `landing_location`, `landing_date`, `photo`, `completion_rate`, `user_id`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'Halie Dibbert',NULL,'Male','1900-01-01','kiehn.katelin@example.org','327-912-4625','中国','East Camden','Virginia','3578 Fritsch Parkway','East Vincentview','California','Cyprus','715 Darrell Crescent','Schuppeburgh','Indiana','79819',NULL,'919736100','CN8077012','2020-11-05','2020-05-02',211,'555943391','Toronto, ON','2015-05-25','','85',26,'2019-09-30 22:17:04','2019-10-11 15:13:14','2019-10-03 14:22:54'),
	(2,'Ulises Johnston','','Male','1900-01-01','brady.ledner@example.com','1-541-756-6881 x02043','中国','Janytown','Florida','6777 Yasmeen Parks Suite 516','Cummingsborough','Nebraska','United States Minor Outlying Islands','67669 Luella Shoals','Kautzerview','New York','80806-6100',NULL,'958574130','CN7892819','2020-10-04','2020-05-16',217,'263997041','Toronto, ON','2015-05-24','','85',6,'2019-09-30 22:17:04','2019-10-11 15:22:40',NULL),
	(3,'Mr. Claude Hill DDS','','Male','1900-01-01','minerva92@example.org','925.861.4036','中国','North Dominiquehaven','Michigan','8327 Alejandrin Harbors','New Reed','New Jersey','Antarctica (the territory South of 60 deg S)','101 Nader Cliff','North Jayce','Washington','52321-9111',NULL,'133222675','CN6504230','2020-10-23','2020-05-15',216,'386567406','Toronto, ON','2015-05-29','','85',9,'2019-09-30 22:17:04','2019-10-11 15:22:40',NULL),
	(4,'Arlene Stark','','Male','1900-01-01','ritchie.morgan@example.com','1-228-498-0811','中国','North Garrettburgh','Ohio','291 Cummerata Coves Apt. 620','North Adell','Hawaii','Bulgaria','419 Ryan Islands Apt. 226','East Marlee','West Virginia','49592',NULL,'148559181','CN8999364','2020-12-10','2020-05-25',226,'652561305','Toronto, ON','2015-05-10','','85',6,'2019-09-30 22:17:04','2019-10-11 15:22:40',NULL),
	(5,'Novella Durgan','','Male','1900-01-01','mcglynn.gavin@example.org','1-871-630-8745 x167','中国','Felipaburgh','Texas','52758 Jakubowski Walks','Port Dashawn','New Hampshire','Mauritius','62319 Auer Plains','Lake Leonie','Nevada','18428',NULL,'874773567','CN8317393','2020-12-03','2020-05-21',222,'933397788','Toronto, ON','2015-05-06','','85',4,'2019-09-30 22:17:04','2019-10-11 15:22:40',NULL),
	(6,'Marcelino Ortiz MD','','Male','1900-01-01','skoch@example.com','787-742-9910 x0945','中国','Leorastad','Colorado','698 Abernathy Well','East Larissaport','Idaho','Poland','87611 Cornelius Lane','Juanitafurt','Wyoming','82779-7171',NULL,'380301876','CN1846119','2020-11-22','2020-05-18',219,'980495797','Toronto, ON','2015-05-18','','85',18,'2019-09-30 22:17:04','2019-10-11 15:22:40',NULL),
	(7,'Letha Howell IV','','Male','1900-01-01','clara84@example.com','774-917-2993 x8512','中国','Port Nicholeburgh','District of Columbia','98535 Corkery Mountain Apt. 967','Jarodland','Oregon','Yemen','38460 Hyatt Throughway','South Walkerville','Arkansas','05362-0882',NULL,'451931518','CN8549279','2020-10-15','2020-05-20',221,'705612303','Toronto, ON','2015-05-08','','85',4,'2019-09-30 22:17:04','2019-10-11 15:22:40',NULL),
	(8,'Prof. Trevion Goldner DVM','','Male','1900-01-01','kreiger.jewel@example.net','646.812.5790 x5869','中国','North Arichester','Florida','584 Kassulke Tunnel Apt. 301','Port Kolbyhaven','Wisconsin','Cote d\'Ivoire','878 Schinner Shoal','Zemlakmouth','North Dakota','14021',NULL,'315289223','CN9870244','2020-11-12','2020-05-09',210,'970214583','Toronto, ON','2015-05-04','','85',14,'2019-09-30 22:17:04','2019-10-11 15:22:40',NULL),
	(9,'Ursula Treutel','','Male','1900-01-01','armando.bartell@example.net','647.813.9510 x8150','中国','New Kearabury','Wisconsin','721 Estrella Village','Leannonberg','South Dakota','United States Minor Outlying Islands','6792 Lenna Mount Apt. 375','West Kearaborough','Virginia','29757-8807',NULL,'387742010','CN8743104','2020-11-28','2020-05-10',219,'529726658','Toronto, ON','2015-05-17','','26',13,'2019-09-30 22:17:04','2019-10-03 14:27:16',NULL),
	(10,'Mrs. Palma Witting','','Male','1900-01-01','deckow.aliya@example.org','476.890.4567','中国','South Kraig','New Jersey','8656 Hagenes Mission Suite 790','Omaritown','Massachusetts','Zambia','349 Koch Lake','Okunevahaven','Oklahoma','24035',NULL,'898195854','CN2834465','2020-11-30','2020-05-01',202,'812479802','Toronto, ON','2015-05-06','','85',9,'2019-09-30 22:17:04','2019-10-11 15:22:40',NULL),
	(11,'Mrs. Eleonore Frami DDS','','Male','1900-01-01','joanie.thompson@example.net','879-365-8615 x890','中国','East Elbertshire','Tennessee','5921 Javon Corners Apt. 352','Ottisbury','District of Columbia','Brazil','1731 Rudy Trace Suite 308','Johnsonland','Nevada','84001-0234',NULL,'885915566','CN3420531','2020-12-13','2020-05-05',206,'819759864','Toronto, ON','2015-05-21','','85',10,'2019-09-30 22:17:04','2019-10-11 15:22:40',NULL),
	(12,'Anjali Schiller','','Male','1900-01-01','cloyd13@example.net','+1-356-715-4460','中国','Zemlakbury','Arizona','638 Etha Common Suite 497','West Kayleigh','Hawaii','Swaziland','507 Joshua Rapids','Goldenberg','South Dakota','89797',NULL,'701331420','CN1428911','2020-10-26','2020-05-04',205,'616829169','Toronto, ON','2015-05-10','','85',7,'2019-09-30 22:17:04','2019-10-11 15:22:41',NULL),
	(13,'Vaughn McGlynn','','Male','1900-01-01','mathias09@example.com','326-687-1712','中国','New Aniya','Alaska','41292 Conn Springs','Carliestad','Rhode Island','Bhutan','6053 Ima Locks','South Ryder','Nebraska','02763',NULL,'184862896','CN8462181','2020-12-20','2020-05-02',203,'665405618','Toronto, ON','2015-05-14','','85',1,'2019-09-30 22:17:04','2019-10-11 15:29:39',NULL),
	(14,'Mrs. Lacy Hyatt V','','Male','1900-01-01','mitchell.lonie@example.org','1-451-964-4429','中国','Schmittfurt','North Carolina','185 Christiansen Orchard Apt. 981','Port Haileehaven','Oklahoma','Guatemala','8173 Donnelly Harbor Suite 056','West Abelardo','Washington','77382',NULL,'485434926','CN3837962','2020-10-10','2020-05-26',234,'322303852','Toronto, ON','2015-05-08','','52',16,'2019-09-30 22:17:04','2019-10-04 12:36:55',NULL),
	(15,'Jonathan Lubowitz','','Male','1900-01-01','lupe.ziemann@example.org','795-219-0489 x046','中国','Auertown','New Jersey','6834 Grace Springs','Mortonland','Illinois','Morocco','279 Adah Glens','Rosaleestad','Washington','10888',NULL,'352435325','CN8857471','2020-10-16','2020-05-09',210,'717222267','Toronto, ON','2015-05-07','','85',19,'2019-09-30 22:17:04','2019-10-11 15:22:41',NULL),
	(16,'Katlyn Huels','','Male','1900-01-01','dmaggio@example.com','+1-383-312-3735','中国','Thielberg','Arkansas','27304 Bartoletti Station','Port Sunny','Arizona','Ukraine','257 Ewald Lights','West Stanley','Arizona','95522',NULL,'627332968','CN4076915','2020-10-14','2020-05-17',226,'691106136','Toronto, ON','2015-05-25','','85',12,'2019-09-30 22:17:04','2019-10-03 14:22:52','2019-10-03 14:22:52'),
	(17,'Miss Kaitlyn McCullough','','Male','1900-01-01','carroll.maida@example.net','+1-729-805-2760','中国','Leotown','Illinois','81063 Meaghan Springs Suite 864','New Reuben','Pennsylvania','Canada','284 Merlin Inlet Apt. 273','Medhurstburgh','New Mexico','06502',NULL,'432003532','CN6169387','2020-12-18','2020-05-12',213,'581709012','Toronto, ON','2015-05-07','','85',10,'2019-09-30 22:17:04','2019-10-11 15:22:41',NULL),
	(18,'Mozell Daniel','','Male','1900-01-01','zdeckow@example.org','473.791.6487','中国','Rockyfort','Pennsylvania','9955 Hodkiewicz Crossing','Bellestad','Maine','Fiji','8980 Jacobs Crescent Suite 995','Shyannhaven','Wyoming','85245',NULL,'840404738','CN9815456','2020-10-13','2020-05-21',222,'989417261','Toronto, ON','2015-05-23','','85',15,'2019-09-30 22:17:04','2019-10-11 15:22:41',NULL),
	(19,'Benton Lindgren','','Male','1900-01-01','mills.amiya@example.com','1-820-690-5312','中国','South Edaburgh','Louisiana','857 Helen Isle','Jenkinsview','Wyoming','Maldives','286 Darius Path Suite 171','Port Fabiolafurt','Nevada','14849-6435',NULL,'203722531','CN2831872','2020-10-15','2020-05-10',211,'301542503','Toronto, ON','2015-05-16','','85',20,'2019-09-30 22:17:04','2019-10-11 15:22:41',NULL),
	(20,'Prof. Cara Witting II','','Male','1900-01-01','aida.effertz@example.org','521.553.2595 x0007','中国','Mayermouth','Utah','12678 Joan Field','Marvinshire','Georgia','Saint Lucia','88979 Kiehn Groves Apt. 183','Bernhardbury','North Carolina','71482-4987',NULL,'264892694','CN2131975','2020-12-16','2020-05-08',209,'831958022','Toronto, ON','2015-05-08','','85',7,'2019-09-30 22:17:04','2019-10-11 15:22:41',NULL),
	(21,'Saul Gorczany','','Male','1900-01-01','dena88@example.net','1-332-933-7419 x560','中国','Priscillatown','Hawaii','242 Crooks Manor','Schroedershire','Indiana','Latvia','276 Larson Springs','Maximeville','Montana','90639',NULL,'405082099','CN2328501','2020-10-19','2020-05-30',231,'143533255','Toronto, ON','2015-05-11','','85',3,'2019-09-30 22:17:04','2019-10-11 15:27:54',NULL),
	(22,'Ms. Margarete White II','','Male','1900-01-01','jesse03@example.org','702-910-4312','中国','Carliside','Texas','4597 Micheal Plain Apt. 513','New Jaredburgh','District of Columbia','Rwanda','234 Botsford Court','Denesikfort','New Jersey','22174-1078',NULL,'992939900','CN1815578','2020-10-11','2020-05-12',213,'400407603','Toronto, ON','2015-05-26','','85',7,'2019-09-30 22:17:04','2019-10-11 15:22:41',NULL),
	(23,'Edmund Herman II','','Male','1900-01-01','toby80@example.org','+1.374.337.4319','中国','Waelchifort','Arkansas','1368 Marks Corner','VonRuedenborough','Pennsylvania','Dominica','898 Larkin Mills Suite 220','Lake Odieport','North Dakota','03196',NULL,'911042253','CN7653515','2020-10-24','2020-05-29',230,'731840406','Toronto, ON','2015-05-23','','85',6,'2019-09-30 22:17:04','2019-10-11 15:22:41',NULL),
	(24,'Cassandra Schneider','','Male','1900-01-01','mconn@example.net','401.718.7610','中国','Colemantown','New Hampshire','61261 Koby Path Apt. 051','Gislasonfurt','Rhode Island','France','692 Rebeka Row Apt. 180','North Ashton','Hawaii','83159-8670',NULL,'155637371','CN2529787','2020-11-08','2020-05-16',217,'465028003','Toronto, ON','2015-05-05','','85',11,'2019-09-30 22:17:04','2019-10-11 15:22:41',NULL),
	(25,'Irwin Beer','','Male','1900-01-01','jeffrey74@example.net','860.423.1921 x669','中国','Ernsershire','Utah','32204 Mustafa Spring Suite 170','Armandland','Indiana','Qatar','83201 Reynolds Mountain Suite 717','South Amelie','Montana','76682',NULL,'659841711','CN3163630','2020-10-19','2020-05-20',221,'942151195','Toronto, ON','2015-05-05','','85',20,'2019-09-30 22:17:04','2019-10-11 15:22:41',NULL),
	(26,'Jaren Zboncak','','Male','1900-01-01','aisha96@example.org','269-435-9118 x04675','中国','Margieport','Maryland','655 Roob Bypass Suite 019','Labadiestad','Oregon','Sri Lanka','422 Mandy Drives','South Alexanestad','Kansas','08454',NULL,'906439552','CN6354575','2020-12-11','2020-05-05',206,'827970141','Toronto, ON','2015-05-07','','85',20,'2019-09-30 22:17:04','2019-10-11 15:22:41',NULL),
	(27,'Myra Hintz V','','Male','1900-01-01','vrowe@example.org','829-741-7442 x51964','中国','South Willy','Maine','25504 Katlynn Centers','Hilpertmouth','New Mexico','Ethiopia','74889 Audie Light','Lake Tristinport','Maine','03898-3821',NULL,'460074225','CN6054133','2020-11-07','2020-05-16',217,'562410853','Toronto, ON','2015-05-03','','85',14,'2019-09-30 22:17:04','2019-10-11 15:22:41',NULL),
	(28,'Geovany Yost','','Male','1900-01-01','weston.kuhn@example.org','+1-640-361-8523','中国','New Caitlyn','Wisconsin','498 Little Common','Maureenstad','Oklahoma','Trinidad and Tobago','46288 Welch Hill Suite 041','East Kyra','West Virginia','83576-6776',NULL,'878721713','CN5116746','2020-12-04','2020-05-11',212,'810365962','Toronto, ON','2015-05-07','','85',2,'2019-09-30 22:17:04','2019-10-11 15:27:54',NULL),
	(29,'Demond Towne','','Male','1900-01-01','cerdman@example.com','+18915787708','中国','Zemlakport','Louisiana','8162 Durgan Rapid','East Toniview','Oregon','Guinea','285 Arne Shoals Suite 370','Schulistview','Oregon','39150',NULL,'212199233','CN1731272','2020-10-30','2020-05-06',207,'987896625','Toronto, ON','2015-05-09','','85',18,'2019-09-30 22:17:04','2019-10-11 15:22:41',NULL),
	(30,'Marcelle Cormier III','','Male','1900-01-01','adriana54@example.org','(936) 630-5207','中国','Fadelborough','Oregon','844 Schuster Locks','West Gardner','Montana','Honduras','15018 Strosin Hills','Antwonland','Michigan','85002-5121',NULL,'504947034','CN8151250','2020-11-13','2020-05-18',219,'856819745','Toronto, ON','2015-05-19','','85',5,'2019-09-30 22:17:04','2019-10-11 15:22:41',NULL),
	(31,'Ruth Simonis Sr.','','Male','1900-01-01','abbey.labadie@example.org','765-308-8564','中国','Hubertborough','Arkansas','6063 McClure Path Suite 676','Ebertfurt','Texas','Netherlands','587 Mariane Square Apt. 884','Jacobifort','Washington','65393-1334',NULL,'353430261','CN8565087','2020-12-22','2020-05-25',0,'195572517','Toronto, ON','2015-05-17','','47',15,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(32,'Dr. Callie Runolfsson DVM','','Male','1900-01-01','auer.alfonzo@example.net','1-562-498-2320 x4701','中国','Berneicestad','Maryland','671 Welch Circles Apt. 317','Lake Keely','Illinois','Serbia','8110 Micheal Spurs Apt. 063','North Wiltonside','Rhode Island','88612-9062',NULL,'435334810','CN2657013','2020-10-21','2020-05-03',0,'137136969','Toronto, ON','2015-05-01','','0',5,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(33,'Joanne Roob DVM','','Male','1900-01-01','christophe.walker@example.net','1-924-250-4361','中国','West Schuyler','Arizona','6768 Grady Bypass','Lenoraborough','Rhode Island','Kiribati','2343 McDermott Track','South Blairland','New Hampshire','19439-3211',NULL,'198812617','CN2623758','2020-10-30','2020-05-21',0,'403213737','Toronto, ON','2015-05-30','','34',6,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(34,'Verlie Langosh','','Male','1900-01-01','maggie.tremblay@example.org','(526) 898-5012 x361','中国','Millerport','Rhode Island','55416 Forrest Shores Apt. 088','South Alexane','Tennessee','Qatar','77250 Nathanial Locks Suite 919','Lenorafurt','Virginia','16597-7090',NULL,'181498731','CN8752317','2020-10-06','2020-05-16',0,'174558114','Toronto, ON','2015-05-06','','87',7,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(35,'Shana Kuvalis','','Male','1900-01-01','cydney.lebsack@example.org','360.390.6128','中国','West Golda','Georgia','3896 Braun Course','Narcisoborough','Nevada','Panama','75047 Skiles Center Apt. 736','Lake Leoview','Maine','89909',NULL,'284093071','CN8776826','2020-12-25','2020-05-05',0,'683271697','Toronto, ON','2015-05-28','','54',20,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(36,'Abelardo Hansen Sr.','','Male','1900-01-01','jailyn48@example.org','(315) 427-2883','中国','North Joaquinbury','Colorado','84388 Nienow Cliff Suite 389','Mauriceberg','Arkansas','Argentina','84654 Gusikowski Plains Suite 591','Streichberg','Indiana','50301-4199',NULL,'626041491','CN7954201','2020-12-03','2020-05-20',0,'434956385','Toronto, ON','2015-05-13','','15',11,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(37,'Moriah Zboncak Jr.','','Male','1900-01-01','elmer46@example.org','+18294427777','中国','Alvamouth','Connecticut','9895 Heidenreich Tunnel Apt. 679','East Sigmund','Nevada','Mexico','161 Mante Track','Adamburgh','New Mexico','30398',NULL,'175766519','CN6349700','2020-10-02','2020-05-30',0,'757684687','Toronto, ON','2015-05-15','','20',4,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(38,'Neil Bednar I','','Male','1900-01-01','nfeil@example.com','(530) 572-4486','中国','West Penelope','Washington','7025 Arjun Plain Suite 916','Thieltown','Minnesota','Belgium','540 Kirlin Vista','East Rebecca','South Dakota','26011',NULL,'444516293','CN4416066','2020-10-17','2020-05-09',0,'234850559','Toronto, ON','2015-05-06','','57',8,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(39,'Oleta Jacobs','','Male','1900-01-01','marvin.padberg@example.net','597.781.7612 x953','中国','Alvatown','Rhode Island','93900 Keegan Views','New Jenniestad','Kentucky','Tajikistan','10398 Estevan Keys','Legrosfurt','California','14088-6486',NULL,'352773135','CN3484730','2020-12-02','2020-05-30',0,'699159329','Toronto, ON','2015-05-10','','32',18,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(40,'Elian Goyette','','Male','1900-01-01','claud64@example.org','302.359.5665 x4064','中国','Rociofurt','Delaware','176 Jakubowski Mall Suite 949','North Lornachester','Delaware','Morocco','22173 Mayert Forge Apt. 279','Cordeliastad','Louisiana','85406-7439',NULL,'727427643','CN1647341','2020-12-15','2020-05-29',0,'314525653','Toronto, ON','2015-05-12','','89',10,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(41,'Rosalinda Cormier','','Male','1900-01-01','bo.kuhlman@example.net','+1-431-209-4675','中国','South Kara','North Carolina','3581 O\'Hara Station','North Brentberg','Nebraska','Comoros','74642 Christian Mills','South Eliza','South Dakota','60500',NULL,'675942818','CN7341421','2020-12-05','2020-05-25',0,'262143843','Toronto, ON','2015-05-21','','70',17,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(42,'Prof. Joan Bode III','','Male','1900-01-01','francisca.block@example.com','(575) 727-0947','中国','Port Roelview','New York','48714 Armand Lane Apt. 838','Kacitown','Kentucky','Sierra Leone','418 Amina Well','Dariantown','North Dakota','77094',NULL,'823337082','CN9530573','2020-10-29','2020-05-29',0,'315848153','Toronto, ON','2015-05-04','','52',14,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(43,'Peggie Ebert','','Male','1900-01-01','loma97@example.com','963.708.8369 x968','中国','Pamelahaven','Minnesota','552 Becker Row Apt. 320','Corkerymouth','Indiana','South Georgia and the South Sandwich Islands','1218 Dibbert Summit Suite 038','Camdenview','Alabama','13099-6869',NULL,'243878279','CN2983218','2020-11-30','2020-05-03',0,'615347804','Toronto, ON','2015-05-23','','19',15,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(44,'Jasmin Purdy','','Male','1900-01-01','daisha.wintheiser@example.org','410-509-6587','中国','New Lorineburgh','Delaware','354 Sarai Mews','Port Magnus','California','Antarctica (the territory South of 60 deg S)','1502 Glover Stravenue Apt. 588','East Durward','Iowa','23242-5068',NULL,'708241725','CN3282361','2020-12-02','2020-05-25',0,'199581420','Toronto, ON','2015-05-09','','9',15,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(45,'Horace Cronin','','Male','1900-01-01','danial00@example.org','(394) 888-7255 x3538','中国','Lake Kayleigh','Utah','170 Idell Crossing Apt. 981','New Walterport','District of Columbia','Cook Islands','411 Tobin Point','Osbaldotown','Iowa','19888',NULL,'749623008','CN7556698','2020-11-16','2020-05-24',0,'408174586','Toronto, ON','2015-05-07','','34',19,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(46,'Dr. Jerel Renner','','Male','1900-01-01','julian.kulas@example.com','989.920.3519','中国','Port Mara','Ohio','486 Abigale River','Marvinton','Idaho','Bulgaria','30766 Nikolaus Terrace','Hiltonfort','New York','93968-5077',NULL,'712422779','CN3807044','2020-11-01','2020-05-29',0,'275432010','Toronto, ON','2015-05-28','','44',15,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(47,'Mr. Hazel Gerlach DVM','','Male','1900-01-01','adriana56@example.net','(651) 252-6482','中国','East Deja','Florida','6412 Jazmyn Lights Apt. 605','Evelinetown','Arizona','Guadeloupe','99159 Keenan Stravenue','Dollystad','Montana','72298-7960',NULL,'924005687','CN3496548','2020-12-20','2020-05-28',0,'955420049','Toronto, ON','2015-05-17','','87',5,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(48,'Mr. Brandt Ledner','','Male','1900-01-01','nikki21@example.net','(865) 369-7391 x1779','中国','Luettgenton','Kentucky','92046 Daniella Rest','New Ethanview','Florida','Korea','2815 Bernie Drive','Hilarioside','Hawaii','11698-4112',NULL,'538049768','CN4518941','2020-12-06','2020-05-08',0,'841750305','Toronto, ON','2015-05-08','','90',15,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(49,'Prof. Jailyn Kulas MD','','Male','1900-01-01','tking@example.org','1-285-495-2776','中国','New Claud','District of Columbia','5485 Yost Passage','Fordburgh','Michigan','Mali','27407 Alessia Lights','Alejandrinhaven','Colorado','69000',NULL,'983109836','CN7178233','2020-12-20','2020-05-30',0,'737927733','Toronto, ON','2015-05-02','','84',6,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(50,'Mr. Guiseppe Waelchi Sr.','','Male','1900-01-01','tess.shanahan@example.com','693-770-7371','中国','Wiegandland','Maine','781 Kuhn Corners','Stiedemannborough','New Hampshire','United States Virgin Islands','728 McCullough Greens Apt. 740','Port Braulioberg','Rhode Island','52040',NULL,'984374417','CN2416332','2020-12-26','2020-05-02',211,'253605660','Toronto, ON','2015-05-17','','85',17,'2019-09-30 22:17:04','2019-10-03 19:40:10',NULL),
	(51,'Marilie Nienow PhD','','Male','1900-01-01','ostokes@example.net','1-889-687-4792 x9187','中国','Corrineview','Colorado','3802 Kemmer Throughway','Stammville','Iowa','Isle of Man','501 Rebeca Rue','Port Augustus','New Jersey','68656-6493',NULL,'459938402','CN7591731','2020-10-19','2020-05-29',0,'933657260','Toronto, ON','2015-05-02','','34',14,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(52,'Dr. Salma Powlowski','','Male','1900-01-01','austin.nitzsche@example.com','258.536.2311 x86828','中国','Beckerhaven','Missouri','725 Weber Skyway','Haleyland','Idaho','United States Minor Outlying Islands','71958 Thiel Radial Suite 913','Lake Layla','Indiana','80608-3097',NULL,'940378119','CN1469294','2020-11-19','2020-05-18',226,'318881890','Toronto, ON','2015-05-10','','1',16,'2019-09-30 22:17:04','2019-10-04 12:36:55',NULL),
	(53,'Prof. Sister Blanda Jr.','','Male','1900-01-01','hhodkiewicz@example.org','+1 (962) 864-6593','中国','Deliahaven','New Hampshire','3672 Jeremy Forest','New Elfrieda','Virginia','Lithuania','3754 Davis Meadow','New Keatonport','Pennsylvania','43934-0575',NULL,'985655873','CN5477752','2020-12-17','2020-05-15',0,'408224521','Toronto, ON','2015-05-14','','26',11,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(54,'Delia Greenholt','','Male','1900-01-01','xruecker@example.com','580.505.5733 x7974','中国','West Electaland','South Dakota','95864 Wolf Mount','North Allene','Missouri','Slovenia','933 Marvin Hollow','Aurelioton','Wyoming','12818',NULL,'181701845','CN4055701','2020-12-05','2020-05-06',0,'481206500','Toronto, ON','2015-05-11','','23',5,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(55,'Miguel Hackett MD','','Male','1900-01-01','heidi16@example.net','+1.253.548.0178','中国','Port Blancaside','Maine','645 Prohaska Extensions','Chanelleland','Wisconsin','Finland','5685 Brooklyn River','Lake Nikolas','Massachusetts','81757',NULL,'996122783','CN7351349','2020-11-02','2020-05-16',225,'165056848','Toronto, ON','2015-05-19','','85',13,'2019-09-30 22:17:04','2019-10-03 14:27:15',NULL),
	(56,'Krista Powlowski','','Male','1900-01-01','vincenzo.von@example.net','(603) 451-4655','中国','Lake Vernonton','Texas','409 Glennie Pike','Patiencemouth','New York','Azerbaijan','9014 Dach Glen','Port Carleeborough','Mississippi','36860',NULL,'902497564','CN5825615','2020-10-29','2020-05-12',0,'727704385','Toronto, ON','2015-05-19','','81',9,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(57,'Miss Frances Schoen I','','Male','1900-01-01','mellie08@example.org','(479) 658-5054','中国','Lake Isadorehaven','New Mexico','732 Ernser Points Suite 225','North Vallie','New York','Solomon Islands','90187 Jo Isle','Macyland','North Carolina','28939',NULL,'664275220','CN9185165','2020-10-12','2020-05-17',0,'120172017','Toronto, ON','2015-05-12','','41',10,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(58,'Dave Haag','','Male','1900-01-01','jeremie74@example.org','+1 (931) 446-5627','中国','Pamelamouth','Kansas','1521 Hermann Cliff','West Clarabellemouth','Vermont','Vietnam','8595 Jude Park','Lindview','West Virginia','76365',NULL,'281696545','CN2454351','2020-10-14','2020-05-19',228,'306364062','Toronto, ON','2015-05-18','','85',17,'2019-09-30 22:17:04','2019-10-03 19:40:10',NULL),
	(59,'Dr. Jaquan Nicolas Sr.','','Male','1900-01-01','lenora.oconnell@example.org','1-663-359-9489','中国','Schambergerview','New Hampshire','21065 Guadalupe Field Suite 025','Lake Yazminburgh','New Hampshire','South Africa','8425 Louie Well Suite 062','New Nelle','Hawaii','75416',NULL,'529218534','CN9904627','2020-10-01','2020-05-01',0,'215957248','Toronto, ON','2015-05-22','','90',11,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(60,'Terrell Kessler','','Male','1900-01-01','yconroy@example.com','1-992-558-1181 x88586','中国','Schmittbury','Pennsylvania','676 Kirsten Road Apt. 214','North Maddisonburgh','Minnesota','Bolivia','581 Walsh Port','Elveratown','Connecticut','03132',NULL,'312991783','CN9459185','2020-10-19','2020-05-05',214,'607054389','Toronto, ON','2015-05-19','','85',13,'2019-09-30 22:17:04','2019-10-03 14:27:15',NULL),
	(61,'Vernie Christiansen','','Male','1900-01-01','cleo45@example.com','943-583-8022 x6113','中国','Mosheview','South Dakota','3810 Schultz Plains Apt. 453','New Maximillia','Vermont','Nepal','9219 Halvorson Trace','New Maggie','Idaho','78927-2789',NULL,'876802491','CN8868012','2020-11-23','2020-05-13',0,'553638476','Toronto, ON','2015-05-09','','36',11,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(62,'Joana Thiel','','Male','1900-01-01','msanford@example.net','826.403.9679 x11669','中国','Kuhnmouth','Alabama','9271 Boyle Throughway','Kirstinshire','Oklahoma','Italy','971 Emmet Circles Suite 112','Port Tyreekchester','Louisiana','58404',NULL,'444019229','CN7131089','2020-10-18','2020-05-25',0,'415974436','Toronto, ON','2015-05-20','','13',14,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(63,'Ransom Jacobi','','Male','1900-01-01','rippin.lesley@example.org','(349) 579-3789 x84253','中国','New Karsonchester','Alabama','7697 Gibson Garden','North Katlynn','Alaska','Netherlands','704 Katelynn Manors Apt. 366','Runolfssonville','Idaho','78383',NULL,'280841231','CN5005018','2020-12-05','2020-05-16',224,'552582757','Toronto, ON','2015-05-07','','75',16,'2019-09-30 22:17:04','2019-10-04 12:36:55',NULL),
	(64,'Cecilia Hayes','','Male','1900-01-01','mertz.reyna@example.com','478-462-9184 x01989','中国','Gutkowskiside','Kansas','142 Garry Park','Martaberg','New Mexico','Saint Vincent and the Grenadines','7418 Brakus Shore Suite 312','West Keeleyport','Arkansas','60983-5293',NULL,'491140621','CN9193158','2020-12-19','2020-05-10',218,'248842862','Toronto, ON','2015-05-05','','85',16,'2019-09-30 22:17:04','2019-10-04 12:36:55',NULL),
	(65,'Kasey Jacobson','','Male','1900-01-01','pbernier@example.net','301-287-3465','中国','Stokesborough','Oregon','24014 America Plains Suite 112','Dickinsonchester','Florida','Rwanda','112 Jaida Path Suite 530','Metzview','Nebraska','42533',NULL,'268003752','CN2506705','2020-12-14','2020-05-17',0,'711427640','Toronto, ON','2015-05-07','','58',15,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(66,'Opal Abshire','','Male','1900-01-01','akohler@example.com','236.403.1984','中国','North Wardborough','North Dakota','84097 Amparo Springs Apt. 331','Carleytown','Arkansas','Turks and Caicos Islands','869 Runte Freeway Suite 758','North Gabeborough','Wisconsin','80154-1895',NULL,'564601535','CN4765863','2020-12-04','2020-05-11',0,'543432320','Toronto, ON','2015-05-30','','90',9,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(67,'Al Walter','','Male','1900-01-01','shaun.howe@example.com','1-757-482-3567','中国','Lake Lyric','Connecticut','55626 Freida Corners','East Sophie','Mississippi','Saint Barthelemy','525 Blanda Centers','Lake Ayanaville','Montana','15208',NULL,'686001883','CN4099382','2020-11-25','2020-05-07',0,'195684402','Toronto, ON','2015-05-22','','74',8,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(68,'Moses Koch','','Male','1900-01-01','johnson.kamille@example.com','502-676-1595','中国','McGlynnfort','Kentucky','16635 Wilkinson Hill','Lake Burnice','Missouri','Liberia','91568 Miller Keys','McCulloughside','California','97973',NULL,'715487667','CN5119487','2020-12-14','2020-05-28',0,'735988242','Toronto, ON','2015-05-06','','66',4,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(69,'Mikel Schamberger','','Male','1900-01-01','jdibbert@example.net','+1-542-893-0190','中国','Rueckerbury','Connecticut','4251 Beatty Wells Apt. 205','Evansside','California','Nepal','7354 Dorcas Falls','West Pearline','Pennsylvania','35650-7970',NULL,'383207563','CN8916883','2020-12-24','2020-05-20',0,'956744799','Toronto, ON','2015-05-12','','6',8,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(70,'Prof. Aric Grimes','','Male','1900-01-01','hhartmann@example.com','1-526-870-9502','中国','New Jackelinehaven','Illinois','99725 Collier Estate','Boehmburgh','South Dakota','Egypt','50420 Prince Corner Suite 997','New Muriel','Oklahoma','99271',NULL,'663547875','CN2620765','2020-11-23','2020-05-13',0,'744095891','Toronto, ON','2015-05-08','','86',15,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(71,'Nestor Lehner','','Male','1900-01-01','jameson.goyette@example.net','1-523-472-7346 x697','中国','Lake Jessyview','Connecticut','3910 Kozey Squares Apt. 352','North Roman','Rhode Island','Christmas Island','647 Aufderhar Rapid Apt. 128','Mayertport','Maryland','20829-6781',NULL,'520262393','CN3605522','2020-11-19','2020-05-22',0,'340390451','Toronto, ON','2015-05-03','','37',18,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(72,'Pearlie Ortiz','','Male','1900-01-01','mara64@example.net','+1 (306) 766-1723','中国','New Jazmynton','Colorado','19765 Hilpert Islands','Erikville','Vermont','Austria','93615 Hamill Streets Apt. 309','Haskellview','Washington','22820-9428',NULL,'154025854','CN8466767','2020-12-08','2020-05-28',237,'540223614','Toronto, ON','2015-05-21','','85',18,'2019-09-30 22:17:04','2019-10-03 19:44:55',NULL),
	(73,'Bradley Paucek','','Male','1900-01-01','emard.danial@example.org','+1 (527) 792-6165','中国','North Genovevachester','District of Columbia','4436 Buckridge Rapid Apt. 766','Boyerview','Utah','Faroe Islands','793 Tess Branch Suite 703','Matildamouth','Maine','84025',NULL,'179026646','CN4808077','2020-10-05','2020-05-11',0,'244686146','Toronto, ON','2015-05-11','','54',10,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(74,'Jacquelyn Ziemann','','Male','1900-01-01','alexandrine90@example.net','942.483.0585 x64329','中国','Lake Titus','Indiana','278 Leffler Ferry Apt. 609','West Buddy','Nevada','Bahrain','326 Terry Crossroad','Lake Opalmouth','Arkansas','37467-8354',NULL,'879287943','CN8241802','2020-11-03','2020-05-07',0,'791703683','Toronto, ON','2015-05-09','','80',10,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(75,'Destany Mante','','Male','1900-01-01','wemmerich@example.org','+1 (558) 554-9023','中国','Thielfort','Kansas','89758 Braulio Station Apt. 299','Patview','Alabama','Niue','84556 Jones Expressway','Clementinemouth','Rhode Island','30668',NULL,'847716820','CN6192533','2020-11-10','2020-05-11',219,'904255237','Toronto, ON','2015-05-26','','85',16,'2019-09-30 22:17:04','2019-10-04 12:36:55',NULL),
	(76,'Prof. Douglas Ondricka','','Male','1900-01-01','fkling@example.com','+1-864-579-7015','中国','Odellshire','New York','834 Hortense Parkway Suite 508','West Juliehaven','Maine','French Polynesia','45375 Travon Village','Benville','Kansas','11689-0889',NULL,'708507308','CN2092503','2020-10-23','2020-05-05',214,'295475674','Toronto, ON','2015-05-26','','85',18,'2019-09-30 22:17:04','2019-10-03 19:44:55',NULL),
	(77,'Kaylah Williamson','','Male','1900-01-01','dax.veum@example.net','1-207-661-7290 x71118','中国','Evalynhaven','Vermont','9342 Hartmann Ranch Suite 427','Lake Rylan','Mississippi','Slovakia (Slovak Republic)','2312 Conroy Trafficway','Augustinemouth','North Dakota','70713-0322',NULL,'858288209','CN2369779','2020-12-29','2020-05-12',0,'833765754','Toronto, ON','2015-05-02','','70',5,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(78,'George Ritchie','','Male','1900-01-01','ibauch@example.net','1-447-639-0762','中国','Granvilleburgh','North Dakota','425 Quinton Rue','Port Angelfort','Alaska','Barbados','4783 Koelpin Meadows','East Jalen','Washington','41751',NULL,'265091431','CN9766614','2020-11-11','2020-05-04',205,'770820350','Toronto, ON','2015-05-25','','85',2,'2019-09-30 22:17:04','2019-10-11 15:27:54',NULL),
	(79,'Harley Stiedemann','','Male','1900-01-01','andrew73@example.net','829-908-8588 x2039','中国','Annabellberg','New Hampshire','368 Odie Curve','New Lucienne','Mississippi','Nauru','702 Albert Square Suite 249','Gerholdland','Iowa','77192',NULL,'149695634','CN5909268','2020-10-04','2020-05-07',0,'157591220','Toronto, ON','2015-05-12','','2',11,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(80,'Lacey King','','Male','1900-01-01','ekirlin@example.com','(619) 682-8511','中国','Hamillton','Wisconsin','4721 Mayert Forge','Hermistonview','Nevada','Niue','6473 Kulas Pass','West Pasqualeshire','Washington','84648',NULL,'921665453','CN5242505','2020-11-03','2020-05-23',224,'567193235','Toronto, ON','2015-05-07','','85',3,'2019-09-30 22:17:04','2019-10-11 15:27:54',NULL),
	(81,'Milan McKenzie PhD','','Male','1900-01-01','pmckenzie@example.net','558-748-6486 x6306','中国','Jessycaburgh','Alabama','43938 Rhoda Plaza Apt. 041','Bergemouth','Michigan','Faroe Islands','6701 Hickle Run','East Lesly','Hawaii','71766',NULL,'458451837','CN4335821','2020-10-10','2020-05-25',0,'439328450','Toronto, ON','2015-05-03','','48',20,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(82,'Freida Rodriguez','','Male','1900-01-01','noemy19@example.net','+1-407-366-7057','中国','West Patience','Kansas','85683 Wunsch Hill','North Alshire','Wyoming','South Africa','4838 Upton Turnpike Apt. 719','Emmerichbury','Missouri','30505-4660',NULL,'472224577','CN5027116','2020-12-19','2020-05-01',0,'712289462','Toronto, ON','2015-05-16','','73',4,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(83,'Georgianna Walter','','Male','1900-01-01','shanon81@example.net','365-229-2716 x4363','中国','East Rashadhaven','Kansas','246 Orin Crossing','Rodolfotown','Wisconsin','Egypt','191 Toni Ports Suite 860','Bergnaumside','Michigan','26894-5019',NULL,'779239857','CN9769792','2020-10-28','2020-05-10',211,'987399034','Toronto, ON','2015-05-22','','85',1,'2019-09-30 22:17:04','2019-10-11 15:29:39',NULL),
	(84,'Prof. Johnson Hill V','','Male','1900-01-01','keebler.carmella@example.com','+1 (609) 792-6987','中国','Lake Stephanie','New Mexico','215 Cremin Squares','East Stephonhaven','Tennessee','Bolivia','2144 Schmeler Rapid','Lake Stephaniabury','West Virginia','31471-0559',NULL,'414022299','CN7700193','2020-11-08','2020-05-15',224,'352003049','Toronto, ON','2015-05-14','','85',18,'2019-09-30 22:17:04','2019-10-03 19:44:55',NULL),
	(85,'Taya Schulist III','','Male','1900-01-01','kimberly.sawayn@example.com','(637) 862-7571 x6063','中国','Johathantown','Massachusetts','49334 Bergstrom Meadow Apt. 395','North Milo','Washington','Greenland','75026 Theresa Burgs Apt. 876','New Jerad','New Mexico','35134-5838',NULL,'138056955','CN2048872','2020-11-24','2020-05-18',0,'492567242','Toronto, ON','2015-05-20','','73',7,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(86,'Rosa Franecki','','Male','1900-01-01','uerdman@example.net','375.501.9911 x36601','中国','Adellaport','Minnesota','64742 Dora Plains Suite 785','West Elbert','North Dakota','Algeria','66991 Smitham Overpass Suite 358','Kuhnton','Rhode Island','43484-2633',NULL,'268509819','CN1605038','2020-12-20','2020-05-19',220,'966282512','Toronto, ON','2015-05-30','','85',1,'2019-09-30 22:17:04','2019-10-11 15:29:39',NULL),
	(87,'Vance Rodriguez','','Male','1900-01-01','treutel.clint@example.net','234.679.4773 x3067','中国','North Harryberg','New York','265 Leffler Plaza','New Eloy','Michigan','Austria','10954 Cheyenne Passage','Rosaleeport','Illinois','87145',NULL,'548949204','CN9136302','2020-12-11','2020-05-23',0,'483892994','Toronto, ON','2015-05-21','','69',14,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(88,'Kacie Jast','','Male','1900-01-01','gzieme@example.net','874-821-5785 x78366','中国','North Efrain','Colorado','549 Bartholome Ville','Runolfsdottirberg','New Jersey','Bangladesh','591 Ruthe Turnpike','South Lulaland','New Hampshire','89812',NULL,'670547169','CN7884226','2020-10-12','2020-05-25',234,'453890217','Toronto, ON','2015-05-07','','85',18,'2019-09-30 22:17:04','2019-10-03 19:44:55',NULL),
	(89,'Mrs. Vita Quitzon','','Male','1900-01-01','boreilly@example.net','226-336-9384','中国','East Douglas','North Carolina','56099 Eryn Lake','Theafurt','North Dakota','Grenada','807 Kunze Prairie','North Abagail','Arkansas','53430-4573',NULL,'394563635','CN6617952','2020-11-27','2020-05-02',0,'138797864','Toronto, ON','2015-05-26','','73',19,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(90,'Jolie Nolan','','Male','1900-01-01','qpouros@example.org','483.275.0430 x90948','中国','Port Selina','New Jersey','9752 Parisian Lock','Skilesview','Connecticut','Oman','3743 Baby Keys Apt. 293','Farrellbury','Nevada','01969-1534',NULL,'939511634','CN8664113','2020-10-21','2020-05-23',224,'356275103','Toronto, ON','2015-05-26','','85',19,'2019-09-30 22:17:04','2019-10-11 15:27:03',NULL),
	(91,'Santa Barrows II','','Male','1900-01-01','uzboncak@example.net','1-541-515-0387 x49978','中国','Stephonshire','Utah','98047 Lockman Plains','Wilfordstad','Hawaii','Panama','64635 Goyette Crescent','Cesarberg','Ohio','19125-8807',NULL,'775978456','CN5755360','2020-12-21','2020-05-23',0,'274078265','Toronto, ON','2015-05-03','','32',20,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(92,'Prof. Raoul Schaden Jr.','','Male','1900-01-01','percival66@example.org','324-977-0485 x87636','中国','Adelbertshire','Wisconsin','7129 Wilber Mills','Jettton','Wisconsin','Netherlands','33717 Ortiz Corners Suite 294','Maeveborough','Maryland','89569-9939',NULL,'519300491','CN9016707','2020-11-11','2020-05-14',223,'680431067','Toronto, ON','2015-05-26','','85',13,'2019-09-30 22:17:04','2019-10-03 14:27:15',NULL),
	(93,'Perry Champlin','','Male','1900-01-01','adalberto.mosciski@example.org','564.443.5844 x957','中国','Hermanmouth','North Dakota','499 Edyth Trace','West Vernie','Alaska','Palau','85770 Aubrey Manor Suite 687','Schambergershire','Rhode Island','07332-0785',NULL,'647349760','CN6107361','2020-12-15','2020-05-20',0,'687809863','Toronto, ON','2015-05-28','','1',4,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(94,'Vita Bailey','','Male','1900-01-01','jacobi.clara@example.org','1-268-233-5656','中国','Peterborough','Delaware','53112 Rudolph Ranch','East Easter','Maryland','Gambia','41845 Ledner Junction Apt. 587','Lake Madelynn','North Carolina','31742',NULL,'240312368','CN6852457','2020-12-17','2020-05-05',0,'513841529','Toronto, ON','2015-05-26','','64',7,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(95,'Ova Bogan III','','Male','1900-01-01','colt.goldner@example.net','1-489-406-0093','中国','South Marionbury','Ohio','6112 Green Corners','Tabithastad','Utah','Azerbaijan','766 Ryan Bypass Suite 897','Darrickview','West Virginia','07332-4210',NULL,'715129949','CN5853094','2020-11-18','2020-05-23',224,'222805975','Toronto, ON','2015-05-13','','85',3,'2019-09-30 22:17:04','2019-10-11 15:27:54',NULL),
	(96,'Euna Hermann','','Male','1900-01-01','gpredovic@example.net','925.467.0689 x475','中国','West Wilber','Minnesota','528 Hazel Circles Apt. 657','Port Melissaville','Utah','New Caledonia','252 Stacey Mountains','South Minnieburgh','Kansas','26982',NULL,'985753056','CN5796904','2020-12-12','2020-05-10',0,'404708981','Toronto, ON','2015-05-17','','30',19,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(97,'Ms. Lenna Mayert II','','Male','1900-01-01','britney00@example.net','486-667-4180','中国','Kihnton','Illinois','7960 Daugherty Well Apt. 021','East Shea','Hawaii','Cambodia','4062 Elenor Spurs','Port Mariahchester','Arizona','51558-7535',NULL,'227005733','CN9571729','2020-12-03','2020-05-29',0,'267890450','Toronto, ON','2015-05-24','','19',15,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(98,'Delpha Kub','','Male','1900-01-01','jazmyn.kling@example.com','901-552-3077 x0790','中国','Everettefort','New Hampshire','98341 Boyle Corner','O\'Keefestad','Colorado','Somalia','6073 Wuckert Bypass','Port Missourifort','Massachusetts','96124',NULL,'630039391','CN8940823','2020-10-06','2020-05-24',232,'587083061','Toronto, ON','2015-05-20','','85',16,'2019-09-30 22:17:04','2019-10-04 12:36:55',NULL),
	(99,'Letitia Nader IV','','Male','1900-01-01','pollich.kiara@example.com','358.309.7845 x688','中国','Nienowhaven','Alabama','679 Mohr Port Suite 442','Naderville','Arkansas','Mayotte','691 Dean Street Suite 510','East Neil','Montana','47556',NULL,'428169731','CN3210576','2020-11-18','2020-05-09',0,'994446230','Toronto, ON','2015-05-22','','8',20,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(100,'Dr. Alvis Conroy Sr.','','Male','1900-01-01','ernie78@example.org','+1-721-594-1275','中国','Zoeville','South Carolina','2096 Hermiston Camp Apt. 060','South Jared','Arkansas','Indonesia','44831 Schaden Square','Manteshire','South Dakota','29142-5689',NULL,'282600132','CN5814758','2020-11-03','2020-05-22',0,'248643653','Toronto, ON','2015-05-05','','35',15,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(101,'Dr. Rosetta Bradtke','','Male','1900-01-01','cwunsch@example.org','556.843.2179 x0546','中国','East Rosalindbury','Virginia','8408 Elbert Lane Suite 010','Cummingsfort','Alaska','Uganda','84666 Coleman Spring Suite 985','Port Malvina','Oklahoma','45824-9535',NULL,'239449991','CN4428760','2020-12-13','2020-05-10',0,'838916097','Toronto, ON','2015-05-27','','71',4,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(102,'Orpha Cassin','','Male','1900-01-01','ylehner@example.net','963-423-8872','中国','Eldridgefort','New Hampshire','2937 Winifred Stravenue Suite 739','Port Matteo','Arkansas','Holy See (Vatican City State)','4322 Ervin Keys Apt. 981','New Shayleeburgh','Washington','68696',NULL,'136764336','CN2294377','2020-12-24','2020-05-27',0,'240025660','Toronto, ON','2015-05-07','','56',15,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(103,'Heloise Dare','','Male','1900-01-01','hassie.rosenbaum@example.net','(284) 691-9152','中国','Samsonstad','California','2374 Travis Cliffs','Preciousport','Virginia','Aruba','57060 Amely Square Suite 366','Simonismouth','West Virginia','78199',NULL,'521434183','CN4761456','2020-10-28','2020-05-15',0,'546896605','Toronto, ON','2015-05-04','','7',19,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(104,'Brandy Abbott DVM','','Male','1900-01-01','zboncak.zachariah@example.net','(483) 328-0058 x56883','中国','West Conorhaven','Connecticut','7576 Rath Way','Tessport','Delaware','Isle of Man','378 Raynor Circles Suite 246','Ethelynchester','District of Columbia','99165',NULL,'907456410','CN1567278','2020-12-25','2020-05-22',0,'306568584','Toronto, ON','2015-05-04','','14',11,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(105,'Sheila Stokes','','Male','1900-01-01','ines.torp@example.net','(347) 986-7250','中国','West Serenachester','Mississippi','61696 Wehner Ferry Apt. 771','South Della','District of Columbia','Italy','5977 McLaughlin Run Suite 654','New Margarita','Washington','90483-9995',NULL,'762252809','CN9305906','2020-11-17','2020-05-05',206,'522397610','Toronto, ON','2015-05-02','','85',2,'2019-09-30 22:17:04','2019-10-11 15:27:54',NULL),
	(106,'Dr. Harley Schaefer','','Male','1900-01-01','balistreri.albert@example.net','+1.932.233.5162','中国','Timothyton','Vermont','837 Hintz Freeway Suite 557','Lake Catharinemouth','West Virginia','Venezuela','583 Mann Shoal','South Juniorland','Rhode Island','61889-1106',NULL,'526240141','CN8236608','2020-12-25','2020-05-24',225,'504073705','Toronto, ON','2015-05-05','','85',3,'2019-09-30 22:17:04','2019-10-11 15:27:54',NULL),
	(107,'Frida Thiel V','','Male','1900-01-01','vokeefe@example.org','686-361-5531','中国','Port Ignatius','Alabama','603 Muller Plain Suite 379','Lurlinefort','Missouri','Morocco','20863 Aidan Dam Apt. 781','Annabellberg','Hawaii','27200',NULL,'892104866','CN5973879','2020-12-12','2020-05-11',0,'300728668','Toronto, ON','2015-05-27','','89',4,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(108,'Evans Skiles','','Male','1900-01-01','omante@example.com','(336) 586-7243 x6802','中国','New Albertha','North Carolina','52826 Kuphal Track','Port Lizeth','Delaware','Cook Islands','557 Gorczany Expressway','Moenchester','North Carolina','43234',NULL,'814108470','CN6729363','2020-11-15','2020-05-13',0,'525477152','Toronto, ON','2015-05-08','','87',6,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(109,'Prof. Janice Balistreri Sr.','','Male','1900-01-01','justyn46@example.com','1-439-905-0302 x61762','中国','North Javierfort','District of Columbia','987 Greenfelder Extension','Nienowmouth','Iowa','Iceland','94523 Marlee Centers Apt. 066','Davehaven','West Virginia','87276-4034',NULL,'274923274','CN8053541','2020-10-24','2020-05-07',0,'304377812','Toronto, ON','2015-05-07','','83',9,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(110,'Esperanza Lakin','','Male','1900-01-01','noemie.wyman@example.net','1-338-762-1193','中国','Port Jedidiah','Arizona','361 Marvin Lights','New Dorothy','New Hampshire','Saint Helena','876 McDermott Circle Apt. 406','Cummingsmouth','Montana','51872-1664',NULL,'178148954','CN2127789','2020-11-13','2020-05-09',218,'131052818','Toronto, ON','2015-05-09','','85',17,'2019-09-30 22:17:04','2019-10-03 19:40:10',NULL),
	(111,'Sammie Hickle','','Male','1900-01-01','zelda42@example.org','1-742-758-1643 x3010','中国','South Gordonfort','Kansas','478 Jamal Mountains','South Juanita','New York','India','4727 Tyreek Prairie','Nikkibury','Texas','52144',NULL,'777236336','CN1792230','2020-11-11','2020-05-22',0,'692192232','Toronto, ON','2015-05-09','','13',5,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(112,'Otto Skiles IV','','Male','1900-01-01','gail71@example.org','(310) 716-0912','中国','Corkeryland','New Mexico','6244 Price Union','Savanahton','New Hampshire','Antigua and Barbuda','8079 Dooley Ford','Lizamouth','District of Columbia','23776',NULL,'456952586','CN7752300','2020-10-17','2020-05-04',0,'223463086','Toronto, ON','2015-05-26','','58',9,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(113,'Miss Tina Durgan MD','','Male','1900-01-01','lon.altenwerth@example.org','(450) 200-7644','中国','Boyleside','Pennsylvania','1790 Jerde Prairie Suite 724','Noefurt','Rhode Island','Lithuania','26615 Muller Mountain','Klington','Wyoming','54258-7383',NULL,'191072982','CN9672256','2020-11-29','2020-05-18',0,'255150537','Toronto, ON','2015-05-19','','31',10,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(114,'Clement Nitzsche','','Male','1900-01-01','fprice@example.net','+1-206-661-2547','中国','South Alexander','Mississippi','2269 Magnus Oval Suite 253','Brauliochester','Alaska','Nicaragua','561 Bins Park','Lednerstad','Texas','45671-6683',NULL,'932945163','CN5995432','2020-11-30','2020-05-25',0,'684123669','Toronto, ON','2015-05-23','','13',7,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(115,'Kareem Pfannerstill III','','Male','1900-01-01','ricky.howell@example.net','431-241-7512 x490','中国','East Raegan','Colorado','47991 Adams Fall','East Adrainborough','Minnesota','Fiji','508 Yost Ports Apt. 755','Judgefort','North Dakota','67364-8562',NULL,'830517191','CN8273102','2020-10-30','2020-05-18',219,'805382658','Toronto, ON','2015-05-13','','85',1,'2019-09-30 22:17:04','2019-10-11 15:29:39',NULL),
	(116,'Loyce Buckridge','','Male','1900-01-01','lorine39@example.net','1-245-679-0746 x4187','中国','Lake Rubye','Vermont','634 Ike Neck Suite 854','Alexandriaburgh','North Dakota','Spain','606 Turcotte Island Apt. 585','Lake Amelia','Alabama','76554',NULL,'593738212','CN1513638','2020-12-15','2020-05-28',237,'583369142','Toronto, ON','2015-05-10','','85',12,'2019-09-30 22:17:04','2019-10-03 14:22:48','2019-10-03 14:22:48'),
	(117,'Chad Strosin','','Male','1900-01-01','vena11@example.com','929-737-0242 x23057','中国','Chestermouth','Oregon','3152 Lockman Mews Suite 509','Eddieside','California','Portugal','1762 Christiansen Freeway','Mozellborough','Wyoming','68400',NULL,'352071525','CN9227086','2020-10-09','2020-05-21',0,'232627750','Toronto, ON','2015-05-23','','32',6,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(118,'Mac Baumbach','','Male','1900-01-01','kaitlin29@example.net','1-758-783-5211','中国','Lake Nadiaville','Texas','2549 Stephanie Circles Suite 936','North Keshaunville','Vermont','Uzbekistan','3750 Ewald Islands','Brekkeshire','Arizona','39291-4423',NULL,'456913146','CN9224306','2020-12-05','2020-05-06',207,'751131553','Toronto, ON','2015-05-29','','85',1,'2019-09-30 22:17:04','2019-10-11 15:29:39',NULL),
	(119,'Sean Wunsch','','Male','1900-01-01','aliya.treutel@example.com','450-273-5238 x62600','中国','Savannahmouth','Kentucky','7736 Purdy Oval','Lake Macy','Louisiana','India','49447 Roosevelt Cape','O\'Konhaven','Georgia','84799-9428',NULL,'291374829','CN6413479','2020-12-20','2020-05-19',0,'973509837','Toronto, ON','2015-05-28','','87',11,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(120,'Ms. Nella Maggio','','Male','1900-01-01','ukshlerin@example.net','(893) 978-0716 x01897','中国','Robbburgh','Arizona','55604 Rippin Squares Apt. 529','South Santoshaven','District of Columbia','Tuvalu','3041 Vesta Throughway','Runolfsdottirview','Connecticut','77500',NULL,'670703530','CN7360046','2020-10-11','2020-05-22',223,'252686071','Toronto, ON','2015-05-30','','85',3,'2019-09-30 22:17:04','2019-10-11 15:27:54',NULL),
	(121,'Norwood Romaguera','','Male','1900-01-01','evan97@example.org','991-694-3866 x557','中国','East Kaliside','Montana','28718 Hiram Isle','Schoenshire','Washington','Timor-Leste','7696 Schaden Parkway','Erdmanton','Texas','22444-7332',NULL,'505418536','CN8137398','2020-10-21','2020-05-15',0,'511434416','Toronto, ON','2015-05-15','','66',9,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(122,'Dr. Arjun DuBuque IV','','Male','1900-01-01','schiller.neil@example.net','282.231.3577 x9838','中国','West Pattieborough','North Carolina','772 Sydnie Square','Brandytown','Connecticut','New Caledonia','188 Carroll Falls Apt. 982','North Raquelhaven','West Virginia','21324-6867',NULL,'131250962','CN1621355','2020-12-22','2020-05-01',0,'758012239','Toronto, ON','2015-05-30','','75',19,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(123,'Jailyn Hettinger','','Male','1900-01-01','zankunding@example.org','824-355-4659 x4578','中国','Smithmouth','Kentucky','345 Shanon Motorway','Stefaniebury','Tennessee','Svalbard & Jan Mayen Islands','5933 Ardith Crossing Apt. 333','Lake Ashlynnfort','South Dakota','01205',NULL,'520776368','CN7254544','2020-11-05','2020-05-12',0,'867752286','Toronto, ON','2015-05-19','','56',14,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(124,'Mariam Padberg DDS','','Male','1900-01-01','parisian.eliza@example.net','+1-578-518-8766','中国','Lake Vladimirtown','Michigan','35140 Grady Wall Apt. 073','East Jessica','Michigan','Brazil','2159 Viviane Vista','Port Jeromyburgh','Tennessee','21419-3751',NULL,'676290233','CN5705815','2020-12-08','2020-05-30',0,'307337402','Toronto, ON','2015-05-29','','62',5,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(125,'Marilou Kozey','','Male','1900-01-01','haley.ashton@example.net','+1-648-477-4098','中国','Lake Chanelle','Kentucky','87789 Vandervort Heights','New Tinaville','Delaware','Zimbabwe','3725 Gulgowski Views','Jacksonstad','Georgia','72574-9834',NULL,'986342079','CN6953780','2020-10-25','2020-05-05',0,'487506943','Toronto, ON','2015-05-03','','84',15,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(126,'Jake McDermott MD','','Male','1900-01-01','santiago83@example.com','980.345.4404 x33410','中国','Noahtown','Michigan','235 Block Junctions Apt. 910','South Ariview','Montana','Nepal','2718 Creola Street','Kshlerinport','Nevada','83499',NULL,'800587958','CN1119462','2020-12-14','2020-05-10',219,'487024631','Toronto, ON','2015-05-17','','85',17,'2019-09-30 22:17:04','2019-10-03 19:40:10',NULL),
	(127,'Candelario Bashirian','','Male','1900-01-01','meghan86@example.com','641-337-4188 x76440','中国','Lake Coraliehaven','Delaware','7858 Kutch Skyway','Ressiebury','Minnesota','Anguilla','34816 Earl Coves Apt. 300','Marinaville','Arkansas','62016',NULL,'546941112','CN5917034','2020-10-29','2020-05-09',218,'721599277','Toronto, ON','2015-05-16','','85',15,'2019-09-30 22:17:04','2019-10-03 15:00:48',NULL),
	(128,'Mr. Akeem Harber','','Male','1900-01-01','josie.hackett@example.com','1-580-707-9839 x76503','中国','Roobland','Michigan','426 Verla Freeway','Lake Santa','Oregon','Antarctica (the territory South of 60 deg S)','435 Dooley Spurs Suite 788','Collinsmouth','Minnesota','99513-7118',NULL,'243579478','CN2073879','2020-10-03','2020-05-10',211,'954107029','Toronto, ON','2015-05-01','','85',3,'2019-09-30 22:17:04','2019-10-11 15:27:54',NULL),
	(129,'Ms. Ana West','','Male','1900-01-01','karelle.bartoletti@example.org','369-588-4554 x1936','中国','Westonside','South Dakota','11945 Marquise Mission','New Liza','Arizona','Guinea','912 Spencer Points Apt. 288','Cronaburgh','Arizona','46916-7425',NULL,'140628482','CN2275986','2020-12-27','2020-05-11',0,'307198044','Toronto, ON','2015-05-14','','86',19,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(130,'Titus Kuphal MD','','Male','1900-01-01','jasper59@example.net','(467) 992-1164 x4731','中国','West Albert','Colorado','5756 Antwon Mission','East Nolanfort','Indiana','Montserrat','3329 Cristopher Wells Apt. 138','North Vitoside','Tennessee','99840',NULL,'764935149','CN4899385','2020-12-16','2020-05-18',227,'113010069','Toronto, ON','2015-05-26','','85',19,'2019-09-30 22:17:04','2019-10-03 19:51:20',NULL),
	(131,'Luella Hirthe','','Male','1900-01-01','napoleon.quigley@example.com','+15615907959','中国','Nicklausshire','Delaware','301 Considine Mount','Lake Tania','South Dakota','Saint Pierre and Miquelon','53999 Rosina Trafficway','East Kaialand','South Dakota','69377',NULL,'800914908','CN2132795','2020-12-08','2020-05-18',226,'730876335','Toronto, ON','2015-05-03','','85',16,'2019-09-30 22:17:04','2019-10-04 12:36:55',NULL),
	(132,'Prof. Ignacio Baumbach','','Male','1900-01-01','isidro69@example.net','(873) 296-2078','中国','West Matilda','Colorado','723 Verda Trace Apt. 372','Port Katrinaborough','Massachusetts','Hungary','54230 King Locks Suite 414','Daughertyburgh','Colorado','77267',NULL,'365817502','CN2132361','2020-10-21','2020-05-19',228,'667697576','Toronto, ON','2015-05-09','','85',14,'2019-09-30 22:17:04','2019-10-03 14:36:53',NULL),
	(133,'Mr. Americo Kilback I','','Male','1900-01-01','lucienne73@example.com','1-818-402-1646 x3132','中国','Streichshire','Kansas','27646 Sauer Trace Apt. 842','Hillaryfurt','Massachusetts','Micronesia','5818 Genesis Mall','North Kailee','Georgia','78483-8901',NULL,'805031275','CN8755649','2020-12-05','2020-05-29',0,'947024851','Toronto, ON','2015-05-03','','7',4,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(134,'Kian Becker','','Male','1900-01-01','damore.valentine@example.com','+1-550-490-6308','中国','Reidchester','North Dakota','23358 Huels Centers Apt. 860','Port Guiseppefort','Georgia','Taiwan','9587 Kennedi Lights Suite 429','South Mireyabury','Mississippi','83035-0034',NULL,'173329045','CN2569602','2020-12-10','2020-05-16',0,'578854670','Toronto, ON','2015-05-11','','90',20,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(135,'Ford Abshire II','','Male','1900-01-01','bruce.funk@example.org','1-428-643-4089 x5915','中国','South Jedside','South Carolina','1972 Kautzer Island Apt. 694','North Sydnee','Nevada','Switzerland','33587 Becker Canyon','Mooreshire','Oregon','04831',NULL,'802870407','CN7153486','2020-12-20','2020-05-18',219,'875001403','Toronto, ON','2015-05-20','','85',1,'2019-09-30 22:17:04','2019-10-11 15:29:39',NULL),
	(136,'Cicero Orn','','Male','1900-01-01','ghaley@example.org','(523) 922-6146 x43007','中国','Swaniawskibury','Illinois','67525 Conroy Station Suite 041','O\'Konstad','Oregon','Djibouti','95783 Leann Freeway','Marlonshire','Tennessee','97363',NULL,'838054847','CN9667857','2020-11-03','2020-05-23',0,'114749010','Toronto, ON','2015-05-23','','2',5,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(137,'Miss Kathryne Lockman II','','Male','1900-01-01','akemmer@example.org','414-305-7846 x5453','中国','East Janickstad','Missouri','584 Nader Burg Suite 537','Lake Darian','California','Belarus','5734 Raynor Junction Apt. 930','Lubowitzville','Connecticut','66898',NULL,'319304907','CN4179065','2020-10-14','2020-05-18',0,'551117203','Toronto, ON','2015-05-16','','66',5,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(138,'Sydnee McLaughlin','','Male','1900-01-01','abigail18@example.com','642-616-5568','中国','West Winfieldborough','Delaware','413 Constantin Shoals Apt. 612','Oleton','New Hampshire','Uganda','6445 Aniyah Overpass','East Emmett','Massachusetts','84636-0705',NULL,'826965437','CN9087121','2020-11-16','2020-05-28',237,'767268743','Toronto, ON','2015-05-04','','85',15,'2019-09-30 22:17:04','2019-10-03 15:00:48',NULL),
	(139,'Leo Kunde','','Male','1900-01-01','ofadel@example.com','949.706.7663 x6755','中国','South Jaqueline','Minnesota','749 Vanessa Extensions Suite 392','East Mandy','Maryland','Tuvalu','32848 Willa Park','Willmstown','Kansas','36627',NULL,'477556269','CN5447388','2020-10-04','2020-05-09',0,'704700953','Toronto, ON','2015-05-16','','16',20,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(140,'Orland Schuster','','Male','1900-01-01','tromp.elfrieda@example.org','1-642-672-0438','中国','Mitchellchester','Mississippi','1256 Rohan Trail Suite 423','Ferryville','Florida','Norway','89243 Isaac Mountains','West Bethany','Pennsylvania','85383-4296',NULL,'356450272','CN2042342','2020-11-08','2020-05-21',0,'531694150','Toronto, ON','2015-05-04','','23',9,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(141,'Grant Franecki','','Male','1900-01-01','ivory.doyle@example.net','+1-825-701-8962','中国','Leilaside','Tennessee','1867 Shanie Avenue','North Isacchester','Wyoming','Saint Martin','9724 Hand Inlet Apt. 554','Port Giovannashire','Georgia','75735',NULL,'461634034','CN8717933','2020-12-09','2020-05-03',0,'813534449','Toronto, ON','2015-05-20','','46',3,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(142,'Lucio Hirthe','','Male','1900-01-01','lemke.lane@example.org','492-563-6998 x158','中国','East Virgil','Wisconsin','6791 Miles Dam Apt. 367','Helmerfort','Oregon','Bhutan','57411 Hammes Drive','Turcotteland','Georgia','08021-4185',NULL,'882594791','CN4226759','2020-11-09','2020-05-22',0,'501130105','Toronto, ON','2015-05-01','','29',20,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(143,'Myrtie Larkin','','Male','1900-01-01','noemy.okon@example.net','562.473.9058 x67984','中国','South Verdietown','Oregon','2090 Jones Stream Suite 310','Littlehaven','North Carolina','Guernsey','558 Purdy Drives','Nettiehaven','Louisiana','42660-6146',NULL,'695270991','CN7852926','2020-12-15','2020-05-26',0,'828918646','Toronto, ON','2015-05-04','','4',20,'2019-09-30 22:17:04','2019-09-30 22:17:04',NULL),
	(144,'Hollie Weber','','Male','1900-01-01','kunze.sydni@example.org','+1 (724) 758-2447','中国','Haleighmouth','South Carolina','446 DuBuque Shore Apt. 227','Gibsonport','New Hampshire','France','793 Goodwin Lake Suite 341','North Colten','New Hampshire','47652-0178',NULL,'475262338','CN6442271','2020-11-12','2020-05-21',230,'681967003','Toronto, ON','2015-05-05','','85',19,'2019-09-30 22:17:04','2019-10-03 19:51:20',NULL),
	(145,'Monte Jakubowski','','Male','1900-01-01','stephon.pouros@example.com','663-621-3530 x157','中国','South Velda','California','4610 Karine Camp','Dickifort','Wisconsin','Switzerland','14170 Elinore Mews Suite 533','Botsfordland','Iowa','04065-4498',NULL,'744767056','CN7434238','2020-11-22','2020-05-29',238,'605665640','Toronto, ON','2015-05-14','','85',20,'2019-09-30 22:17:04','2019-10-03 19:53:29',NULL),
	(146,'Mr. Waino Wilkinson','','Male','1900-01-01','darrel88@example.net','(884) 573-3051 x0844','中国','Cordellmouth','Wyoming','73653 Durgan Throughway','Blanchehaven','Arizona','Benin','1973 Marcelina Manor','Winfieldshire','Hawaii','32791',NULL,'423379119','CN2130787','2020-11-10','2020-05-27',228,'411639313','Toronto, ON','2015-05-11','','85',2,'2019-09-30 22:17:04','2019-10-11 15:27:54',NULL),
	(147,'Tabitha Reichert','','Male','1900-01-01','tromp.mauricio@example.com','648.746.8396 x2595','中国','Greenmouth','Louisiana','1192 Upton Stravenue','West Soniaside','Utah','Mauritania','833 Balistreri Wells Suite 747','East Rosemarie','Minnesota','58454',NULL,'664440530','CN1995638','2020-12-07','2020-05-06',215,'811686619','Toronto, ON','2015-05-06','','85',19,'2019-09-30 22:17:04','2019-10-03 19:51:20',NULL),
	(148,'Miss Lilly McKenzie','','Male','1900-01-01','fmayert@example.net','1-661-686-3366','中国','Hickleberg','Virginia','33247 Zane Forges Suite 480','Vonville','Indiana','Paraguay','8865 Sawayn River','Anabelmouth','Hawaii','34680',NULL,'127262273','CN8928109','2020-11-23','2020-05-07',208,'350324858','Toronto, ON','2015-05-10','','85',9,'2019-09-30 22:17:04','2019-10-11 15:26:43',NULL),
	(149,'Nona Williamson','','Male','1900-01-01','ikoss@example.org','(256) 465-5972','中国','Jermainbury','New Mexico','972 Runolfsdottir Lane Apt. 046','Jovaniborough','California','Italy','15509 Tito Estates Suite 966','Beahanchester','New Mexico','09462',NULL,'568950565','CN8771947','2020-12-24','2020-05-03',204,'775958172','Toronto, ON','2015-05-24','','85',20,'2019-09-30 22:17:04','2019-10-11 15:26:43',NULL),
	(150,'Otilia Maggio','','Male','1900-01-01','letitia93@example.org','676-785-7539 x4048','中国','Effertzhaven','Hawaii','283 Kirlin Gardens','Creminmouth','Wisconsin','United States Virgin Islands','58533 Stiedemann Pine','VonRuedenstad','Georgia','45687',NULL,'830277646','CN8108540','2020-11-24','2020-05-07',208,'263941647','Toronto, ON','2015-05-24','','85',11,'2019-09-30 22:17:04','2019-10-11 15:26:43',NULL),
	(151,'Marianne Koelpin','','Male','1900-01-01','robert84@example.org','(784) 651-7715','中国','Robelstad','Illinois','513 Predovic Shore','Nyasiashire','West Virginia','Svalbard & Jan Mayen Islands','216 Jed Green','South Van','Mississippi','95072',NULL,'466777333','CN1677084','2020-12-22','2020-05-25',226,'820319342','Toronto, ON','2015-05-09','','85',2,'2019-09-30 22:17:04','2019-10-11 15:27:54',NULL),
	(152,'Marisa Bosco V','','Male','1900-01-01','zfeeney@example.net','1-215-518-8483 x3852','中国','North Keyon','Pennsylvania','7441 Dach Field','Stephenchester','New Jersey','Senegal','637 Erdman Ville Suite 824','North Adolf','Idaho','72068',NULL,'785467054','CN6045241','2020-11-04','2020-05-30',239,'757548040','Toronto, ON','2015-05-04','','85',12,'2019-09-30 22:17:04','2019-10-03 14:22:46','2019-10-03 14:22:46'),
	(153,'Prof. Maye Hand II','','Male','1900-01-01','williamson.randy@example.net','798-763-0466 x96536','中国','Katelynnhaven','Virginia','55292 Nicolas Cape Apt. 086','Fadelburgh','Maryland','British Indian Ocean Territory (Chagos Archipelago)','64744 Quitzon Brooks Apt. 954','East Hollisfort','Tennessee','27820',NULL,'333134873','CN5168741','2020-10-24','2020-05-08',209,'293600948','Toronto, ON','2015-05-18','','85',2,'2019-09-30 22:17:04','2019-10-11 15:27:54',NULL),
	(154,'Adella Upton','','Male','1900-01-01','augusta.bernhard@example.org','1-838-450-8621 x4754','中国','South Domingo','Alabama','36377 Magnus Spur Apt. 700','West Berneice','Indiana','French Polynesia','695 Champlin Knoll','Lake Sisterside','District of Columbia','96096',NULL,'872939281','CN9084627','2020-12-05','2020-05-20',221,'990884011','Toronto, ON','2015-05-29','','85',16,'2019-09-30 22:17:04','2019-10-11 15:26:43',NULL),
	(155,'Prof. Raheem Collier','','Male','1900-01-01','andreanne.russel@example.net','324.972.7300 x19936','中国','Toneyberg','Maryland','7821 Dach Branch Apt. 483','Rowechester','Indiana','Russian Federation','1387 Garth Drives','McKenzieberg','Arkansas','44107',NULL,'332269264','CN4960847','2020-12-25','2020-05-11',212,'313759750','Toronto, ON','2015-05-10','','85',3,'2019-09-30 22:17:04','2019-10-11 15:26:43',NULL),
	(156,'Dr. Elian Johns III','','Male','1900-01-01','yost.flavio@example.org','350.513.3717','中国','Joanyton','Washington','116 Haag Views','South Jeramy','Minnesota','Guyana','63433 Kayden Spurs','Garrickview','Iowa','44662',NULL,'729877593','CN4319875','2020-11-05','2020-05-03',204,'569467560','Toronto, ON','2015-05-14','','85',8,'2019-09-30 22:17:04','2019-10-11 15:26:43',NULL),
	(157,'Daphne Walker','','Male','1900-01-01','zieme.russell@example.org','785.509.8870 x7157','中国','Vandervortchester','Minnesota','1038 Larkin Island','Zackton','Vermont','Italy','8877 Maiya Spurs','Glentown','California','03029',NULL,'315107486','CN4817182','2020-10-10','2020-05-05',206,'925333711','Toronto, ON','2015-05-17','','85',11,'2019-09-30 22:17:04','2019-10-11 15:26:43',NULL),
	(158,'Prof. Alene Durgan','','Male','1900-01-01','tfay@example.net','+1-983-587-0280','中国','Phoebehaven','West Virginia','66835 Herzog Parkways','South Nya','South Dakota','Honduras','14160 Lakin Summit Suite 192','Port Dorothy','California','98815',NULL,'425563620','CN1821266','2020-12-30','2020-05-19',220,'210093653','Toronto, ON','2015-05-18','','85',14,'2019-09-30 22:17:04','2019-10-11 15:26:43',NULL),
	(159,'Ms. Aubree Gusikowski PhD','','Male','1900-01-01','jleannon@example.org','997.789.8483 x915','中国','Brycenhaven','Hawaii','1641 Elenora Pine Apt. 463','Port Matteostad','New York','Cape Verde','745 Corkery Ferry Suite 350','Eichmannland','West Virginia','09901',NULL,'737084562','CN1411743','2020-11-11','2020-05-05',206,'505577999','Toronto, ON','2015-05-18','','85',1,'2019-09-30 22:17:04','2019-10-11 15:29:39',NULL),
	(160,'Dakota Abernathy','','Male','1900-01-01','miller.narciso@example.org','343.429.3769 x88507','中国','West Marty','Kansas','36036 Alec Ranch','Turnerhaven','Rhode Island','Cook Islands','33763 Wilkinson Way','North Elroyborough','Florida','93730',NULL,'184014071','CN6290032','2020-10-26','2020-05-16',217,'489641624','Toronto, ON','2015-05-30','','85',7,'2019-09-30 22:17:04','2019-10-11 15:26:42',NULL),
	(161,'Tad Macejkovic','','Male','1900-01-01','valentin.skiles@example.org','605.365.0559 x41067','中国','East Sigmund','Hawaii','605 Cullen Drive Apt. 009','East Rowan','Virginia','Peru','644 Janice Fall','Cathyport','Iowa','68724',NULL,'846967094','CN4104757','2020-11-17','2020-05-06',207,'546291318','Toronto, ON','2015-05-28','','85',1,'2019-09-30 22:17:04','2019-10-11 15:29:39',NULL),
	(162,'Maeve Mann','','Male','1900-01-01','vstehr@example.net','1-421-907-9267 x556','中国','Marjolaineside','South Carolina','821 Kutch Garden','Orvalmouth','North Dakota','Greece','8023 Heller Lake Apt. 214','Susanastad','Maryland','45042',NULL,'215134651','CN1220449','2020-12-29','2020-05-07',216,'957757096','Toronto, ON','2015-05-20','','85',12,'2019-09-30 22:17:04','2019-10-03 14:22:43','2019-10-03 14:22:43'),
	(163,'Annalise Kemmer','','Male','1900-01-01','imosciski@example.com','752-445-9892','中国','East Jadyn','New Hampshire','51639 Nienow Park','Pacochabury','Arizona','Belarus','2181 Malvina Glens','North Joshuah','Washington','14822-5006',NULL,'278020187','CN4671601','2020-12-02','2020-05-02',211,'841067459','Toronto, ON','2015-05-05','','85',12,'2019-09-30 22:17:04','2019-10-03 14:22:39','2019-10-03 14:22:39'),
	(164,'Prof. Vito Altenwerth IV','','Male','1900-01-01','xbogan@example.org','+1.496.994.6882','中国','Zacheryville','Illinois','513 Paula Islands Apt. 363','New Juliofort','Mississippi','Malaysia','3763 Hagenes Throughway Apt. 484','Lindview','Nebraska','75476',NULL,'385897842','CN8863819','2020-11-17','2020-05-22',223,'786649925','Toronto, ON','2015-05-03','','85',7,'2019-09-30 22:17:04','2019-10-11 15:26:42',NULL),
	(165,'Stephan Rau','','Male','1900-01-01','tad.kertzmann@example.net','696-964-5145 x0390','中国','Lockmanton','West Virginia','6710 Mikayla Knoll','East Koryville','Florida','Tanzania','21880 Mertie Glens Apt. 286','New Jane','Oklahoma','88508',NULL,'790580463','CN5041878','2020-11-24','2020-05-22',223,'512164182','Toronto, ON','2015-05-04','','85',14,'2019-09-30 22:17:04','2019-10-11 15:26:42',NULL),
	(166,'Alex Schaden','','Male','1900-01-01','gonzalo65@example.com','(771) 797-8566 x461','中国','Daremouth','West Virginia','14512 Jefferey Streets Suite 794','South Maribel','Georgia','Armenia','539 Mitchell Station Apt. 989','Morissettebury','Kansas','98425-3352',NULL,'456662745','CN7718113','2020-12-21','2020-05-01',202,'279733059','Toronto, ON','2015-05-21','','85',18,'2019-09-30 22:17:04','2019-10-11 15:26:42',NULL),
	(167,'Janet Glover MD',NULL,'Male','1900-01-01','rahul32@example.net','(607) 300-7914','AW','Lake Garrison','Iowa','3463 Bahringer Overpass Suite 316','Eltonchester','Delaware','Western Sahara','264 Veda Center','West Lavernaview','Delaware','60404-4254',NULL,'750133630','CN7430593','2020-10-12','2020-05-30',231,'678696401','Toronto, ON','2015-05-24','','85',1,'2019-09-30 22:17:04','2019-10-11 15:29:39',NULL),
	(168,'Mr. Deven Crist','','Male','1900-01-01','icronin@example.org','768-908-8305','中国','North Tressie','Kentucky','13733 Nestor Viaduct','Port Antoniobury','West Virginia','Bangladesh','722 Tina Mall','Port Antonettahaven','Kentucky','56413',NULL,'651764340','CN7617432','2020-10-07','2020-05-08',209,'721274128','Toronto, ON','2015-05-25','','85',7,'2019-09-30 22:17:04','2019-10-11 15:26:42',NULL),
	(169,'Estrella Hayes','','Male','1900-01-01','amaya24@example.com','751-440-4033','中国','New Jennifermouth','Missouri','27741 Hayley Plains','South Dallas','Minnesota','Uganda','543 Tom Unions','Baronberg','Wisconsin','16514',NULL,'824240193','CN6172127','2020-11-17','2020-05-02',203,'971176873','Toronto, ON','2015-05-20','','85',6,'2019-09-30 22:17:04','2019-10-11 15:26:42',NULL),
	(170,'Kiel Ratke','','Male','1900-01-01','kayden.rolfson@example.org','696.980.1647','中国','Kochhaven','New Mexico','164 Ullrich Street','North Glennie','Oklahoma','Dominica','324 Schneider Circle Suite 060','Turnerfort','Arkansas','49884-7497',NULL,'983300571','CN4479087','2020-11-29','2020-05-10',211,'641642202','Toronto, ON','2015-05-19','','85',7,'2019-09-30 22:17:04','2019-10-11 15:26:42',NULL),
	(171,'Zaria Kilback','','Male','1900-01-01','augustus.baumbach@example.com','1-512-915-8202','中国','New Loganville','New Jersey','7432 Greta Lake','New Lilianashire','Alaska','Cape Verde','8422 Annabelle Rapid','Port Irmamouth','Colorado','24600',NULL,'592657422','CN4360875','2020-11-02','2020-05-22',223,'617299444','Toronto, ON','2015-05-25','','85',13,'2019-09-30 22:17:04','2019-10-11 15:26:42',NULL),
	(172,'Dexter Mosciski','','Male','1900-01-01','ekunde@example.org','+1-691-692-6513','中国','Kulasstad','Rhode Island','3250 Brenda Center Apt. 758','West Alfonso','New Hampshire','Uganda','376 Hessel Crossroad Apt. 848','East Dillanberg','Florida','23849',NULL,'934084021','CN6721502','2020-11-26','2020-05-14',215,'300530228','Toronto, ON','2015-05-12','','85',2,'2019-09-30 22:17:04','2019-10-11 15:27:54',NULL),
	(173,'Ms. Earline Boyle','','Male','1900-01-01','windler.tyshawn@example.net','1-646-404-5037 x0016','中国','North Oliver','California','74977 Jerome Orchard Apt. 788','Lake Nathanael','Colorado','Ethiopia','94576 Winfield Pass','Bulahport','Alaska','85741',NULL,'857105616','CN1515293','2020-12-13','2020-05-06',207,'860698848','Toronto, ON','2015-05-06','','85',6,'2019-09-30 22:17:04','2019-10-11 15:26:42',NULL),
	(174,'Carol Stehr DDS','','Male','1900-01-01','jessika.shanahan@example.com','752-518-4287 x8824','中国','East Zula','Louisiana','4521 Grady Mission Suite 207','New Zita','New Jersey','Belgium','7906 Quinten Burg','Binsmouth','North Dakota','31234',NULL,'137056402','CN9352517','2020-12-19','2020-05-30',231,'417558448','Toronto, ON','2015-05-03','','85',15,'2019-09-30 22:17:04','2019-10-11 15:26:42',NULL),
	(175,'Juliana White','','Male','1900-01-01','zpfannerstill@example.org','545-240-9796 x370','中国','East Edd','Utah','97380 Darron Locks Apt. 719','Korbinburgh','Colorado','Jamaica','67247 Ramon Village Suite 274','Lake Branson','Hawaii','97018-6702',NULL,'940265215','CN8782009','2020-12-26','2020-05-04',205,'482788490','Toronto, ON','2015-05-30','','85',14,'2019-09-30 22:17:04','2019-10-11 15:26:42',NULL),
	(176,'Adolfo Williamson','','Male','1900-01-01','ubosco@example.org','1-495-727-0507 x9102','中国','Dollyland','California','247 Cronin Turnpike Suite 341','Daughertyberg','Arkansas','Holy See (Vatican City State)','898 Bernhard Manors','East Ludie','Wyoming','10840-2874',NULL,'184872182','CN8361694','2020-10-20','2020-05-21',222,'665238869','Toronto, ON','2015-05-29','','85',19,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(177,'Verla Rogahn','','Male','1900-01-01','keebler.destinee@example.org','+1 (278) 760-9673','中国','Belleport','Indiana','13433 Demetrius Ways','Ignatiusland','Arizona','Ghana','151 Weimann Burg Apt. 017','Maggieburgh','South Dakota','83003-7609',NULL,'446485546','CN9257230','2020-10-12','2020-05-03',204,'648194822','Toronto, ON','2015-05-20','','85',20,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(178,'Mr. Quinn Skiles','','Male','1900-01-01','schowalter.opal@example.net','236.375.3165','中国','East Delpha','Delaware','1019 Lorine Mountain','New Kadehaven','Wyoming','Swaziland','1667 Steuber Spring','McCulloughborough','South Carolina','65065',NULL,'945890568','CN2911229','2020-11-10','2020-05-15',216,'581059900','Toronto, ON','2015-05-04','','85',7,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(179,'Montana Becker','','Male','1900-01-01','zblanda@example.net','970-913-1941','中国','New Jaredhaven','Maine','53880 Fritsch Mall','Sanfordside','Alabama','Argentina','2681 Grant Plains','Bernardochester','Kentucky','86638',NULL,'599410449','CN2837390','2020-11-17','2020-05-09',210,'610548095','Toronto, ON','2015-05-24','','85',20,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(180,'Aglae Homenick','','Male','1900-01-01','libby30@example.net','(594) 718-6027 x578','中国','Lake Brock','Vermont','1262 Shields Drives','Michaelborough','Kansas','Georgia','16759 Gideon Manors','South Fritz','Tennessee','62617-3143',NULL,'591642155','CN7481741','2020-10-13','2020-05-05',206,'176674603','Toronto, ON','2015-05-27','','85',8,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(181,'Kayleigh Kihn','','Male','1900-01-01','lexi63@example.org','1-713-425-7323','中国','South Willardstad','Florida','47720 Manley Port Suite 324','Ericbury','Maryland','Cote d\'Ivoire','58514 Ransom Turnpike','Nitzschefurt','Utah','08579',NULL,'553676677','CN8771135','2020-12-15','2020-05-29',230,'763048196','Toronto, ON','2015-05-04','','85',20,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(182,'Vada Olson','','Male','1900-01-01','daugherty.crawford@example.net','+1-948-761-4065','中国','Narcisobury','Ohio','68521 Jensen Green','South Albinberg','Rhode Island','Isle of Man','32828 Antwan Stravenue Apt. 613','Gibsonstad','California','27646',NULL,'852679725','CN5305610','2020-11-10','2020-05-17',226,'188594175','Toronto, ON','2015-05-12','','85',12,'2019-09-30 22:17:04','2019-10-03 14:22:36','2019-10-03 14:22:36'),
	(183,'Rose Altenwerth','','Male','1900-01-01','nhuels@example.org','554.524.3475 x06483','中国','South Kamille','Ohio','2079 Kailey Manors','Hettingerfort','Arkansas','Marshall Islands','138 Ayden Gardens','Beerbury','Delaware','18000',NULL,'397766178','CN9251930','2020-11-24','2020-05-13',214,'272514298','Toronto, ON','2015-05-24','','85',11,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(184,'Marquise Schuppe','','Male','1900-01-01','schuster.kennedy@example.com','(679) 389-9040 x73153','中国','Schadenshire','Georgia','932 Dejon Terrace Apt. 936','Hermanmouth','Illinois','Tuvalu','154 Mertz Stream Apt. 467','Mayertberg','Oklahoma','60643-3449',NULL,'544142307','CN3962410','2020-10-27','2020-05-28',229,'809847972','Toronto, ON','2015-05-28','','85',14,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(185,'Marcelino Botsford','','Male','1900-01-01','vabbott@example.org','816.924.8536 x15559','中国','East Lia','Minnesota','81107 Haag Vista Apt. 904','Nolanberg','Nebraska','Maldives','334 Frami Valleys Apt. 870','Lake Jaunita','South Carolina','61147',NULL,'508525279','CN7896298','2020-12-09','2020-05-13',214,'670804795','Toronto, ON','2015-05-10','','85',17,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(186,'Jayden Stamm','','Male','1900-01-01','neoma.gibson@example.net','445-686-4828 x96718','中国','East Eudoramouth','North Carolina','6536 Morissette Forge Suite 996','Madelynmouth','Louisiana','Sri Lanka','382 Stracke Walk Suite 122','Kileyside','Ohio','38201-1154',NULL,'188035858','CN6934713','2020-11-17','2020-05-23',224,'141682330','Toronto, ON','2015-05-01','','85',2,'2019-09-30 22:17:04','2019-10-11 15:27:54',NULL),
	(187,'Ariane Lubowitz','','Male','1900-01-01','aditya.stiedemann@example.org','549-440-7911 x99061','中国','East Rosettafort','Iowa','40647 Lempi Fields Suite 250','Beattyshire','Wyoming','Jersey','11030 Ernser Junction Suite 308','Mayertborough','District of Columbia','79949-6359',NULL,'683889634','CN8108019','2020-12-28','2020-05-24',225,'449691394','Toronto, ON','2015-05-30','','85',5,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(188,'Prof. Fleta Satterfield PhD','','Male','1900-01-01','odamore@example.net','+1-348-245-7481','中国','Stiedemannport','Kentucky','6745 Demetris Vista Suite 275','Timothyberg','South Dakota','Martinique','27626 Hope Throughway Suite 552','South Rafaelaborough','District of Columbia','19588',NULL,'925692280','CN1924482','2020-11-04','2020-05-03',204,'991857356','Toronto, ON','2015-05-27','','85',5,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(189,'Dovie Murazik IV','','Male','1900-01-01','mlehner@example.com','609.473.8745','中国','Enolashire','New Hampshire','8609 Effertz Hollow Suite 331','West Maiaburgh','North Carolina','Oman','377 Antwan Ranch Suite 018','West Hardy','Virginia','35972-6710',NULL,'649586739','CN3715809','2020-12-23','2020-05-14',215,'177232039','Toronto, ON','2015-05-13','','85',15,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(190,'Mr. Brain Lang Jr.','','Male','1900-01-01','schulist.edna@example.com','692.744.5074','中国','South Brady','Rhode Island','1579 Orn Parkways','East Leta','Connecticut','United States of America','33640 Rick Stream Apt. 080','West Lewis','Arkansas','08596-5793',NULL,'238795788','CN4859166','2020-12-15','2020-05-28',229,'665343103','Toronto, ON','2015-05-30','','85',9,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(191,'Dr. Percy Gaylord PhD','','Male','1900-01-01','karine99@example.net','(532) 252-0222 x64633','中国','West Diana','District of Columbia','9978 Sylvia Dam Suite 364','New Luis','Ohio','United States of America','96390 Quigley Fork','South Brantton','Maryland','23190',NULL,'930259051','CN4039512','2020-12-10','2020-05-20',221,'176950517','Toronto, ON','2015-05-16','','85',15,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(192,'Alexander Hintz','','Male','1900-01-01','carol.hickle@example.net','(862) 718-2611 x9618','中国','New Kaley','Virginia','83281 Leland Path Apt. 813','North Tierraborough','Kansas','Sri Lanka','818 Reichel Pass','East Patberg','Texas','85620',NULL,'962684040','CN2750842','2020-11-19','2020-05-03',204,'750147351','Toronto, ON','2015-05-05','','85',13,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(193,'Prof. Abdul Oberbrunner DDS','','Male','1900-01-01','michele.graham@example.org','393.489.6504','中国','Parkerside','Arizona','88308 Luettgen Parks Apt. 096','Bahringerberg','South Carolina','Thailand','819 Adrien Skyway','Lake Lester','Iowa','28898-1227',NULL,'381752166','CN2168574','2020-12-20','2020-05-20',221,'675151105','Toronto, ON','2015-05-06','','85',9,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(194,'Mrs. Evalyn Turner','','Male','1900-01-01','terry.farrell@example.org','(952) 674-6015 x51362','中国','Haagfort','New Jersey','7168 Roderick Harbor Apt. 056','Wisokyton','Alaska','Hong Kong','2757 Bernhard Meadow','Garrisonland','Kansas','59593-5856',NULL,'652124848','CN4691049','2020-12-13','2020-05-24',225,'545519767','Toronto, ON','2015-05-05','','85',19,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(195,'Emmanuelle Stamm DDS','','Male','1900-01-01','lynn.schumm@example.org','+1.842.799.4362','中国','Daphneeport','Montana','132 Homenick Prairie','Port Shawn','North Dakota','Latvia','5919 Cecile Drive','Herminioview','Arizona','17722-3042',NULL,'365531001','CN4984966','2020-12-01','2020-05-30',231,'490759920','Toronto, ON','2015-05-29','','85',4,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(196,'Glennie Kiehn','','Male','1900-01-01','mdamore@example.net','+1.449.546.0342','中国','South Furman','Massachusetts','689 Gaston Orchard','Cheyanneport','Arizona','Congo','88558 Edna Plains Suite 134','Aniyaburgh','Montana','63407',NULL,'482658010','CN3374938','2020-10-11','2020-05-25',226,'114362831','Toronto, ON','2015-05-02','','85',3,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(197,'Buck Wunsch','','Male','1900-01-01','mschroeder@example.com','+18987712616','中国','Nyaburgh','Maryland','2301 Destinee Isle Suite 039','New Caterinaberg','Connecticut','Kiribati','47732 Hackett Pass Suite 471','North Russel','California','65483-0336',NULL,'536830040','CN6784058','2020-12-24','2020-05-01',202,'993907902','Toronto, ON','2015-05-29','','85',6,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(198,'Gerardo Heathcote','','Male','1900-01-01','jacobi.lorna@example.org','452-489-5878','中国','Elmirabury','Hawaii','9433 White Curve Apt. 092','Cathyville','Michigan','Papua New Guinea','728 Justyn Islands','Quintenview','Louisiana','33426-0859',NULL,'411321860','CN4617007','2020-10-17','2020-05-06',207,'239963735','Toronto, ON','2015-05-01','','85',8,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(199,'Dan Monahan V','','Male','1900-01-01','hermina31@example.net','691.942.4740 x29456','中国','Lake Alysaville','Washington','775 Nikolaus Orchard','Christborough','Texas','Isle of Man','860 Daniel Mews','Cassidyfurt','Nebraska','83738-3949',NULL,'334602361','CN3522785','2020-11-03','2020-05-25',226,'858569346','Toronto, ON','2015-05-24','','85',4,'2019-09-30 22:17:04','2019-10-11 15:27:40',NULL),
	(200,'Lilla Considine V',NULL,'Male','1900-01-01','alisha.aufderhar@example.net','930-351-9409 x065','中国','Bauchshire','Arizona','5425 Shane Pike Suite 810','West Cortneychester','Tennessee','Greece','4993 Jaime Station','East Hortensefort','New York','69318-7508',1,'325968100','CN7757746','2020-11-05','2019-09-13',-28,'454323885','Toronto, ON','2015-05-08','','95',NULL,'2019-09-30 22:17:04','2019-10-11 15:23:54',NULL),
	(201,'许诺',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'W9XOkpiZqtbo8WHsUQxUTY4W2mU6NVt62J1lSKu5.jpeg','',NULL,'2019-10-03 15:06:35','2019-10-11 14:25:23',NULL);

/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table countries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table departments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `departments_department_id_foreign` (`department_id`),
  CONSTRAINT `departments_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;

INSERT INTO `departments` (`id`, `name`, `department_id`, `created_at`, `updated_at`)
VALUES
	(1,'经理部',NULL,'2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(2,'行政部',NULL,'2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(3,'市场部',NULL,'2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(4,'业务部',NULL,'2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(5,'文案部',NULL,'2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(6,'规划部',NULL,'2019-10-03 20:29:07','2019-10-03 20:29:07');

/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table employees
# ------------------------------------------------------------

DROP TABLE IF EXISTS `employees`;

CREATE TABLE `employees` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `join_date` date DEFAULT '1900-01-01',
  `job_level_id` bigint(20) unsigned DEFAULT NULL,
  `passport` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'CN00000',
  `sin_number` int(11) DEFAULT '0',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `address2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'Canada',
  `postal_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `gender` enum('Male','Female','Other') COLLATE utf8mb4_unicode_ci DEFAULT 'Other',
  `contact_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `initial_department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `salary` int(11) DEFAULT '0',
  `status_in_canada_id` bigint(20) unsigned DEFAULT NULL,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `department_id` bigint(20) unsigned DEFAULT NULL,
  `birthdate` date NOT NULL DEFAULT '1900-01-01',
  `title_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employees_user_id_foreign` (`user_id`),
  KEY `employees_job_level_id_foreign` (`job_level_id`),
  KEY `employees_role_id_foreign` (`role_id`),
  KEY `employees_department_id_foreign` (`department_id`),
  KEY `employees_title_id_foreign` (`title_id`),
  CONSTRAINT `employees_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`),
  CONSTRAINT `employees_job_level_id_foreign` FOREIGN KEY (`job_level_id`) REFERENCES `job_levels` (`id`),
  CONSTRAINT `employees_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `employees_title_id_foreign` FOREIGN KEY (`title_id`) REFERENCES `titles` (`id`),
  CONSTRAINT `employees_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;

INSERT INTO `employees` (`id`, `user_id`, `name`, `phone`, `join_date`, `job_level_id`, `passport`, `sin_number`, `address`, `address2`, `city`, `province`, `country`, `postal_code`, `gender`, `contact_email`, `initial_department`, `salary`, `status_in_canada_id`, `role_id`, `department_id`, `birthdate`, `title_id`, `created_at`, `updated_at`)
VALUES
	(1,1,'Admin','728.699.6815 x2245','2019-09-30',6,'CN3221886',213036481,'19790 Camron Forest','','West Pierre','South Carolina','Liberia','55881-8676','Male','kfadel@example.net','',8685039,NULL,1,4,'1900-01-01',16,'2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(2,2,'Edison Bauch','(347) 909-7353 x1262','2019-09-30',2,'CN1329103',811431568,'1369 Bailey Dam Apt. 792','','East Aidan','New Mexico','United States of America','43923','Male','betty.terry@example.com','',8664391,NULL,2,2,'1900-01-01',40,'2019-09-30 22:17:03','2019-09-30 22:17:03');

/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table failed_jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table job_levels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `job_levels`;

CREATE TABLE `job_levels` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `job_levels` WRITE;
/*!40000 ALTER TABLE `job_levels` DISABLE KEYS */;

INSERT INTO `job_levels` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(2,'留学顾问','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(3,'见习规划师','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(4,'留学规划师','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(5,'移民规划师','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(6,'资深规划师','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(7,'暂无','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(8,'行政规划师','2019-10-04 12:55:51','2019-10-04 12:55:51');

/*!40000 ALTER TABLE `job_levels` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(416,'2014_10_12_000000_create_users_table',1),
	(417,'2014_10_12_100000_create_password_resets_table',1),
	(418,'2018_01_01_000000_create_action_events_table',1),
	(419,'2018_09_30_182643_create_clients_table',1),
	(420,'2019_05_10_000000_add_fields_to_action_events_table',1),
	(421,'2019_08_19_000000_create_failed_jobs_table',1),
	(422,'2019_09_05_201507_create_countries_table',1),
	(423,'2019_09_05_202320_create_cities_table',1),
	(424,'2019_09_06_182657_create_roles_table',1),
	(425,'2019_09_06_182711_create_departments_table',1),
	(426,'2019_09_06_212843_create_titles_table',1),
	(427,'2019_09_06_215739_create_permissions_table',1),
	(428,'2019_09_12_204009_create_job_levels_table',1),
	(429,'2019_09_19_144456_create_status_in_canadas_table',1),
	(430,'2019_09_25_204049_create_provinces_table',1),
	(431,'2019_09_25_204101_create_regions_table',1),
	(432,'2019_09_30_182628_create_employees_table',1),
	(433,'2019_10_01_222149_add_employee_to_users_table',2),
	(434,'2019_10_01_222443_drop_employees_table',2),
	(435,'2019_10_02_205532_add_name_to_users_table',3),
	(436,'2019_10_02_213118_add_name_to_users_table',4),
	(437,'2019_10_02_220116_add_phone_to_users_table',5),
	(438,'2019_10_02_221402_add_join_date_to_users_table',6),
	(439,'2019_10_03_220549_add_department_id_to_titles',7);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table permission_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` bigint(20) unsigned DEFAULT NULL,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `more` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_foreign` (`permission_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`, `more`, `created_at`, `updated_at`)
VALUES
	(1,1,1,NULL,NULL,NULL),
	(2,2,1,NULL,NULL,NULL),
	(3,3,1,NULL,NULL,NULL),
	(4,4,1,NULL,NULL,NULL),
	(5,5,1,NULL,NULL,NULL),
	(6,6,1,NULL,NULL,NULL),
	(7,7,1,NULL,NULL,NULL),
	(8,8,1,NULL,NULL,NULL),
	(9,5,2,NULL,NULL,NULL),
	(10,7,2,NULL,NULL,NULL);

/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;

INSERT INTO `permissions` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'用户权限管理','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(2,'信息管理','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(3,'客户列表','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(4,'安全界面','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(5,'添加新客户','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(6,'删除客户','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(7,'修改客户信息','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(8,'地址修改','2019-09-30 22:17:03','2019-09-30 22:17:03');

/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table provinces
# ------------------------------------------------------------

DROP TABLE IF EXISTS `provinces`;

CREATE TABLE `provinces` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `region_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `provinces` WRITE;
/*!40000 ALTER TABLE `provinces` DISABLE KEYS */;

INSERT INTO `provinces` (`id`, `name`, `region_id`, `created_at`, `updated_at`)
VALUES
	(11,'北京市',1,NULL,NULL),
	(12,'天津市',1,NULL,NULL),
	(13,'河北省',1,NULL,NULL),
	(14,'山西省',1,NULL,NULL),
	(15,'内蒙古自治区',1,NULL,NULL),
	(21,'辽宁省',2,NULL,NULL),
	(22,'吉林省',2,NULL,NULL),
	(23,'黑龙江省',2,NULL,NULL),
	(31,'上海市',3,NULL,NULL),
	(32,'江苏省',3,NULL,NULL),
	(33,'浙江省',3,NULL,NULL),
	(34,'安徽省',3,NULL,NULL),
	(35,'福建省',3,NULL,NULL),
	(36,'江西省',3,NULL,NULL),
	(37,'山东省',3,NULL,NULL),
	(41,'河南省',4,NULL,NULL),
	(42,'湖北省',4,NULL,NULL),
	(43,'湖南省',4,NULL,NULL),
	(44,'广东省',4,NULL,NULL),
	(45,'广西壮族自治区',4,NULL,NULL),
	(46,'海南省',4,NULL,NULL),
	(50,'重庆市',5,NULL,NULL),
	(51,'四川省',5,NULL,NULL),
	(52,'贵州省',5,NULL,NULL),
	(53,'云南省',5,NULL,NULL),
	(54,'西藏自治区',5,NULL,NULL),
	(61,'陕西省',6,NULL,NULL),
	(62,'甘肃省',6,NULL,NULL),
	(63,'青海省',6,NULL,NULL),
	(64,'宁夏回族自治区',6,NULL,NULL),
	(65,'新疆维吾尔自治区',6,NULL,NULL);

/*!40000 ALTER TABLE `provinces` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table regions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `regions`;

CREATE TABLE `regions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `regions` WRITE;
/*!40000 ALTER TABLE `regions` DISABLE KEYS */;

INSERT INTO `regions` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'华北地区',NULL,NULL),
	(2,'东北地区',NULL,NULL),
	(3,'华东地区',NULL,NULL),
	(4,'中南地区',NULL,NULL),
	(5,'西南地区',NULL,NULL),
	(6,'西北地区',NULL,NULL);

/*!40000 ALTER TABLE `regions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'管理员','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(2,'实习生','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(3,'规划师','2019-10-03 19:31:56','2019-10-03 19:31:56'),
	(4,'文案师','2019-10-03 19:32:04','2019-10-03 19:32:04'),
	(5,'收款员','2019-10-03 19:32:13','2019-10-03 19:32:13'),
	(7,'行政师','2019-10-03 19:50:37','2019-10-03 19:50:37');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table status_in_canadas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `status_in_canadas`;

CREATE TABLE `status_in_canadas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `status_in_canadas` WRITE;
/*!40000 ALTER TABLE `status_in_canadas` DISABLE KEYS */;

INSERT INTO `status_in_canadas` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'SP','2019-09-30 22:17:03','2019-10-03 14:12:54'),
	(2,'WP','2019-09-30 22:17:03','2019-10-03 14:13:27'),
	(3,'VV','2019-09-30 22:17:03','2019-10-03 14:13:51'),
	(4,'居民','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(5,'公民','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(6,'难民','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(7,'第一次签证','2019-09-30 22:17:03','2019-09-30 22:17:03'),
	(8,'10','2019-10-03 14:14:26','2019-10-03 14:14:26'),
	(10,'20','2019-10-03 14:14:34','2019-10-03 14:14:34'),
	(11,'15','2019-10-03 14:15:01','2019-10-03 14:15:01');

/*!40000 ALTER TABLE `status_in_canadas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table titles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `titles`;

CREATE TABLE `titles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_title_id` bigint(20) unsigned DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `department_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `titles` WRITE;
/*!40000 ALTER TABLE `titles` DISABLE KEYS */;

INSERT INTO `titles` (`id`, `name`, `parent_title_id`, `created_at`, `updated_at`, `department_id`)
VALUES
	(1,'总经理 ',0,'2019-09-30 22:17:03','2019-09-30 22:17:03',1),
	(2,'Markham/经理 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',1),
	(3,'North ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',1),
	(4,'业务部/部长 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',4),
	(5,'规划一队/主任 ',4,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(6,'规划二队/.主任 ',4,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(7,'规划三队/主任 ',4,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(8,'规划一队/一组/主管 ',5,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(9,'规划一队/二组/主管 ',5,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(10,'规划一队/三组/主管 ',5,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(11,'规划二队/一组/主管 ',6,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(12,'规划二队/二组/主管 ',6,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(13,'规划二队/三组/主管 ',6,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(14,'规划三队/一组/主管 ',7,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(15,'规划三队/二组/主管 ',7,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(16,'规划三队/三组/主管 ',7,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(17,'规划一队/一组/组员 ',8,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(18,'规划一队/二组/组员 ',9,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(19,'规划一队/三组/组员 ',10,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(20,'规划二队/一组/组员 ',11,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(21,'规划二队/二组/组员 ',12,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(22,'规划二队/三组/组员 ',13,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(23,'规划三队/一组/组员 ',14,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(24,'规划三队/二组/组员 ',15,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(25,'规划三队/三组/组员 ',16,'2019-09-30 22:17:03','2019-09-30 22:17:03',6),
	(26,'留学顾问/主任 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',4),
	(27,'培训讲师/主管 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',4),
	(28,'留学顾问/主管 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',4),
	(29,'实习生 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',4),
	(30,'留学顾问 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',4),
	(31,'移民/讲师 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',4),
	(32,'留学/讲师 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',4),
	(33,'行政部/部长 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',2),
	(34,'外联/主任 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',2),
	(35,'人力资源/主任 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',2),
	(36,'外联/助理 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',2),
	(37,'招聘/主管 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',2),
	(38,'前台 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',2),
	(39,'市场部/部长 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',3),
	(40,'新媒体运营/主任 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',3),
	(41,'IT/主任 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',3),
	(42,'设计/主任 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',3),
	(43,'视频剪辑师 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',3),
	(44,'文字编辑师 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',3),
	(45,'网络工程师 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',3),
	(46,'平面设计师 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',3),
	(47,'文案部/部长 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',5),
	(48,'签证中心/主任 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',5),
	(49,'申请中心/主任 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',5),
	(50,'文案 ',1,'2019-09-30 22:17:03','2019-09-30 22:17:03',5);

/*!40000 ALTER TABLE `titles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `job_level_id` bigint(20) unsigned DEFAULT NULL,
  `passport` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'CN00000',
  `sin_number` int(11) DEFAULT '0',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `address2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'Canada',
  `postal_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `gender` enum('Male','Female','Other') COLLATE utf8mb4_unicode_ci DEFAULT 'Other',
  `contact_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `initial_department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `salary` int(11) DEFAULT '0',
  `status_in_canada_id` bigint(20) unsigned DEFAULT NULL,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `department_id` bigint(20) unsigned DEFAULT NULL,
  `birthdate` date NOT NULL DEFAULT '1900-01-01',
  `title_id` bigint(20) unsigned DEFAULT NULL,
  `identity` enum('SP','WP','VR','PR','CC','CN') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `join_date` date DEFAULT '1900-01-01',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_job_level_id_foreign` (`job_level_id`),
  KEY `users_role_id_foreign` (`role_id`),
  KEY `users_department_id_foreign` (`department_id`),
  KEY `users_title_id_foreign` (`title_id`),
  CONSTRAINT `users_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`),
  CONSTRAINT `users_job_level_id_foreign` FOREIGN KEY (`job_level_id`) REFERENCES `job_levels` (`id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `users_title_id_foreign` FOREIGN KEY (`title_id`) REFERENCES `titles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `job_level_id`, `passport`, `sin_number`, `address`, `address2`, `city`, `province`, `country`, `postal_code`, `gender`, `contact_email`, `initial_department`, `salary`, `status_in_canada_id`, `role_id`, `department_id`, `birthdate`, `title_id`, `identity`, `name`, `phone`, `join_date`)
VALUES
	(1,'admin@admin.com','2019-09-30 22:17:03','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','VwazbyNlqEJrQBt24m5N7dL0FvcvN6OIf1WbEu3ofQqEN0hZFp2bFjKIj35f','2019-09-30 22:17:03','2019-10-11 14:57:10',5,'CN00000',0,NULL,'',NULL,NULL,'Canada',NULL,'Male','admin@admin.com','',0,NULL,1,4,'1900-01-01',4,'VR','admin','+1(905)-000-0000','2000-01-01'),
	(2,'test@test.com','2019-09-30 22:17:03','$2y$10$p4Z//itvZkPd/mhHHRmuVuoQTo66uz.GcQBn3wRsPRLxBYPp98XHS','NEbM2JEYMFZFovlyJcenLWJnkPPede1oXgRZbe7jkBdNJZOEO3ikdGU3QbAx','2019-09-30 22:17:03','2019-10-03 13:07:00',7,'CN00000',0,'110 steels','','Markham','ON','Canada','L4D 9D8','Female','test@test.com','',0,NULL,NULL,4,'1900-01-01',44,'SP','USER','+1+1(905)-111-01','1900-01-01'),
	(13,'test@gmail.com',NULL,'$2y$10$XjTBSRz5iurngyFwlND3K.jVvcnHCgti85lp/0oSWGaO8ciTkguS6',NULL,'2019-10-03 14:24:05','2019-10-03 14:24:05',3,'12313',12344567,'12 PARSELL ST','','RICHMOND HILL','ON','Canada','L4E0C7','Male','test@gmail.com','',12,NULL,2,4,'1995-12-12',1,'PR','test','+1(123)-456-7890','2019-10-10'),
	(16,'hank.promise@gmail.com',NULL,'$2y$10$71sZd4slmfghTwyeGbRNd.EAfCid.kKX5PklGcSf0W0Su586XuV1m','mDwxOCtB9WDnjveCTDTqArSJZ64KjuhQHnz01sP13UEESCaDwTHipdab16RQ','2019-10-03 15:05:35','2019-10-03 19:42:31',6,NULL,NULL,'Unit 201，20 Amber St','','Markham','ON','Canada','L3R 5P4','Male','hank.promise@gmail.com','',4000,NULL,1,1,'1991-09-19',2,'CC','许诺','+1(647)-870-2666','2018-01-01'),
	(17,'Zero870112@hotmail.com',NULL,'$2y$10$o11z1HcP9I87Gi37BQZSduipRNiFqXJqcOQn53k8iOhmiZch4u4Da',NULL,'2019-10-03 19:40:08','2019-10-03 19:40:08',7,NULL,578385973,'506-8110  Brichmount Road','','Markham','ON','Canada','L6G 0E3','Male','Zero870112@hotmail.com','',NULL,NULL,4,5,'1987-01-12',47,'PR','张翊人','+1(647)-808-8458','2018-01-01'),
	(18,'angle.hq@hotmail.com',NULL,'$2y$10$nl7K/YGXYkNul/RU0.w99ubiIo3i1s.u.TCnXVgsAgc0ZXpCxREw2',NULL,'2019-10-03 19:44:54','2019-10-03 19:44:54',7,NULL,NULL,NULL,'',NULL,NULL,'Canada',NULL,'Male','angle.hq@hotmail.com','',NULL,NULL,4,5,'1990-10-11',2,'PR','宋海培','+1(416)-846-2886','2018-01-01'),
	(19,'oyworkshop@hotmail.com',NULL,'$2y$10$VghCGIgeV3hsfRQpYLATO.kuo7mtjcEmnoSavog9ikQCvEb45sdXG',NULL,'2019-10-03 19:50:14','2019-10-03 19:51:17',7,NULL,545256471,NULL,'',NULL,NULL,'Canada',NULL,'Male','oyworkshop@hotmail.com','',1000,NULL,7,2,'1989-08-04',33,'CC','欧阳俊杰','+1(416)-875-6689','2018-03-15'),
	(20,'timsun@lpfirm.ca',NULL,'$2y$10$8FnPDpqZv3gNR2j8OgGP3OOUtg4imWGflI5C/Sk00HECPe0QP.W6K',NULL,'2019-10-03 19:53:26','2019-10-03 19:53:26',6,NULL,565176708,NULL,'',NULL,NULL,'Canada',NULL,'Male','timsun@lpfirm.ca','',NULL,NULL,3,4,'1995-06-13',4,'CC','孙浩棠','+1(541)-566-3722','2018-01-01'),
	(21,'cynthiapink1013@gmail.com',NULL,'$2y$10$LPBI3FpO/oJPNm.ZvOxAE.eMfb7DDj43VxQGJzbV9byao/krQgCua',NULL,'2019-10-03 20:28:55','2019-10-03 20:33:46',6,NULL,592056527,NULL,'',NULL,NULL,'Canada',NULL,'Female','cynthiapink1013@gmail.com','',NULL,NULL,NULL,6,'1990-11-26',5,'PR','高姿','+1(416)-721-9686','2018-10-01'),
	(22,'mumulee555@gmail.com',NULL,'$2y$10$krwEEYZWvr8BwY9Y56fb0.s6v/zzsheC.sMlzEFvHrKAbAbgOwU.G',NULL,'2019-10-03 20:37:14','2019-10-03 20:37:14',7,NULL,930518295,NULL,'',NULL,NULL,'Canada',NULL,'Female','mumulee555@gmail.com','',NULL,NULL,3,4,'1993-08-09',35,'WP','李建莹','+1(647)-895-5856','2018-05-01'),
	(23,'wujiayi9322@gmail.com',NULL,'$2y$10$3hdsXaU2EoenzDArkk21P.NY8eMcTGYPz.2vmk02d4hxO.deJTKVS',NULL,'2019-10-03 20:39:17','2019-10-03 20:39:17',7,NULL,945376192,NULL,'',NULL,NULL,'Canada',NULL,'Female','wujiayi9322@gmail.com','',NULL,NULL,4,5,'1993-02-02',NULL,'WP','吴佳伊','+1(647)-871-4622','2018-05-01'),
	(25,'olivia.zhcc@gmail.com',NULL,'$2y$10$I7Z7yvbf5EsagPvgmvXzp.0vsekrbXu4M3JiUNeaEB/N.e65680Hu',NULL,'2019-10-04 12:40:13','2019-10-11 15:04:57',2,NULL,935938803,NULL,'',NULL,NULL,'Canada',NULL,'Female','olivia.zhcc@gmail.com','',0,NULL,3,4,'1995-04-24',30,'WP','Alvin Zhong','+1+1+1(905)-000','2018-05-01'),
	(26,'test1@gmail.com',NULL,'$2y$10$Khuy9VVVHnQyQGrI1e5e.uOfTfPcrc6nRVYLbLL5pjSKSHh9xOaf6',NULL,'2019-10-11 15:09:40','2019-10-11 15:09:40',3,'999-999-999',999999999,'2-110 cre','','markham','Ontario','Canada','L3R 1H3','Male','test@gmail.com','',0,NULL,2,4,'2000-01-01',29,'CN','test','+1(905)-000-0001','2019-01-01');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
