<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->unsignedBigInteger('user_id')->nullable();

            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            
            $table->string("name")->default("")->nullable();
            $table->string("phone")->default("")->nullable();
            $table->date("join_date")->default("1900-01-01")->nullable();

            $table->unsignedBigInteger('job_level_id')->nullable()->nullable(); //工作职称
            $table->foreign('job_level_id')
                    ->references('id')
                    ->on('job_levels');
        
            $table->String("passport")->default("CN00000")->nullable();
            $table->String("sin_number")->default('0')->nullable();
            
            $table->string("address")->default("")->nullable();
            $table->string("address2")->default("")->nullable();
            $table->string("city")->default("")->nullable();
            $table->string("province")->default("")->nullable();
            $table->string("country")->default("Canada")->nullable();
            $table->string("postal_code")->default("")->nullable();

            $table->enum('gender', ['Male', 'Female', 'Other'])->default("Other")->nullable();
            $table->string("contact_email")->default("")->nullable();
            $table->string("initial_department")->default("")->nullable();
            $table->integer("salary")->default(0)->nullable();

            $table->unsignedBigInteger('status_in_canada_id')->nullable();
            
            $table->unsignedBigInteger('role_id')->nullable(); //权限
            $table->foreign('role_id')
                    ->references('id')
                    ->on('roles');


            $table->unsignedBigInteger('department_id')->nullable(); //权限
            $table->foreign('department_id')
                    ->references('id')
                    ->on('departments');

            $table->date("birthdate")->default("1900-01-01");

            $table->unsignedBigInteger('title_id')->nullable(); //工作职位
            $table->foreign('title_id')
                    ->references('id')
                    ->on('titles');

            $table->timestamps();
        });
    }

    /**
     * 
     * 
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
