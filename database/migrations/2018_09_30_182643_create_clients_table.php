<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string("name");
            $table->string("usedName")->default("")->nullable();
            $table->enum('gender', ['Male', 'Female', 'Other'])->default("Other")->nullable();
            
            $table->date("birthdate")->default("1900-01-01")->nullable();
            $table->string("contact_email")->default("")->nullable();
            $table->string("phone")->default("")->nullable();

            $table->string("citizenship")->default("Canada")->nullable(); //国籍

            $table->string("birth_city")->default("")->nullable();
            $table->string("birth_province")->default("")->nullable();
            $table->string("birth_region")->default("")->nullable();

            
            $table->string("original_city")->default("")->nullable();
            $table->string("original_province")->default("")->nullable();
            $table->string("original_region")->default("")->nullable();
            
            $table->string("address")->default("")->nullable();
            $table->string("city")->default("")->nullable();
            $table->string("province")->default("")->nullable();
            $table->string("postal_code")->default("")->nullable();

            $table->unsignedBigInteger('status_in_canada_id')->nullable()->nullable();
            
            $table->string("chinese_id_number")->default("")->nullable();
            $table->string("passport")->default("")->nullable();
            $table->date("passport_expire")->default("1900-01-01")->nullable();
            
            $table->date("visa_expire")->default("1900-01-01")->nullable();
            $table->integer("days_until_expire")->default(0)->nullable()->nullable();

            $table->string("uci_number")->default("")->nullable();

            $table->string("landing_location")->default("")->nullable();
            $table->date("landing_date")->default("1900-01-01")->nullable();

            $table->string("photo")->default("")->nullable();
            $table->string("completion_rate")->default("")->nullable();

            $table->unsignedBigInteger('user_id')->nullable();

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
